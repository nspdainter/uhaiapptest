import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:uhaiapp/nurse/nurseDash.dart';
import 'package:uhaiapp/parent/parentDash.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginProd extends StatefulWidget {
  const LoginProd({super.key});

  @override
  _LoginProdState createState() => _LoginProdState();
}

class _LoginProdState extends State<LoginProd> {
  final _phoneController = TextEditingController();
  final _passwordController = TextEditingController();
  String _errorMessage = '';
  bool _isLoading = false;
  var _isObscured;

  @override
  void initState() {
    super.initState();
    _isObscured = true;

  }

  @override
  void dispose(){
    super.dispose();
  }

  // /////// login method start

  Future<void> _login( ) async {
    setState(() {
      _isLoading = true;

    });


    final String phoneNumber = _phoneController.text.trim();
    final String password = _passwordController.text;

    final Uri url = Uri.parse('https://uhaihealthcare.com/api/v1/user/login/');
    // var headers = {'Content-Type': 'application/json'};
    final Map<String, dynamic > requestBody = {
      "username": phoneNumber,
      "password": password
    };

    // print(requestBody);

    try {
      http.Response response = await http.post(
        url,
        body: requestBody

      );
      // print(response.statusCode);

      if (response.statusCode == 200) {
        // Successful login
        final Map<String, dynamic> responseData = json.decode(response.body);
        final String token = responseData['token'];

        // Save token to SharedPreferences
        final SharedPreferences prefs = await SharedPreferences.getInstance();
        await prefs.setString('token', token);

        final int nurse_id = responseData['user_data']['id'];
        // print("Nurse ID:$nurse_id");

        // Navigate to dashboard page
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) =>  NurseDash(nurse_id: nurse_id)),
        );
        // Navigator.pushReplacementNamed(context, '/dashboard');
      } else {
        // Login failed, display error message from API
        final Map<String, dynamic> responseData = json.decode(response.body);
        setState(() {
          _errorMessage = responseData['message'];
          _isLoading = false;
        });
      }
    } catch (e) {
      // Error occurred during login
      setState(() {
        _errorMessage = e.toString() ?? 'An error occurred. Please try again later.';
        _isLoading = false;
      });
    }
  }

  // /////// login method end


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        // title: Text("Login Page"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 60.0),
              child: Center(
                child: SizedBox(
                    width: 200,
                    height: 150,
                    /*decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(50.0)),*/
                    child:

                    Image.asset('assets/images/uhai-logo-sample.png')
                ),
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            Padding(
              //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Text(
                _errorMessage,
                style: TextStyle(color: Colors.red),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Padding(
              //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: TextField(
                controller: _phoneController,
                keyboardType: TextInputType.phone,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Phone number',
                  // errorText: _validatePhoneNumber(_phoneController.text) ? null : 'Invalid phone number format',

                ),
              ),
            ),
            const SizedBox(
              height: 50,
            ),
            Padding(
              //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: TextField(
                obscureText: _isObscured,
                controller: _passwordController,
                decoration:  InputDecoration(
                  suffixIcon: IconButton(
                    padding: const EdgeInsetsDirectional.only(end: 12.0),
                    icon: _isObscured ?  Icon(Icons.visibility) : Icon(Icons.visibility_off),
                    onPressed: () {
                        setState(() {
                          _isObscured = !_isObscured;
                        });
                      }

                  ),
                  border: OutlineInputBorder(),
                  labelText: 'Password',

                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 50,
              width: 250,
              decoration: BoxDecoration(
                //
                  color: const Color(0xff18588d), borderRadius: BorderRadius.circular(5)),
              child: TextButton(
                onPressed: _isLoading ? null : _login,
                child: const Text(
                  'Login',
                  style: TextStyle(color: Colors.white, fontSize: 25),
                ),
              ),
            ),
            if (_isLoading)
              Container(
                // color: Colors.black54,
                child: Center(
                  child: Dialog(
                    backgroundColor: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          CircularProgressIndicator(),
                          SizedBox(height: 20),
                          Text(
                            'Logging in...',
                            style: TextStyle(fontSize: 16),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),

            // Text('New User? Create Account')
          ],
        ),
      ),
    );
  }
}

bool _validatePhoneNumber(String phoneNumber) {
  // Regular expression to validate phone number format
  RegExp phoneNumberRegExp = RegExp(r'^\+\d{3}\d{9}$');
  return phoneNumberRegExp.hasMatch(phoneNumber);
}

