import 'dart:convert';
import 'dart:core';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:multi_select_flutter/multi_select_flutter.dart';
import 'allStudents.dart';


class FamilyHistory {
  int id;
  String condition_name;

  FamilyHistory({
    required this.id,
    required this.condition_name,
  });

  factory FamilyHistory.fromJson(Map<String, dynamic> json) {
    return FamilyHistory(
      id: json['condition'],
      condition_name: json['condition_name'] is String ? json['condition_name'] : 'Unknown',
    );
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'condition_name': condition_name,
  };
}

class EditStudFamilyHistory extends StatefulWidget {
  final Student student;

  const EditStudFamilyHistory({Key? key, required this.student}) : super(key: key);

  @override
  _EditStudFamilyHistoryState createState() => _EditStudFamilyHistoryState();
}

class _EditStudFamilyHistoryState extends State<EditStudFamilyHistory> {
  final _familyHistoryCommentController = TextEditingController();

  List<FamilyHistory> _familyHistory = [];
  List<FamilyHistory> _selectedFamilyHistory = [];
  List<FamilyHistory>? _mySelectedFamilyHistory;
  List<Map<String, String>> transformedList = [];

  @override
  void initState() {
    super.initState();
    _fetchFamilyHistoryes();
  }

  Future<void> _fetchFamilyHistoryes() async {
    try {
      final response = await http.get(Uri.parse('https://uhaihealthcare.com/api/v1/school/medical-conditions/condition/2/'));
      if (response.statusCode == 200) {
        List<dynamic> data = json.decode(response.body);
        setState(() {
          _familyHistory = data.map((json) => FamilyHistory.fromJson(json)).toList();
        });
      } else {
        throw Exception('Failed to load family history');
      }
    } catch (e) {
      print('Failed to load family history: $e');
    }
  }

  @override
  void dispose() {
    _familyHistoryCommentController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xff18588d),
        title: Text("${widget.student.first_name}", style: const TextStyle(color: Color(0xffffffff))),
        iconTheme: const IconThemeData(
          color: Color(0xffffffff),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(16.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Edit / Add Family History", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.all(16.0),
              child: _familyHistory.isEmpty
                  ? Center(child: CircularProgressIndicator())
                  : MultiSelectDialogField(
                searchable: true,
                items: _familyHistory.map((familyHistory) => MultiSelectItem<FamilyHistory>(familyHistory, familyHistory.condition_name)).toList(),
                title: const Text("Family History"),
                selectedColor: Theme.of(context).primaryColor,
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor.withOpacity(0.1),
                  border: Border.all(
                    color: Theme.of(context).primaryColor,
                    width: 2,
                  ),
                ),
                buttonIcon: Icon(
                  Icons.add,
                  color: Theme.of(context).primaryColor,
                ),
                buttonText: const Text(
                  "Select Family History",
                  style: TextStyle(
                    color: Color(0xff000000),
                    fontSize: 16,
                  ),
                ),
                onConfirm: (results) {
                  setState(() {
                    _selectedFamilyHistory = List<FamilyHistory>.from(results);
                    _mySelectedFamilyHistory = _selectedFamilyHistory;
                    transformedList = transformFamilyHistoryList(_mySelectedFamilyHistory);

                  });
                },
                chipDisplay: MultiSelectChipDisplay(
                  chipColor: const Color(0xffc5c5c5),
                  textStyle: const TextStyle(
                    color: Color(0xff000000),
                  ),
                  onTap: (value) {
                    setState(() {
                      _selectedFamilyHistory.remove(value);
                    });
                  },
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(16.0),
              child: TextField(
                controller: _familyHistoryCommentController,
                maxLines: 5,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Add comments about the family history',
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(16.0),
              child: ElevatedButton(
                onPressed: () {
                  saveFamilyHistory(context);
                },
                style: ElevatedButton.styleFrom(
                  foregroundColor: const Color(0xffffffff),
                  backgroundColor: const Color(0xff18588d),
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.zero,
                  ),
                ),
                child: const Text('Save family history'),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> saveFamilyHistory(BuildContext context) async {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                CircularProgressIndicator(),
                SizedBox(width: 20),
                Text("Saving data..."),
              ],
            ),
          ),
        );
      },
    );

    Map<String, dynamic> family_history_data = {
      "student_id": widget.student.id,
      'condition_type': 2,
      'selected_conditions': transformedList,
      'condition_comment': _familyHistoryCommentController.text,
    };

    String encodedData = jsonEncode(family_history_data);

    // try catch starts here

    try {
      final response = await http.post(
        Uri.parse('https://uhaihealthcare.com/api/v1/school/add-student-medical-conditions/'),
        headers: {
          'Content-Type': 'application/json',
        },
        body: encodedData,
      );
      Navigator.pop(context);
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Response', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xff18588d))),
            content: Text(jsonDecode(response.body)['message'] ?? '', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xff008000))),
            actions: <Widget>[
              TextButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    } catch (e) {
      Navigator.pop(context);

      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Error', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xff000000))),
            content: Text('An error occurred: $e', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xffff0000))),
            actions: <Widget>[
              TextButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
    //   try catch ends here

  }
}

List<Map<String, String>> transformFamilyHistoryList(List<FamilyHistory>? familyHistoryList) {
  if (familyHistoryList == null) {
    return [];
  }
  return familyHistoryList.map((illness) {
    return {"condition_name": illness.id.toString()};

  }).toList();
}
