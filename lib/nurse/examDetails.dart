import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
// import 'package:uhaiapp/nosql_models/astudent.dart';
import 'package:uhaiapp/nurse/vitalsCards/vitalsCards.dart';
// import 'package:hive/hive.dart';
import '../nosql_models/exam.dart';
import 'allStudents.dart';

class ExamDetails extends StatefulWidget {
  final Exam exam;
  // final Astudent student;
  final Student student;

  const ExamDetails({Key? key, required this.exam, required this.student}) : super(key: key);

  @override
  State<ExamDetails> createState() => _ExamDetailsState();
}

class _ExamDetailsState extends State<ExamDetails> {
  // Add your state variables if needed


  @override
  Widget build(BuildContext context) {
    // Use widget.exam to access the Exam instance
    String? firstname = widget.student.first_name;
    String? lastname = widget.student.last_name!;
    String? initials = (firstname != null && lastname != null) ? "${firstname[0]}${lastname[0]}" : "!";
    // ////////

    // ////// sp02
    String stringValue = widget.exam.oximeter?.firstWhere((element) => element.containsKey('sp02'), orElse: () => {'sp02': null})['sp02']?.toString() ?? 'null';
    int? sp02Value = int.tryParse(stringValue);
    // ///// sp02

    // ////// prbpm
    String stringValue1 = widget.exam.oximeter?.firstWhere((element) => element.containsKey('prbpm'), orElse: () => {'prbpm': null})['prbpm']?.toString() ?? 'null';
    int? prbpmValue = int.tryParse(stringValue1);
    // ///// prbpm

    // ////// sys
    String stringValue2 = widget.exam.bloodPressure?.firstWhere((element) => element.containsKey('sys'), orElse: () => {'sys': null})['sys']?.toString() ?? 'null';
    int? sysValue = int.tryParse(stringValue2);
    // ///// sys

    // ////// sys
    String stringValue3 = widget.exam.bloodPressure?.firstWhere((element) => element.containsKey('dia'), orElse: () => {'dia': null})['dia']?.toString() ?? 'null';
    int? diaValue = int.tryParse(stringValue3);
    // ///// sys

    // ////// sys
    String stringValue4 = widget.exam.bloodPressure?.firstWhere((element) => element.containsKey('pul'), orElse: () => {'pul': null})['pul']?.toString() ?? 'null';
    int? pulValue = int.tryParse(stringValue4);
    // ///// sys

    return Scaffold(
      appBar: AppBar(
        actions: [

          IconButton(
            onPressed: () => showDataPopup(context),

            icon: const Icon(Icons.sync),)
        ],
        backgroundColor: const Color(0xff18588d),
        title: Text("Details for medical exam ID:  ${widget.exam.key}",
            style: const TextStyle(color: Color(0xffffffff))
        ),

        iconTheme: const IconThemeData(
          color: Color(0xffffffff),
        ),
      ),
      body: SingleChildScrollView(
        child: Column (
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      padding: EdgeInsets.all(8.0),
                      color: const Color(0xff949494), // Grey colored heading section
                      width: double.infinity, // Makes the container take full width
                      child: Text(
                        'Bio data',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    SizedBox(height: 10,),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // Container(
                        //   height: 128,
                        //   width: 128,
                        //   color: Colors.grey,
                        //   child: Center(
                        //     child: Text(
                        //       initials.toUpperCase(),
                        //       style: TextStyle(
                        //         fontSize: 80,
                        //         color: const Color(0xff000000),
                        //         fontWeight: FontWeight.bold,
                        //       ),
                        //     ),
                        //   ),
                        // ),
                        SizedBox(width: 20,),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    // Text(
                                    //   widget.student.firstName.toString(),
                                    //   style: TextStyle(
                                    //     fontSize: 24,
                                    //     fontWeight: FontWeight.bold,
                                    //     color: Color(0xff18588d),
                                    //   ),
                                    // ),
                                    SizedBox(width: 40,),
                                    // Text(
                                    //   widget.student.toString(),
                                    //   style: TextStyle(
                                    //     fontSize: 24,
                                    //     fontWeight: FontWeight.bold,
                                    //     color: Color(0xff18588d),
                                    //   ),
                                    // ),
                                  ],
                                ),
                                Container(
                                  child: Text(
                                    "Student ID: ${widget.student.id!}",
                                    style: TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                ),
                                Container(
                                  padding: const EdgeInsets.only(bottom: 2),
                                  child: Text(
                                    widget.student.gender! == "M" ? "MALE" : "FEMALE",
                                    style: TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                ),

                                Container(
                                  child: Text(
                                    "UID:${widget.student.id}",
                                    style: TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                ),

                              ],
                            ),
                          ),
                        ),

                      ],
                    ),

                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: const EdgeInsets.all(8.0),
                      color: const Color(0xff949494), // Grey colored heading section
                      width: double.infinity, // Makes the container take full width
                      child: const Text(
                        'Exam details',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Text("Exam ID: ${widget.exam.key}"),
                          Text("Exam category: ${widget.exam.examCategory}"),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: const EdgeInsets.all(8.0),
                      color: const Color(0xff949494), // Grey colored heading section
                      width: double.infinity, // Makes the container take full width
                      child: const Text(
                        'Vitals',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Wrap(
                        spacing: 8.0, // space between cards horizontally
                        runSpacing: 8.0, // space between cards vertically
                        children: <Widget>[
                          // Add your Card widgets here

                          TempCard(temp: widget.exam.temperature),
                          WeightCard(weight: widget.exam.weight),


                          Card(
                            child: Container(
                              width: 100,
                              height: 60,
                              color: const Color(0xffc8c8c8),
                              child: Column(children: [const SizedBox(height: 8,),const Text('Height:(cm)'),Text('${widget.exam.height}', style: const TextStyle(fontWeight: FontWeight.bold),)]),
                            ),
                          ),
                          GlucoseCard(glucose: widget.exam.glucose),

                          // Add more cards as needed
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),

            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.all(8.0),
                      // color: const Color(0xff949494), // Grey colored heading section
                      width: double.infinity, // Makes the container take full width
                      child: Text(
                        'Oxygyen Saturation Level',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),

                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(


                        width: double.infinity,
                        child: Wrap(
                          spacing: 8.0, // space between cards horizontally
                          runSpacing: 8.0, // space between cards vertically
                          children: <Widget>[
                            // Add your Card widgets here
                            Sp02Card(sp02: sp02Value),

                            PbrmCard(pbrm: prbpmValue),

                            // Add more cards as needed
                          ],
                        ),
                      ),

                    ),

                  ],
                ),
              ),
            ),

            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [

                    Container(
                      padding: EdgeInsets.all(8.0),
                      // color: const Color(0xff949494), // Grey colored heading section
                      width: double.infinity, // Makes the container take full width
                      child: Text(
                        'Blood Pressure',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    // SizedBox(height: 40,),


                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Wrap(
                        spacing: 8.0, // space between cards horizontally
                        runSpacing: 8.0, // space between cards vertically
                        children: <Widget>[
                          // Add your Card widgets here

                          SysCard(sys: sysValue),
                          DiaCard(dia: diaValue),
                          PulCard(pul: pulValue),

                          // Add more cards as needed
                        ],
                      ),
                    ),

                  ],
                ),
              ),
            ),




            Padding(
              padding: EdgeInsets.all(8.0),
              child: ExpansionTile(
                initiallyExpanded:true,
                collapsedBackgroundColor: const Color(0xff949494),
                collapsedTextColor: const Color(0xff000000),
                title: Text('Symptoms', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                trailing: Icon(Icons.expand_more),
                children: <Widget>[
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Wrap(
                        alignment: WrapAlignment.start,
                        spacing: 8.0,
                        children: widget.exam.symptoms.map((symptoms) {
                          return InputChip(
                            onSelected: (bool value) {},
                            backgroundColor: Color(0xffc8c8c8),
                            label: Text(
                              symptoms['symptomName'],
                              style: TextStyle(color: Color(0xff000000)),
                            ),
                            avatar: Icon(
                              Icons.check,
                              size: 20.0,
                              color: Color(0xff000000),
                            ),
                          );
                        }).toList(),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Align(
                        alignment: Alignment.topLeft,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Text("Comment", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                            SizedBox(height: 15,),
                            Text(widget.exam.symptomsComment!),

                          ],

                        )
                    ),

                  )

                ],
              ),
            ),

            Padding(
              padding: EdgeInsets.all(8.0),
              child: ExpansionTile(
                initiallyExpanded:true,
                collapsedBackgroundColor: const Color(0xff949494),
                collapsedTextColor: const Color(0xff000000),
                title: Text('Rapid tests results', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                trailing: Icon(Icons.expand_more),
                children: <Widget>[
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Wrap(
                        alignment: WrapAlignment.start,
                        spacing: 15.0,
                        runSpacing: 15.0,
                        children: [
                          Row(children: [SizedBox(height: 8,),
                            Text('Malaria:'),
                            Text(
                              widget.exam.rapidTests?.firstWhere((element) => element.containsKey('malaria'), orElse: () => {'malaria': null})['malaria']?.toString() ?? 'null',
                              style: TextStyle(fontWeight: FontWeight.bold),)]
                          ),

                          Row(children: [SizedBox(height: 8,),
                            Text('Pregnancy:'),
                            Text(
                              widget.exam.rapidTests?.firstWhere((element) => element.containsKey('pregnancy'), orElse: () => {'pregnancy': null})['pregnancy']?.toString() ?? 'null',
                              style: TextStyle(fontWeight: FontWeight.bold),)]
                          ),

                          Row(children: [SizedBox(height: 8,),
                            Text('Urinary Tract Infections:'),
                            Text(
                              widget.exam.rapidTests?.firstWhere((element) => element.containsKey('pregnancy'), orElse: () => {'pregnancy': null})['pregnancy']?.toString() ?? 'null',
                              style: TextStyle(fontWeight: FontWeight.bold),)]
                          ),
                          Row(children: [SizedBox(height: 8,),
                            Text('HIV:'),
                            Text(
                              widget.exam.rapidTests?.firstWhere((element) => element.containsKey('hiv'), orElse: () => {'hiv': null})['hiv']?.toString() ?? 'null',
                              style: TextStyle(fontWeight: FontWeight.bold),)]
                          ),
                          Row(children: [SizedBox(height: 8,),
                            Text('Typhoid:'),
                            Text(
                              widget.exam.rapidTests?.firstWhere((element) => element.containsKey('typhoid'), orElse: () => {'typhoid': null})['typhoid']?.toString() ?? 'null',
                              style: TextStyle(fontWeight: FontWeight.bold),)]
                          ),

                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),


                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: ExpansionTile(
                initiallyExpanded:true,
                collapsedBackgroundColor: const Color(0xff949494),
                collapsedTextColor: const Color(0xff000000),
                title: Text('Suspected conditions', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                trailing: Icon(Icons.expand_more),
                children: <Widget>[
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Wrap(
                        alignment: WrapAlignment.start,
                        spacing: 8.0,
                        children: widget.exam.diseases.map((diseases) {
                          return InputChip(
                            onSelected: (bool value) {},
                            backgroundColor: Color(0xffc8c8c8),
                            label: Text(
                              diseases['diseaseName'],
                              style: TextStyle(color: Color(0xff000000)),
                            ),
                            avatar: Icon(
                              Icons.check,
                              size: 20.0,
                              color: Color(0xff000000),
                            ),
                          );
                        }).toList(),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Align(
                        alignment: Alignment.topLeft,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Text("Comment", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                            SizedBox(height: 15,),
                            Text(widget.exam.diseasesComment!),

                          ],

                        )
                    ),

                  )

                ],
              ),
            ),

            Padding(
              padding: EdgeInsets.all(8.0),
              child: ExpansionTile(
                initiallyExpanded:true,
                collapsedBackgroundColor: const Color(0xff949494),
                collapsedTextColor: const Color(0xff000000),
                title: Text('Drugs prescribed', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                trailing: Icon(Icons.expand_more),
                children: <Widget>[
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Wrap(
                        alignment: WrapAlignment.start,
                        spacing: 8.0,
                        children: widget.exam.drugs.map((drugs) {
                          return InputChip(
                            onSelected: (bool value) {},
                            backgroundColor: Color(0xffc8c8c8),
                            label: Text(
                              drugs['drugName'],
                              style: TextStyle(color: Color(0xff000000)),
                            ),
                            avatar: Icon(
                              Icons.check,
                              size: 20.0,
                              color: Color(0xff000000),
                            ),
                          );
                        }).toList(),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Align(
                        alignment: Alignment.topLeft,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Text("Comment", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                            SizedBox(height: 15,),
                            Text(widget.exam.drugsComment!),

                          ],

                        )
                    ),

                  )

                ],
              ),
            ),
          ],),
      ),
    );
  }
  void showDataPopup(BuildContext context) async {
    // var box = await Hive.openBox<Exam>('exams'); // Make sure this matches your actual box name
    // var data = box.get(widget.exam.key); // Adjust this key according to your data structure
    var objdata = {
      'exam_id': widget.exam.key,
      'exam_date': widget.exam.createdAt.toString(),
      'student_id': widget.exam.studentId,
      'nurse_id' : widget.exam.nurseId,
      'symptoms' : widget.exam.symptoms,
      'symptoms_comment' : widget.exam.symptomsComment,
      'temperature' : widget.exam.temperature,
      'weight': widget.exam.weight,
      'oximeter': widget.exam.oximeter,
      'blood_pressure': widget.exam.bloodPressure,
      'glucose' : widget.exam.glucose,
      'height' : widget.exam.height,
      'rapid_tests' : widget.exam.rapidTests,
      'exam_category' : widget.exam.examCategory,
      'diseases' : widget.exam.diseases,
      'diseases_comment' : widget.exam.diseasesComment,
      'drugs' : widget.exam.drugs,
      'drugs_comment' : widget.exam.drugsComment


    };


    String jsonData = jsonEncode(objdata);
    print(jsonData);

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Data to be synced online'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Data: $jsonData'), // Customize this based on your data's structure

              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Send to API'),
              onPressed: () {
                // Call your API here with the data
                // For example: sendDataToAPI(data);
                Navigator.of(context).pop(); // Close the dialog
              },
            ),
            TextButton(
              child: const Text('Close'),
              onPressed: () {
                Navigator.of(context).pop(); // Close the dialog
              },
            ),
          ],
        );
      },
    );
  }

  // Future<Unit8List> makeReport() async{
  //   final pdf = pw.Document();
  //
  //   pdf.addPage(
  //     pw.Page(
  //       build: (pw.Context context) {
  //         return pw.Center(
  //           child: pw.Text('Hello World!'),
  //         );
  //       }
  //     ),
  //   );
  //   return pdf.save();
  //   // final file = File('example.pdf');
  //   // await file.writeAsBytes(await pdf.save());
  // }
// Add your methods for handling state changes if necessary
// //////////////




// //////////////
}
