import 'dart:core';

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';
// import 'package:uhaiapp/nosql_models/student.dart';
import 'package:uhaiapp/nosql_models/astudent.dart';
import 'package:uhaiapp/nurse/studts.dart';
import 'dart:convert';
import 'allStudents.dart';
import 'package:http/http.dart' as http;

import '../nosql_models/exam.dart';
import 'direction_message.dart';

class MedicalHistory {
  
  String condition_name;

  MedicalHistory({

    required this.condition_name,
  });

  factory MedicalHistory.fromJson(Map<String, dynamic> json) {
    return MedicalHistory(

      condition_name: json['condition_name'] is String ? json['condition_name'] : 'Unknown',
    );
  }
  // ///// added
  Map<String, dynamic> toJson() => {

    "condition_name": condition_name,
  };


}
// //////////

class EditMedicalHistory extends StatefulWidget {
  // final Astudent student;
  final Student student;

  const EditMedicalHistory({Key? key, required this.student}) : super(key: key);

  @override
  _EditMedicalHistoryState createState() => _EditMedicalHistoryState();
}

class _EditMedicalHistoryState extends State<EditMedicalHistory> {

  final _medicalHistoryCommentController = TextEditingController();

  // ///// hive

  // late Box studentBox;
  late Box <Exam>examBox;
  late Box <Astudent>localstudentBox;


  @override
  void initState() {
    super.initState();

    // studentBox = Hive.box('students');
    examBox = Hive.box('exams');
    localstudentBox = Hive.box('localstudents');


    //   /////blood pressure values change state


  }



  @override
  void dispose() {
    super.dispose();
  }

  final List<MedicalHistory> _medicalHistory = [
    MedicalHistory(condition_name: "Major accident"),
    MedicalHistory(condition_name: "Theater Operation"),
    MedicalHistory(condition_name: "Blood transfusion"),
    MedicalHistory(condition_name: "Tuberculosis"),
    MedicalHistory(condition_name: "Anemia"),
    MedicalHistory(condition_name: "ENT (Ear, Nose and Throat) condition"),
    MedicalHistory(condition_name: "Uterine Fibroid (female students)"),
    MedicalHistory(condition_name: "Physical disability"),
    MedicalHistory(condition_name: "Mental disability"),
    MedicalHistory(condition_name: "Long hospitalization (more than a week)"),


  ];

  List<MedicalHistory> _selectedMedicalHistory = [];
  String? _mySelectedMedicalHistory;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xff18588d),
        title: Text("${widget.student.first_name}", style: const TextStyle(color: Color(0xffffffff))),
        iconTheme: const IconThemeData(
          color: Color(0xffffffff),
        ),
      ),
      body: SingleChildScrollView(
        // scrollDirection: Axis.vertical,
        child:
        Column(
          children: [
            Padding(
              padding: EdgeInsets.all(16.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                // alignment: ,
                children: [
                  Text("Edit / Add Medical History", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                ]

              )

            ),

            // Padding(
            //   padding: EdgeInsets.all(16.0),
            //   child: TextFormField(
            //     readOnly: true,
            //     initialValue: 'Medical History', // Set your pre-filled value here
            //     decoration: InputDecoration(
            //       labelText: 'Condition type',
            //       border: OutlineInputBorder(),
            //     ),
            //   ),
            // ),

            Padding(
                padding: EdgeInsets.all(16.0),
                child: MultiSelectDialogField(
                  searchable: true,
                  items: _medicalHistory.map((medicalHistory) => MultiSelectItem<MedicalHistory>(medicalHistory, medicalHistory.condition_name)).toList(),
                  title: const Text("Medical History"),
                  selectedColor: Theme.of(context).primaryColor,
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor.withOpacity(0.1),
                    // borderRadius: BorderRadius.all(Radius.circular(0)),
                    border: Border.all(
                      color: Theme.of(context).primaryColor,
                      width: 2,
                    ),
                  ),
                  buttonIcon: Icon(
                    Icons.add,
                    color: Theme.of(context).primaryColor,
                  ),
                  buttonText: const Text(
                    "Select Medical History",
                    style: TextStyle(
                      color: Color(0xff000000),
                      fontSize: 16,
                    ),
                  ),
                  onConfirm: (results) {
                    setState(() {
                      // List<Symptoms> symptomsFromJson(String str) => List<Symptoms>.from(json.decode(str).map((x) => Symptoms.fromJson(x)));
                      _selectedMedicalHistory = List<MedicalHistory>.from(results);
                      String medicalHistoryToJson(List<MedicalHistory> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));
                      final myMedicalHistory = medicalHistoryToJson(_selectedMedicalHistory);
                      _mySelectedMedicalHistory = myMedicalHistory as String?;
                      print(_mySelectedMedicalHistory);
                    });

                  },
                  chipDisplay: MultiSelectChipDisplay(
                    chipColor: const Color(0xffc5c5c5),
                    textStyle: const TextStyle(
                      color: Color(0xff000000),
                    ),
                    onTap: (value) {
                      setState(() {
                        _selectedMedicalHistory.remove(value);
                      });
                    },
                  ),
                ),
            ),

            Padding(
              padding: EdgeInsets.all(16.0),
              child: TextField(
                controller: _medicalHistoryCommentController,
                maxLines: 5,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Add comments about the medical history'
                ),
              ),
            ),

            Padding(
              padding: EdgeInsets.all(16.0),
              child: ElevatedButton(
                onPressed: () {
                  saveMedicalHistory(context);
                },
                style: ElevatedButton.styleFrom(
                  foregroundColor: const Color(0xffffffff),
                  backgroundColor: const Color(0xff18588d),
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.zero, // No rounded corners
                  ),
                ),
                child: const Text('Save medical history'),
              ),
            ),

          ],
        ),


      ),
    );
  }

  // void saveMedicalHistory(BuildContext context) {
    Future<void> saveMedicalHistory(BuildContext context) async {
    //
      showDialog(
        context: context,
        barrierDismissible: false, // Prevent dialog from closing on tap
        builder: (BuildContext context) {
          return Dialog(
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  CircularProgressIndicator(),
                  SizedBox(width: 20),
                  Text("Saving data..."),
                ],
              ),
            ),
          );
        },
      );

      // Prepare the data for saving
      Map<String, dynamic> medical_history_data = {
        'student_id': widget.student.id,
        'condition_type': "Medical History",
        'selected_conditions' : jsonDecode(_mySelectedMedicalHistory ?? ''),
        'condition_comment': _medicalHistoryCommentController.text,

      };

      String encodedData = jsonEncode(medical_history_data);

      // //////////////////
      try {
        // Send data to server
        final response = await http.post(
          Uri.parse('https://uhaihealthcare.com/api/v1/school/add-student-medical-conditions/'),
          headers: {
            'Content-Type': 'application/json',
          },
          body: encodedData,
        );

        // Close initial dialog
        Navigator.pop(context);

        // Show response dialog
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Response' , style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xff18588d)),),
              content: Text(jsonDecode(response.body)['message'] ?? '', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xff008000)),),

              actions: <Widget>[
                TextButton(
                  child: Text('OK'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      } catch (e) {
        // Close initial dialog
        Navigator.pop(context);

        // Show error dialog
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Error', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xff000000)),),
              content: Text('An error occurred: $e', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xffff0000)),),

              actions: <Widget>[
                TextButton(
                  child: Text('OK'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      }
        //   //////////////////
        

  }




}
