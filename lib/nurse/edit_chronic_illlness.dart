import 'dart:core';

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';
// import 'package:uhaiapp/nosql_models/student.dart';
import 'package:uhaiapp/nosql_models/astudent.dart';
import 'package:uhaiapp/nurse/studts.dart';
import 'dart:convert';
import 'allStudents.dart';
import 'package:http/http.dart' as http;

import '../nosql_models/exam.dart';
import 'direction_message.dart';

class ChronicIllness {

  String condition_name;

  ChronicIllness({

    required this.condition_name,
  });

  factory ChronicIllness.fromJson(Map<String, dynamic> json) {
    return ChronicIllness(

      condition_name: json['condition_name'] is String ? json['condition_name'] : 'Unknown',
    );
  }
  // ///// added
  Map<String, dynamic> toJson() => {

    "condition_name": condition_name,
  };


}
// //////////

class EditChronicIllness extends StatefulWidget {
  // final Astudent student;
  final Student student;

  const EditChronicIllness({Key? key, required this.student}) : super(key: key);

  @override
  _EditChronicIllnessState createState() => _EditChronicIllnessState();
}

class _EditChronicIllnessState extends State<EditChronicIllness> {

  final _chronicIllnessCommentController = TextEditingController();

  // ///// hive

  // late Box studentBox;
  late Box <Exam>examBox;
  late Box <Astudent>localstudentBox;


  @override
  void initState() {
    super.initState();

    // studentBox = Hive.box('students');
    examBox = Hive.box('exams');
    localstudentBox = Hive.box('localstudents');


    //   /////blood pressure values change state


  }



  @override
  void dispose() {
    super.dispose();
  }

  final List<ChronicIllness> _chronicIllness = [
    ChronicIllness(condition_name: "Diabetes"),
    ChronicIllness(condition_name: "Ulcers (PUD)"),
    ChronicIllness(condition_name: "Sickle Cell Disease"),
    ChronicIllness(condition_name: "Sickle Cell Trait (Carrier)"),
    ChronicIllness(condition_name: "Asthma"),
    ChronicIllness(condition_name: "Hypertension"),
    ChronicIllness(condition_name: "Eye condition"),
    ChronicIllness(condition_name: "Epilepsy"),
    ChronicIllness(condition_name: "Psychiatric disorders"),
    ChronicIllness(condition_name: "Autism Spectrum Disorder (ASD) "),
    ChronicIllness(condition_name: "Sleep disorder"),
    ChronicIllness(condition_name: "COPD - Chronic Obstructive Pulmonary Disease "),
    ChronicIllness(condition_name: "HIV/ AIDS"),
    ChronicIllness(condition_name: "Hepatitis ("),
    ChronicIllness(condition_name: "Metabolic syndrome"),
    ChronicIllness(condition_name: "Thyroid hormone resistance"),
    ChronicIllness(condition_name: "Heart Disease"),
    ChronicIllness(condition_name: "Chronic Kidney Disease"),

  ];

  List<ChronicIllness> _selectedChronicIllness = [];
  String? _mySelectedChronicIllness;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xff18588d),
        title: Text("${widget.student.first_name}", style: const TextStyle(color: Color(0xffffffff))),
        iconTheme: const IconThemeData(
          color: Color(0xffffffff),
        ),
      ),
      body: SingleChildScrollView(
        // scrollDirection: Axis.vertical,
        child:
        Column(
          children: [
            Padding(
              padding: EdgeInsets.all(16.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                // alignment: ,
                children: [
                  Text("Edit / Add Chronic Illness", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                ]

              )

            ),

            Padding(
                padding: EdgeInsets.all(16.0),
                child: MultiSelectDialogField(
                  searchable: true,
                  items: _chronicIllness.map((chronicIllness) => MultiSelectItem<ChronicIllness>(chronicIllness, chronicIllness.condition_name)).toList(),
                  title: const Text("Chronic Illness"),
                  selectedColor: Theme.of(context).primaryColor,
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor.withOpacity(0.1),
                    // borderRadius: BorderRadius.all(Radius.circular(0)),
                    border: Border.all(
                      color: Theme.of(context).primaryColor,
                      width: 2,
                    ),
                  ),
                  buttonIcon: Icon(
                    Icons.add,
                    color: Theme.of(context).primaryColor,
                  ),
                  buttonText: const Text(
                    "Select Chronic Illness",
                    style: TextStyle(
                      color: Color(0xff000000),
                      fontSize: 16,
                    ),
                  ),
                  onConfirm: (results) {
                    setState(() {
                      // List<Symptoms> symptomsFromJson(String str) => List<Symptoms>.from(json.decode(str).map((x) => Symptoms.fromJson(x)));
                      _selectedChronicIllness = List<ChronicIllness>.from(results);
                      String chronicToJson(List<ChronicIllness> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));
                      final myChronicIllness = chronicToJson(_selectedChronicIllness);
                      _mySelectedChronicIllness = myChronicIllness as String?;
                      print(_mySelectedChronicIllness);
                    });

                  },
                  chipDisplay: MultiSelectChipDisplay(
                    chipColor: const Color(0xffc5c5c5),
                    textStyle: const TextStyle(
                      color: Color(0xff000000),
                    ),
                    onTap: (value) {
                      setState(() {
                        _selectedChronicIllness.remove(value);
                      });
                    },
                  ),
                ),
            ),

            Padding(
              padding: EdgeInsets.all(16.0),
              child: TextField(
                controller: _chronicIllnessCommentController,
                maxLines: 5,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Add comments about the chronic illness'
                ),
              ),
            ),

            Padding(
              padding: EdgeInsets.all(16.0),
              child: ElevatedButton(
                onPressed: () {
                  saveChronicIllness(context);
                },
                style: ElevatedButton.styleFrom(
                  foregroundColor: const Color(0xffffffff),
                  backgroundColor: const Color(0xff18588d),
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.zero, // No rounded corners
                  ),
                ),
                child: const Text('Save chronic illness'),
              ),
            ),

          ],
        ),


      ),
    );
  }

  // void saveChronicIllness(BuildContext context) {
    Future<void> saveChronicIllness(BuildContext context) async {
    //
      showDialog(
        context: context,
        barrierDismissible: false, // Prevent dialog from closing on tap
        builder: (BuildContext context) {
          return Dialog(
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  CircularProgressIndicator(),
                  SizedBox(width: 20),
                  Text("Saving data..."),
                ],
              ),
            ),
          );
        },
      );

      // Prepare the data for saving
      Map<String, dynamic> chronic_illness_data = {
        'student_id': widget.student.id,
        'condition_type': "Chronic Illness",
        'selected_conditions' : jsonDecode(_mySelectedChronicIllness ?? ''),
        'condition_comment': _chronicIllnessCommentController.text,

      };

      String encodedData = jsonEncode(chronic_illness_data);

      // ////////////////////

      try {
        // Send data to server
        final response = await http.post(
          Uri.parse('https://uhaihealthcare.com/api/v1/school/add-student-medical-conditions/'),
          headers: {
            'Content-Type': 'application/json',
          },
          body: encodedData,
        );

        // Close initial dialog
        Navigator.pop(context);

        // Show response dialog
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Response' , style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xff18588d)),),
              content: Text(jsonDecode(response.body)['message'] ?? '', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xff008000)),),

              actions: <Widget>[
                TextButton(
                  child: Text('OK'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      } catch (e) {
        // Close initial dialog
        Navigator.pop(context);

        // Show error dialog
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Error', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xff000000)),),
              content: Text('An error occurred: $e', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xffff0000)),),

              actions: <Widget>[
                TextButton(
                  child: Text('OK'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      }

      // ///////////////////

  }

}
