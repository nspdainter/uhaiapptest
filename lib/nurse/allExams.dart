import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:uhaiapp/nosql_models/exam.dart';
//
import 'package:intl/intl.dart';
import 'examDetails.dart';
import 'allStudents.dart';

class AllExams extends StatefulWidget {
final Student student;

const AllExams({
    Key? key,
  required this.student,
  }) : super(key: key);

  @override
  _AllExamsState createState() => _AllExamsState();
}

class _AllExamsState extends State<AllExams> with SingleTickerProviderStateMixin {
  late TabController _tabController;

  late Box<Exam> examBox;



  @override
  void initState() {
    super.initState();
    examBox = Hive.box<Exam>('exams');
    _tabController = TabController(length: 5, vsync: this);
    // Update the UI when the tab changes
    _tabController.addListener(() {
      // Force widget rebuild on tab change
      if (!mounted) return; // Check if the widget is still in the widget tree
      setState(() {
        // The UI will rebuild, and the FloatingActionButton's visibility will be updated
      });
    });
  }

  @override
  void dispose() {
    _tabController.dispose(); // Dispose of the TabController to clean up resources
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // final Student student = studentBox.getAt(index) as Student;

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(70.0),
        child: AppBar(

        backgroundColor: const Color(0xff18588d),
        title:
        const Row(

          children: [
            Text(
                "All medical examinations",
                style: TextStyle(color: Color(0xffffffff))
            ),


          ],
        ),

        // actions: <Widget>[
        //   Padding(
        //     padding: EdgeInsets.only(bottom: 20.0), // Adjust the padding as needed
        //     child: PopupMenuButton<String>(
        //       onSelected: (String result) {
        //         // Handle the selection
        //         print(result);
        //       },
        //       itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
        //         PopupMenuItem<String>(
        //           value: 'Option 1',
        //           child: Text('All'),
        //         ),
        //         PopupMenuItem<String>(
        //           value: 'Option 2',
        //           child: Text('Pending'),
        //         ),
        //         PopupMenuItem<String>(
        //           value: 'Option 3',
        //           child: Text('Reviewed'),
        //         ),
        //       ],
        //     ),
        //   ),
        // ],
        iconTheme: const IconThemeData(
          color: Color(0xffffffff),
        ),
      ),

      ),


      body: SingleChildScrollView(
            child: Column (
              children: [
                // search bar start
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextField(
                    // controller: searchController,
                    decoration: const InputDecoration(
                      labelText: 'Search',
                      suffixIcon: Icon(Icons.search),
                    ),
                    onChanged: (value) {
                      // setState(() {
                      //   searchQuery = value.toLowerCase();
                      // });
                    },
                  ),
                ),
                // search bar end


                //   //////list the conducted exams here
                Padding(padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                      height: 900,
                      child: ListView.builder(
                          itemCount: examBox.length,
                          itemBuilder: ((context, index){
                            final exam = examBox.getAt(index) as Exam;
                            return Card(
                              shape: const RoundedRectangleBorder( // Specify the shape of the Card
                                borderRadius: BorderRadius.zero, // Set borderRadius to zero for sharp corners
                              ),
                              elevation: 2.0,
                              // margin: EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [ // Card Header
                                  Container(
                                    padding: const EdgeInsets.all(8.0),
                                    color: const Color(0xffd9d9d9),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          'Student ID: ${exam.studentId}',
                                          style: const TextStyle(
                                            fontWeight: FontWeight.bold, // Makes the text bold
                                            fontSize: 12.0, // Increase the font size
                                            color: Colors.black, // Darker blue color for the text
                                          ),
                                        ),
                                        // const SizedBox(width: 100,),

                                        Text(
                                          'Name:${exam.studentId}',
                                          style: const TextStyle(
                                            fontWeight: FontWeight.bold, // Makes the text bold
                                            fontSize: 12.0, // Increase the font size
                                            color: Colors.black, // Darker blue color for the text
                                          ),
                                        ),

                                        Container(
                                          child: IconButton(
                                            icon: Icon(Icons.delete),
                                            onPressed: () {
                                              showDialog(
                                                context: context,
                                                builder: (BuildContext context) {
                                                  return AlertDialog(
                                                    title: Text(
                                                        "Confirm Deletion",
                                                      style: TextStyle(
                                                        color: Colors.red, // Red color for the title
                                                        fontWeight: FontWeight.bold, // Bold font weight for the title
                                                      ),),
                                                    content: Text(
                                                        "Are you sure you want to delete this medical exam?",
                                                      style: TextStyle(
                                                        color: Colors.red, // Red color for the title
                                                        fontWeight: FontWeight.bold, // Bold font weight for the title
                                                      ),),
                                                    actions: <Widget>[
                                                      TextButton(
                                                        child: Text("Cancel"),
                                                        onPressed: () {
                                                          Navigator.of(context).pop(); // Close the dialog
                                                        },
                                                      ),
                                                      TextButton(
                                                        child: Text("Delete"),
                                                        onPressed: () async {
                                                          // Replace 'your_key' with the key of the item you want to delete
                                                          examBox.delete(exam.key);
                                                          Navigator.of(context).pop(); // Close the dialog
                                                          ScaffoldMessenger.of(context).showSnackBar(
                                                            SnackBar(
                                                              backgroundColor: Colors.green,
                                                              content: Text(
                                                                  "Medical exam deleted successfully",
                                                                style: TextStyle(
                                                                  color: Colors.white, // White color for the SnackBar text
                                                                ),
                                                              ),
                                                            ),
                                                          );
                                                          setState(() {});
                                                        },
                                                      ),
                                                    ],
                                                  );
                                                },
                                              );
                                            },
                                            color: Colors.grey, // You can customize the color
                                            // iconSize: 50, // You can customize the size
                                          ),
                                        )



                                      ],
                                    ),
                                  ),

                                  Container(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(
                                      children: [
                                        ListTile(
                                          leading: Column(
                                              children: [
                                                const Text("Exam ID:"),
                                                // Text(exam.examId.toString()),
                                                Text(exam.key.toString()),
                                              ]
                                          ),

                                          title:
                                          Row(
                                            mainAxisSize: MainAxisSize.min,
                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                            children: [
                                              Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  const Text("Created:"),
                                                  Text(formatDateTime(exam.createdAt).toString()),
                                                ],
                                              ),
                                              // Column(
                                              //   crossAxisAlignment: CrossAxisAlignment.start,
                                              //   children: [
                                              //     const Text("Reviewed:"),
                                              //     Text(formatDateTime(exam.createdAt).toString()),
                                              //   ],
                                              // ),
                                            ]

                                          ),



                                          trailing: OutlinedButton(
                                            onPressed: () {


                                              Navigator.of(context).push(
                                                MaterialPageRoute(
                                                  builder: (context) => ExamDetails(exam: exam, student: widget.student,),
                                                ),
                                              );


                                            },
                                            style: OutlinedButton.styleFrom(
                                              side: const BorderSide(color: Color(0xffd3d3d3), width: 2.0),
                                              foregroundColor: const Color(0xff000000),

                                              shape: const RoundedRectangleBorder(
                                                borderRadius: BorderRadius.zero, // No rounded corners
                                              ),

                                            ),
                                            child: const Text('Details'),
                                          ),

                                        ),



                                      ],
                                    ),
                                  )

                                ],
                              ),
                            );
                          }
                          )

                      )

                  ),

                ),
                //   //////list the conducted exam here

              ],),
          ),


      // floatingActionButton: _tabController.index == 1 ? FloatingActionButton.extended(
      //   foregroundColor: const Color(0xffffffff),
      //   backgroundColor: const Color(0xff18588d),
      //   label: Text("New Exam"),
      //   icon: Icon(Icons.add),
      //   onPressed: () {
      //     Navigator.push(
      //       context,
      //       MaterialPageRoute(
      //         builder: (context) => ConductExam(student: widget.student),
      //       ),
      //     );
      //   },
      //
      // ) : null,
    );
  }

  String formatDateTime(DateTime dateTime) {
    // Create a DateFormat with the desired format
    final DateFormat formatter = DateFormat('yyyy-MM-dd HH:mm');
    // Return the formatted date and time string
    return formatter.format(dateTime);
  }

  int calculateAge(DateTime birthdate) {
    DateTime currentDate = DateTime.now();
    int age = currentDate.year - birthdate.year;
    if (birthdate.month > currentDate.month ||
        (birthdate.month == currentDate.month && birthdate.day > currentDate.day)) {
      age--;
    }
    return age;
  }

//   //////// delete exam start


  // void _showDeleteConfirmationDialog(BuildContext context) {
  //   showDialog(
  //     context: context,
  //     builder: (BuildContext context) {
  //       return AlertDialog(
  //         title: Text("Confirm Deletion"),
  //         content: Text("Are you sure you want to delete this item?"),
  //         actions: <Widget>[
  //           TextButton(
  //             child: Text("Cancel"),
  //             onPressed: () {
  //               Navigator.of(context).pop(); // Close the dialog
  //             },
  //           ),
  //           TextButton(
  //             child: Text("Delete"),
  //             onPressed: () async {
  //               // Replace 'your_key' with the key of the item you want to delete
  //               await _deleteItemFromBox('your_key');
  //               Navigator.of(context).pop(); // Close the dialog
  //               ScaffoldMessenger.of(context).showSnackBar(
  //                 SnackBar(
  //                   content: Text("Item deleted successfully"),
  //                 ),
  //               );
  //             },
  //           ),
  //         ],
  //       );
  //     },
  //   );
  // }

  // Future<void> _deleteItemFromBox(String key) async {
  //   // var box = await Hive.openBox('your_box_name');
  //   await examBox.delete();
  //   // examBox.close();
  // }


// /////// delete exam end

}




// temperature preview code ends here