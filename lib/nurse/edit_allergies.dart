import 'dart:core';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';
// import 'package:uhaiapp/nosql_models/student.dart';
import 'package:uhaiapp/nosql_models/astudent.dart';
import 'package:uhaiapp/nurse/studts.dart';
import 'dart:convert';
import 'allStudents.dart';

import '../nosql_models/exam.dart';
import 'direction_message.dart';

class Allergies {
  // int condition_id;
  String condition_name;

  Allergies({
    // required this.condition_id,
    required this.condition_name,
  });

  factory Allergies.fromJson(Map<String, dynamic> json) {
    return Allergies(
      // condition_id: json['condition_id'] is int ? json['condition_id'] : 0,
      condition_name: json['condition_name'] is String ? json['condition_name'] : 'Unknown',
    );
  }
  // ///// added
  Map<String, dynamic> toJson() => {
    // "condition_id": condition_id,
    "condition_name": condition_name,
  };


}
// //////////

class EditAllergies extends StatefulWidget {
  // final Astudent student;
  final Student student;

  const EditAllergies({Key? key, required this.student}) : super(key: key);

  @override
  _EditAllergiesState createState() => _EditAllergiesState();
}

class _EditAllergiesState extends State<EditAllergies> {

  final _allergiesCommentController = TextEditingController();

  // ///// hive

  // late Box studentBox;
  late Box <Exam>examBox;
  late Box <Astudent>localstudentBox;


  @override
  void initState() {
    super.initState();

    // studentBox = Hive.box('students');
    examBox = Hive.box('exams');
    localstudentBox = Hive.box('localstudents');


    //   /////blood pressure values change state


  }



  @override
  void dispose() {
    super.dispose();
  }

  // final List<Allergies> _allergies = [
  //   Allergies(condition_id: 1, condition_name: "Dust, Pollen (trees, grass, weeds etc.) "),
  //   Allergies(condition_id: 2, condition_name: "Pets (cats, dogs etc.)"),
  //   Allergies(condition_id: 3, condition_name: "Food"),
  //   Allergies(condition_id: 4, condition_name: "Latex (Plastics and rubbers)"),
  //   Allergies(condition_id: 5, condition_name: "Medicines"),
  //
  // ];
  final List<Allergies> _allergies = [
    Allergies(condition_name: "Dust, Pollen (trees, grass, weeds etc.) "),
    Allergies(condition_name: "Pets (cats, dogs etc.)"),
    Allergies(condition_name: "Food"),
    Allergies(condition_name: "Latex (Plastics and rubbers)"),
    Allergies(condition_name: "Medicines"),

  ];

  List<Allergies> _selectedAllergies = [];
  String? _mySelectedAllergies;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xff18588d),
        title: Text("${widget.student.first_name}", style: const TextStyle(color: Color(0xffffffff))),
        iconTheme: const IconThemeData(
          color: Color(0xffffffff),
        ),
      ),
      body: SingleChildScrollView(
        // scrollDirection: Axis.vertical,
        child:
        Column(
          children: [
            Padding(
              padding: EdgeInsets.all(16.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                // alignment: ,
                children: [
                  Text("Edit / Add Allergies", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                ]

              )

            ),


            Padding(
                padding: EdgeInsets.all(16.0),
                child: MultiSelectDialogField(
                  searchable: true,
                  items: _allergies.map((allergies) => MultiSelectItem<Allergies>(allergies, allergies.condition_name)).toList(),
                  title: const Text("Allergies"),
                  selectedColor: Theme.of(context).primaryColor,
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor.withOpacity(0.1),
                    // borderRadius: BorderRadius.all(Radius.circular(0)),
                    border: Border.all(
                      color: Theme.of(context).primaryColor,
                      width: 2,
                    ),
                  ),
                  buttonIcon: Icon(
                    Icons.add,
                    color: Theme.of(context).primaryColor,
                  ),
                  buttonText: const Text(
                    "Select Allergies",
                    style: TextStyle(
                      color: Color(0xff000000),
                      fontSize: 16,
                    ),
                  ),
                  onConfirm: (results) {
                    setState(() {
                      // List<Symptoms> symptomsFromJson(String str) => List<Symptoms>.from(json.decode(str).map((x) => Symptoms.fromJson(x)));
                      _selectedAllergies = List<Allergies>.from(results);
                      String allergiesToJson(List<Allergies> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));
                      final myAllergies = allergiesToJson(_selectedAllergies);
                      _mySelectedAllergies = myAllergies as String?;
                      print(_mySelectedAllergies);
                    });

                  },
                  chipDisplay: MultiSelectChipDisplay(
                    chipColor: const Color(0xffc5c5c5),
                    textStyle: const TextStyle(
                      color: Color(0xff000000),
                    ),
                    onTap: (value) {
                      setState(() {
                        _selectedAllergies.remove(value);
                      });
                    },
                  ),
                ),
            ),

            Padding(
              padding: EdgeInsets.all(16.0),
              child: TextField(
                controller: _allergiesCommentController,
                maxLines: 5,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Add comments about the allergies'
                ),
              ),
            ),

            Padding(
              padding: EdgeInsets.all(16.0),
              child: ElevatedButton(
                onPressed: () {
                  saveAllergies(context);
                },
                style: ElevatedButton.styleFrom(
                  foregroundColor: const Color(0xffffffff),
                  backgroundColor: const Color(0xff18588d),
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.zero, // No rounded corners
                  ),
                ),
                child: const Text('Save allergies'),
              ),
            ),

          ],
        ),


      ),
    );
  }

  // void saveAllergies(BuildContext context) {
  Future<void> saveAllergies(BuildContext context) async {
    // Show initial dialog
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                CircularProgressIndicator(),
                SizedBox(width: 20),
                Text("Saving data..."),
              ],
            ),
          ),
        );
      },
    );

    // Prepare the data for saving
    Map<String, dynamic> chronicIllnessData = {
      'student_id': widget.student.id,
      'condition_type': "Allergies",
      'selected_conditions': jsonDecode(_mySelectedAllergies ?? ''),
      'condition_comment': _allergiesCommentController.text,
    };

    // Encode data to JSON
    String encodedData = jsonEncode(chronicIllnessData);
    print(encodedData);

    try {
      // Send data to server
      final response = await http.post(
        Uri.parse('https://uhaihealthcare.com/api/v1/school/add-student-medical-conditions/'),
        headers: {
          'Content-Type': 'application/json',
        },
        body: encodedData,
      );

      // Close initial dialog
      Navigator.pop(context);

      // Show response dialog
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Response' , style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xff18588d)),),
            content: Text(jsonDecode(response.body)['message'] ?? '', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xff008000)),),

            actions: <Widget>[
              TextButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    } catch (e) {
      // Close initial dialog
      Navigator.pop(context);

      // Show error dialog
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Error', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xff000000)),),
            content: Text('An error occurred: $e', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xffff0000)),),

            actions: <Widget>[
              TextButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }

  }

}
