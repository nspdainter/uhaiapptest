import 'dart:convert';

import 'package:flutter/material.dart';
// import 'package:hive/hive.dart';
import '../nosql_models/exam.dart';

class ExamDetail extends StatefulWidget {
  final Exam exam;

  const ExamDetail({Key? key, required this.exam}) : super(key: key);

  @override
  State<ExamDetail> createState() => _ExamDetailState();
}

class _ExamDetailState extends State<ExamDetail> {
  // Add your state variables if needed


  @override
  Widget build(BuildContext context) {
    // Use widget.exam to access the Exam instance

    // ////////
    return Scaffold(
      appBar: AppBar(
        actions: [

          IconButton(
            onPressed: () => showDataPopup(context),

            icon: const Icon(Icons.sync),)
        ],
        backgroundColor: const Color(0xff18588d),
        title: Text("Details for medical exam for ${widget.exam.key}",
            style: const TextStyle(color: Color(0xffffffff))
        ),

        iconTheme: const IconThemeData(
          color: Color(0xffffffff),
        ),
      ),
      body: SingleChildScrollView(
        child: Column (
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: const EdgeInsets.all(8.0),
                      color: const Color(0xff949494), // Grey colored heading section
                      width: double.infinity, // Makes the container take full width
                      child: const Text(
                        'Bio data',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ListTile(
                        leading: Container(
                          width: 120, // Width of the rectangular avatar
                          height: 120, // Height of the rectangular avatar
                          decoration: const BoxDecoration(
                            color: Color(0xffc8c8c8), // Background color of the rectangular avatar
                            // shape: BoxShape.rectangle, // Shape of the avatar
                          ),
                          child: const Center(
                            child: Text(
                              ""
                              // initials.toUpperCase(), style: TextStyle(fontSize: 20, color: const Color(0xff000000), fontWeight: FontWeight.bold), // Initial or any other content for the avatar
                            ),
                          ),
                        ),
                        title: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(children: [
                              Text(widget.exam.studentId, ),

                            ],)

                          ],
                        ),
                        subtitle: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(widget.exam.createdAt.toString(),),
                            

                          ],
                        ),
                        trailing: Column(
                          children: [
                            const Text("Exam category"),
                            Text(widget.exam.examCategory),
                          ],
                        ),

                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: const EdgeInsets.all(8.0),
                      color: const Color(0xff949494), // Grey colored heading section
                      width: double.infinity, // Makes the container take full width
                      child: const Text(
                        'Vitals',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Wrap(
                        spacing: 8.0, // space between cards horizontally
                        runSpacing: 8.0, // space between cards vertically
                        children: <Widget>[
                          // Add your Card widgets here
                          Card(
                            child: Container(
                              width: 100,
                              height: 80,
                              color: const Color(0xffc8c8c8),
                              child: Column(children: [const SizedBox(height: 8,),const Text('Temp:(°C)'),Text('${widget.exam.temperature}', style: const TextStyle(fontWeight: FontWeight.bold),)]),
                            ),
                          ),
                          Card(
                            child: Container(
                              width: 100,
                              height: 60,
                              color: const Color(0xffc8c8c8),
                              child: Column(children: [const SizedBox(height: 8,),const Text('Weight:(kgs)'),Text('${widget.exam.weight}', style: const TextStyle(fontWeight: FontWeight.bold),)]),
                            ),
                          ),
                          Card(
                            child: Container(
                              width: 100,
                              height: 60,
                              color: const Color(0xffc8c8c8),
                              child: Column(children: [const SizedBox(height: 8,),const Text('Height:(cm)'),Text('${widget.exam.height}', style: const TextStyle(fontWeight: FontWeight.bold),)]),
                            ),
                          ),
                          Card(
                            child: Container(
                              width: 120,
                              height: 80,
                              color: const Color(0xffc8c8c8),
                              child: Column(children: [const SizedBox(height: 8,),const Text('Glucose:(mg/dL)'),Text('${widget.exam.glucose}', style: const TextStyle(fontWeight: FontWeight.bold),)]),
                            ),
                          ),
                          Card(
                            child: Container(
                              width: 100,
                              height: 60,
                              color: const Color(0xffc8c8c8),
                              child: const Column(children: [SizedBox(height: 8,),Text('Sp02:'),Text('180', style: TextStyle(fontWeight: FontWeight.bold),)]),
                            ),
                          ),
                          Card(
                            child: Container(
                              width: 100,
                              height: 60,
                              color: const Color(0xffc8c8c8),
                              child: const Column(children: [SizedBox(height: 8,),Text('PRbpm:'),Text('83', style: TextStyle(fontWeight: FontWeight.bold),)]),
                            ),
                          ),

                          Card(
                            child: Container(
                              width: 100,
                              height: 60,
                              color: const Color(0xffc8c8c8),
                              child: const Column(children: [SizedBox(height: 8,),Text('SYS:'),Text('62 mmHg', style: TextStyle(fontWeight: FontWeight.bold),)]),
                            ),
                          ),
                          Card(
                            child: Container(
                              width: 100,
                              height: 60,
                              color: const Color(0xffc8c8c8),
                              child: const Column(children: [SizedBox(height: 8,),Text('DIA:'),Text('176 mmHg', style: TextStyle(fontWeight: FontWeight.bold),)]),
                            ),
                          ),
                          Card(
                            child: Container(
                              width: 100,
                              height: 60,
                              color: const Color(0xffc8c8c8),
                              child: const Column(children: [SizedBox(height: 8,),Text('PUL:'),Text('17 /min', style: TextStyle(fontWeight: FontWeight.bold),)]),
                            ),
                          ),
                          // Add more cards as needed
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),

          //   symptoms

            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: const EdgeInsets.all(8.0),
                      color: const Color(0xff949494), // Grey colored heading section
                      width: double.infinity, // Makes the container take full width
                      child: const Text(
                        'Symptoms',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),

                  ],
                ),
              ),
            ),

            SizedBox(
              height: 100, // Provide a specific height
              child: Column(
                children: <Widget>[
                  Expanded(
                    child:  ListView.builder(
                    itemCount: widget.exam.symptoms.length,
                    itemBuilder: (context, index) {
                  return ListTile(
                    title: Text('${widget.exam.symptoms[index]['symptomName']}'),

                  );
                },
              ),
                  ),
                ],
              ),
            ),

            // Suspected conditions

            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: const EdgeInsets.all(8.0),
                      color: const Color(0xff949494), // Grey colored heading section
                      width: double.infinity, // Makes the container take full width
                      child: const Text(
                        'Suspected conditions',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),

                  ],
                ),
              ),
            ),

            SizedBox(
              height: 100, // Provide a specific height
              child: Column(
                children: <Widget>[
                  Expanded(
                    child:  ListView.builder(
                      itemCount: widget.exam.diseases.length,
                      itemBuilder: (context, index) {
                        return ListTile(
                          title: Text('${widget.exam.diseases[index]['diseaseName']}'),

                        );
                      },
                    ),
                  ),
                ],
              ),
            ),

          //   drugs administered

            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: const EdgeInsets.all(8.0),
                      color: const Color(0xff949494), // Grey colored heading section
                      width: double.infinity, // Makes the container take full width
                      child: const Text(
                        'Drugs prescribed',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),

                  ],
                ),
              ),
            ),

            SizedBox(
              height: 100, // Provide a specific height
              child: Column(
                children: <Widget>[
                  Expanded(
                    child:  ListView.builder(
                      itemCount: widget.exam.drugs.length,
                      itemBuilder: (context, index) {

                        // ///// input chip
                        return InputChip(
                          label: Text('${widget.exam.drugs[index]['drugName']}'),
                          // selected: isSelected,
                          onSelected: (bool selected) {
                            setState(() {
                              if (selected) {
                                // _selectedOptions.add(option);
                              } else {
                                // _selectedOptions.remove(option);
                              }
                            });
                          },
                          avatar: const Icon(Icons.check, size: 20.0,),
                        );
                        // ///// input chip

                      },
                    ),
                  ),

                ],
              ),
            ),
            const Padding(
              padding: EdgeInsets.all(16.0),
              child: Text(
                'Comments on administored drugs', // The heading
                style: TextStyle(
                  fontSize: 10.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),

            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Center(
                child: ConstrainedBox(
                  constraints: const BoxConstraints(
                    maxHeight: 200.0, // Maximum height
                    // Add minWidth, maxWidth, minHeight as needed
                  ),
                  child: TextField(
                    readOnly: true,
                    maxLines: null, // Allows the TextField to grow vertically
                    keyboardType: TextInputType.multiline,
                    decoration: InputDecoration(
                      hintText: widget.exam.drugsComment,
                      border: const OutlineInputBorder(),
                    ),
                  ),
                ),
              ),
            ),




          ],),
      ),
    );
  }
  void showDataPopup(BuildContext context) async {
    // var box = await Hive.openBox<Exam>('exams'); // Make sure this matches your actual box name
    // var data = box.get(widget.exam.key); // Adjust this key according to your data structure
    var objdata = {
      'examId': widget.exam.key,
      'examDate': widget.exam.createdAt.toString(),};

    String jsonData = jsonEncode(objdata);

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Data to be synced online'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Data: $jsonData'), // Customize this based on your data's structure
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Send to API'),
              onPressed: () {
                // Call your API here with the data
                // For example: sendDataToAPI(data);
                Navigator.of(context).pop(); // Close the dialog
              },
            ),
            TextButton(
              child: const Text('Close'),
              onPressed: () {
                Navigator.of(context).pop(); // Close the dialog
              },
            ),
          ],
        );
      },
    );
  }

  // Future<Unit8List> makeReport() async{
  //   final pdf = pw.Document();
  //
  //   pdf.addPage(
  //     pw.Page(
  //       build: (pw.Context context) {
  //         return pw.Center(
  //           child: pw.Text('Hello World!'),
  //         );
  //       }
  //     ),
  //   );
  //   return pdf.save();
  //   // final file = File('example.pdf');
  //   // await file.writeAsBytes(await pdf.save());
  // }
// Add your methods for handling state changes if necessary
// //////////////




// //////////////
}
