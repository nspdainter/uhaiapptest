import 'dart:core';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hive/hive.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';
// import 'package:uhaiapp/nosql_models/student.dart';
// import 'package:uhaiapp/nosql_models/astudent.dart';
import 'dart:convert';
import 'allStudents.dart';


import '../nosql_models/exam.dart';



class DiseaseSymptom {
  int id;
  String symptomName;

  DiseaseSymptom({
    required this.id,
    required this.symptomName,
  });

  factory DiseaseSymptom.fromJson(Map<String, dynamic> json) {
    return DiseaseSymptom(
      id: json['id'] is int ? json['id'] : 0,
      symptomName: json['symptomName'] is String ? json['symptomName'] : 'Unknown',
    );
  }
  // ///// added
  Map<String, dynamic> toJson() => {
    "symptomId": id,
    "symptomName": symptomName,
  };


}

class Diseases {
  int id;
  String diseaseName;

  Diseases({
     required this.id,
     required this.diseaseName,
  });

  factory Diseases.fromJson(Map<String, dynamic> json) {
    return Diseases(
      id: json['id'] is int ? json['id'] : 0,
      diseaseName: json['diseaseName'] is String ? json['diseaseName'] : 'Unknown',
    );
  }

  // ///// added
  Map<String, dynamic> toJson() => {
    "id": id,
    "diseaseName": diseaseName,
  };
}

class Prescriptions {
  int id;
  String drugName;

  Prescriptions({
    required this.id,
    required this.drugName,
  });

  factory Prescriptions.fromJson(Map<String, dynamic> json) {
    return Prescriptions(
      id: json['id'] is int ? json['id'] : 0,
      drugName: json['drugName'] is String ? json['drugName'] : 'Unknown',
    );
  }

  // ///// added
  Map<String, dynamic> toJson() => {
    "id": id,
    "drugName": drugName,
  };
}


// //////////

class ConductExam extends StatefulWidget {
  // final Astudent student;
  final Student student;

  const ConductExam({Key? key, required this.student}) : super(key: key);

  @override
  _ConductExamState createState() => _ConductExamState();
}

class _ConductExamState extends State<ConductExam> {

  final _diseasesCommentController = TextEditingController();
  final _symptomCommentsController = TextEditingController();
  final _tempController = TextEditingController();
  final _weightController = TextEditingController();
  final _heightController = TextEditingController();
  final  _spOxiController = TextEditingController();
  final _bpmOxiController = TextEditingController();
  final _glucoseController = TextEditingController();
  final _sysBPController = TextEditingController();
  final _diaBPController = TextEditingController();
  final _pulBPController = TextEditingController();
  final _drugsCommentController = TextEditingController();

  String _selectedCategoryOption = 'Minor';

  String? _selectedMalariaOption;
  String? _selectedPregnancyOption;
  String? _selectedVaginalInfectionOption;
  String? _selectedCovidOption;
  String? _selectedHivOption;
  String? _selectedTyphoidOption;


  void _setSelectedMalariaOption(String? value) {
    setState(() {
      _selectedMalariaOption = value;
    });
  }

  void _setSelectedPregnancyOption(String? value) {
    setState(() {
      _selectedPregnancyOption = value;
    });
  }

  void _setSelectedVaginalInfectionOption(String? value) {
    setState(() {
      _selectedVaginalInfectionOption = value;
    });
  }

  void _setSelectedCovidOption(String? value) {
    setState(() {
      _selectedCovidOption = value;
    });
  }

  void _setSelectedHivOption(String? value) {
    setState(() {
      _selectedHivOption = value;
    });
  }

  void _setSelectedTyphoidOption(String? value) {
    setState(() {
      _selectedTyphoidOption = value;
    });
  }

  void _setSelectedCategoryOption(String? value) {
    setState(() {
      _selectedCategoryOption = value!;
    });
  }

  // ///// hive

  // late Box studentBox;
  late Box <Exam>examBox;


  @override
  void initState() {
    super.initState();

    // studentBox = Hive.box('students');
    examBox = Hive.box('exams');

    // _selectedSymptomsId = _selectedSymptoms.map((results) => results.id).toList();
    _tempController.addListener(_updateTempPreview);
    _heightController.addListener(_updateHeightPreview);
    _weightController.addListener(_updateWeightPreview);
    _glucoseController.addListener(_updateGlucosePreview);
    _sysBPController.addListener(_updateSYSBPPreview);
    _diaBPController.addListener(_updateDIABPPreview);
    _pulBPController.addListener(_updatePULBPPreview);
    _spOxiController.addListener(_updateSpoOxiPreview);
    _bpmOxiController.addListener(_updateBPMOxiPreview);

  //   oximeter values change of state
    int? getIntValue(String str) {
      try {
        final intValue = int.parse(str);
        return intValue;
      } catch (e) {
        return null; // Return null if parsing fails
      }
    }
    List<Map<String, int?>> theOximeterValues = [ {"sp02":getIntValue(_spOxiController.text), "prbpm":getIntValue(_bpmOxiController.text)}];
    String oximeterToJson(List<Map<String, int?>> theOximeterValues) => json.encode(List<dynamic>.from(theOximeterValues.map((x) => Map.from(x).map((k, v) => MapEntry<String, dynamic>(k, v)))));
    final oximeterValues = oximeterToJson(theOximeterValues);
    _myOximeterValues = oximeterValues;
    // print(_myOximeterValues);

  //   /////blood pressure values change state

      List<Map<String, int?>> theBloodPressureValues = [ {"sys":getIntValue(_sysBPController.text), "dia":getIntValue(_diaBPController.text), "pul":getIntValue(_pulBPController.text)}];
      String bloodPressureToJson(List<Map<String, int?>> theBloodPressureValues) => json.encode(List<dynamic>.from(theBloodPressureValues.map((x) => Map.from(x).map((k, v) => MapEntry<String, dynamic>(k, v)))));
      final bloodPressureValues = bloodPressureToJson(theBloodPressureValues);
      _myBloodPressureValues = bloodPressureValues;
      // print(_myOximeterValues);






  }

  void _updateTempPreview() {
    setState(() {
    });
  }
  void _updateWeightPreview() {
    setState(() {
    });
  }
  void _updateHeightPreview() {
    setState(() {
    });
  }

  void _updateGlucosePreview() {
    setState(() {
    });
  }

  void _updateSpoOxiPreview() {
    setState(() {
    });
  }

  void _updateBPMOxiPreview() {
    setState(() {
    });
  }

  void _updateSYSBPPreview() {
    setState(() {
    });
  }

  void _updateDIABPPreview() {
    setState(() {
    });
  }

  void _updatePULBPPreview() {
    setState(() {
    });
  }


  @override
  void dispose() {

    _tempController.removeListener(_updateTempPreview);
    _heightController.removeListener(_updateTempPreview);
    _weightController.addListener(_updateWeightPreview);
    _glucoseController.removeListener(_updateTempPreview);
    _sysBPController.removeListener(_updateTempPreview);
    _diaBPController.removeListener(_updateTempPreview);
    _pulBPController.removeListener(_updateTempPreview);
    _spOxiController.removeListener(_updateTempPreview);
    _bpmOxiController.removeListener(_updateTempPreview);
   // Clean up the listener when the widget is disposed.

    _tempController.dispose();
    _heightController.dispose();
    _weightController.dispose();
    _glucoseController.dispose();
    _sysBPController.dispose();
    _diaBPController.dispose();
    _pulBPController.dispose();
    _spOxiController.dispose();
    _bpmOxiController.dispose();


    super.dispose();
  }

  int _currentStep = 0;

  final List<DiseaseSymptom> _symptoms = [
    DiseaseSymptom(id: 1, symptomName: "Fever"),
    DiseaseSymptom(id: 2, symptomName: "Cough"),
    DiseaseSymptom(id: 3, symptomName: "Fatigue"),
    DiseaseSymptom(id: 4, symptomName: "Nausea"),
    DiseaseSymptom(id: 5, symptomName: "Headache"),
    DiseaseSymptom(id: 6, symptomName: "Dizziness"),
    DiseaseSymptom(id: 7, symptomName: "Shortness of breath"),
    DiseaseSymptom(id: 8, symptomName: "Chest pain"),
    DiseaseSymptom(id: 9, symptomName: "Loss of taste or smell"),
    DiseaseSymptom(id: 10, symptomName: "Muscle or joint pain"),
    DiseaseSymptom(id: 11, symptomName: "Pale skin"),
    DiseaseSymptom(id: 12, symptomName: "Red eyes"),
    DiseaseSymptom(id: 13, symptomName: "Itching skin"),
    DiseaseSymptom(id: 14, symptomName: "Paralyzed"),
    DiseaseSymptom(id: 15, symptomName: "Stomachache"),
    DiseaseSymptom(id: 16, symptomName: "No appetite"),
    DiseaseSymptom(id: 17, symptomName: "Loss of hair"),
    DiseaseSymptom(id: 18, symptomName: "Blurred vision"),
    // Add your symptoms here...
  ];

  final List<Diseases> _diseases = [
    Diseases(id: 1, diseaseName: "Malaria"),
    Diseases(id: 2, diseaseName: "Typhoid"),
    Diseases(id: 3, diseaseName: "Sexually Transmitted Infection"),
    Diseases(id: 4, diseaseName: "Urinary Tract Infection"),
    Diseases(id: 5, diseaseName: "Common Cold"),
    Diseases(id: 6, diseaseName: "Pneumonia"),
    Diseases(id: 7, diseaseName: "Asthma"),
    Diseases(id: 8, diseaseName: "Diabetes"),
    Diseases(id: 9, diseaseName: "Hypertension"),
    Diseases(id: 10, diseaseName: "Migraine"),
    Diseases(id: 11, diseaseName: "Measles"),
    Diseases(id: 12, diseaseName: "Fever"),
    Diseases(id: 13, diseaseName: "Coronary Artery Disease"),
    Diseases(id: 14, diseaseName: "Gastroenteritis"),
    Diseases(id: 15, diseaseName: "Depression"),


    // Add your symptoms here...
  ];

  final List<Prescriptions> _drugs = [
    Prescriptions(id: 1, drugName: "Paracetamol"),
    Prescriptions(id: 2, drugName: "Amoxicillin"),
    Prescriptions(id: 3, drugName: "Vitamin C"),
    Prescriptions(id: 4, drugName: "Cough Syrup"),
    Prescriptions(id: 5, drugName: "Magnesium"),
    Prescriptions(id: 6, drugName: "Cetirizine"),
    Prescriptions(id: 7, drugName: "Metformin"),
    Prescriptions(id: 8, drugName: "Pantoprazole"),
    Prescriptions(id: 9, drugName: "Pravastatin"),
    Prescriptions(id: 10, drugName: "Sertraline"),
    Prescriptions(id: 11, drugName: "Simvastatin"),
    Prescriptions(id: 12, drugName: "Gabapentin"),
    Prescriptions(id: 13, drugName: "Losartan"),
    Prescriptions(id: 14, drugName: "Omeprazole"),
    Prescriptions(id: 15, drugName: "Albuterol"),
    Prescriptions(id: 16, drugName: "Metoprolol"),
    Prescriptions(id: 17, drugName: "Amlodipine"),
    Prescriptions(id: 18, drugName: "Mabendazo"),
    Prescriptions(id: 19, drugName: "Levothyroxine"),
    Prescriptions(id: 20, drugName: "Atorvastatin"),

  ];

  String? _myOximeterValues;
  String? _myBloodPressureValues;
  String? _myRapidTests;

  List<DiseaseSymptom> _selectedSymptoms = [];
  String? _mySelectedSymptoms;
  // late List<DiseaseSymptom> _mySelectedSymptoms;

  List<Diseases> _selectedDiseases = [];
  String? _mySelectedDiseases;

  List<Prescriptions> _selectedDrugs = [];
  String? _mySelectedDrugs;
  // send data to db start

  final storeExamUrl = "https://copint.org/device_test/postExam.php";
  // ///// post method starts here

  // Future<void> saveExamLocally(BuildContext context) async {
  //
  //   showDialog(
  //     context: context,
  //     barrierDismissible: false, // Prevent dialog from closing on tap
  //     builder: (BuildContext context) {
  //       return Dialog(
  //         child: Padding(
  //           padding: const EdgeInsets.all(20),
  //           child: Row(
  //             mainAxisSize: MainAxisSize.min,
  //             children: [
  //               CircularProgressIndicator(),
  //               SizedBox(width: 20),
  //               Text("Saving data..."),
  //             ],
  //           ),
  //         ),
  //       );
  //     },
  //   );
  //
  //   // Prepare the data for saving
  //   Map<String, dynamic> examData = {
  //     'nurseId': '1',
  //     'studentId': widget.student.id,
  //     'symptoms' : jsonDecode(_mySelectedSymptoms ?? ''),
  //     'symptomsComment': _symptomCommentsController.text,
  //     'temperature': _tempController.text,
  //     'weight': _weightController.text,
  //     'glucose': _glucoseController.text,
  //     'height': _heightController.text,
  //     'oximeter': _myOximeterValues,
  //     'bloodPressure': _myBloodPressureValues,
  //     'rapidTests': _myRapidTests,
  //     'diseases' : jsonDecode(_mySelectedDiseases ?? ''),
  //     'drugs' : jsonDecode(_mySelectedDrugs ?? ''),
  //     'examCategoryId': '2',// Your data here
  //     'category': _selectedCategoryOption, // Your data here
  //     'comments': _symptomCommentsController.text, //
  //   };
  //
  //   String encodedData = jsonEncode(examData);
  //
  //   // Step 1: Show the dialog with the JSON data
  //   bool proceedWithPosting = await showDialog(
  //     context: context,
  //     builder: (BuildContext context) {
  //       return AlertDialog(
  //         title: Text('Confirm Data'),
  //         content: SingleChildScrollView(
  //           child: Text(encodedData),
  //         ),
  //         actions: <Widget>[
  //           TextButton(
  //             child: Text('Cancel'),
  //             onPressed: () => Navigator.of(context).pop(false), // User cancels, do not proceed
  //           ),
  //           TextButton(
  //             child: Text('Proceed'),
  //             onPressed: () => Navigator.of(context).pop(true), // User confirms, proceed with posting
  //           ),
  //         ],
  //       );
  //     },
  //   ) ?? false; // In case the dialog is dismissed (e.g., back button), treat it as "do not proceed"
  //
  //   // Step 2: Check the user's decision and proceed accordingly
  //   if (!proceedWithPosting) {
  //     return; // Early return if the user decides not to proceed
  //   }
  //
  //   // Continue with showing the loading dialog and posting the data as previously described
  //   // showDialog(
  //   //   context: context,
  //   //   barrierDismissible: false, // Prevent dialog from closing on tap
  //   //   builder: (BuildContext context) {
  //   //     return Dialog(
  //   //       child: Padding(
  //   //         padding: const EdgeInsets.all(20),
  //   //         child: Row(
  //   //           mainAxisSize: MainAxisSize.min,
  //   //           children: [
  //   //             CircularProgressIndicator(),
  //   //             SizedBox(width: 20),
  //   //             Text("Posting data..."),
  //   //           ],
  //   //         ),
  //   //       ),
  //   //     );
  //   //   },
  //   // );
  //   // //////////////
  //
  //   // var url = Uri.parse('https://copint.org/device_test/postExam.php');
  //   //
  //   // try {
  //   //   var response = await http.post(
  //   //     url,
  //   //     headers: {"Content-Type": "application/json"},
  //   //     body: encodedData,
  //   //   );
  //   //
  //   //   Navigator.of(context).pop(); // Dismiss the loading dialog
  //   //
  //   //   final responseJson = json.decode(response.body);
  //   //
  //   //   if (response.statusCode == 200 && responseJson["success"]) {
  //   //     // Success
  //   //     showDialog(
  //   //       context: context,
  //   //       builder: (BuildContext context) {
  //   //         return AlertDialog(
  //   //           title: Text('Success'),
  //   //           content: Text(responseJson["message"]),
  //   //           actions: <Widget>[
  //   //             TextButton(
  //   //               child: Text('OK'),
  //   //               onPressed: () {
  //   //                 Navigator.of(context).pop();
  //   //               },
  //   //             ),
  //   //           ],
  //   //         );
  //   //       },
  //   //     );
  //   //   } else {
  //   //     // Error scenario, including SQL errors from the server
  //   //     String errorMessage = "Failed to post symptoms.";
  //   //     if (responseJson.containsKey("message")) {
  //   //       errorMessage = responseJson["message"];
  //   //     }
  //   //     if (responseJson.containsKey("error")) {
  //   //       errorMessage += "\nSQL Error: ${responseJson["error"]}";
  //   //     }
  //   //     showDialog(
  //   //       context: context,
  //   //       builder: (BuildContext context) {
  //   //         return AlertDialog(
  //   //           title: Text('Error'),
  //   //           content: Text(errorMessage),
  //   //           actions: <Widget>[
  //   //             TextButton(
  //   //               child: Text('OK'),
  //   //               onPressed: () {
  //   //                 Navigator.of(context).pop();
  //   //               },
  //   //             ),
  //   //           ],
  //   //         );
  //   //       },
  //   //     );
  //   //   }
  //   // } catch (e) {
  //   //   Navigator.of(context).pop(); // Ensure loading dialog is popped in case of error
  //   //   // Handle error
  //   // }
  // }

  // ///// post method starts here

  // void saveExamLocally()  {
  //
  //   double? tryParseDouble(String? text) {
  //     if (text == null || text.isEmpty) {
  //       return null;  // Return null if text is null or empty
  //     }
  //     try {
  //       return double.parse(text);
  //     } catch (e) {
  //       return null;  // Return null if parsing fails
  //     }
  //   }
  //
  //
  //   examBox.add(
  //       Exam(
  //         examId: 18,
  //         createdAt: DateTime.now(),
  //         studentId: widget.student.id,
  //         nurseId: '1',
  //         symptoms: jsonDecode(_mySelectedSymptoms ?? ''),
  //         symptomsComment: _symptomCommentsController.text,
  //         temperature: tryParseDouble(_tempController.text),
  //         weight: tryParseDouble(_weightController.text),
  //         oximeter: jsonDecode(_myOximeterValues ?? " "),
  //         bloodPressure: jsonDecode(_myBloodPressureValues ?? " "),
  //         glucose: tryParseDouble(_glucoseController.text),
  //         height: tryParseDouble(_heightController.text),
  //         rapidTests: jsonDecode(_myRapidTests ?? " "),
  //         examCategory: _selectedCategoryOption,
  //         diseases: jsonDecode(_mySelectedDiseases ?? ''),
  //         diseasesComment: _diseasesCommentController.text,
  //         drugs: jsonDecode(_mySelectedDrugs ?? ''),
  //         drugsComment: _drugsCommentController.text,
  //       )
  //
  //   );
  //
  //
  //
  //
  //
  // }
// Ensure this is part of your stateful widget class or has access to a BuildContext
//   void saveExamLocally(BuildContext context) {
//     double? tryParseDouble(String? text) {
//       if (text == null || text.isEmpty) {
//         return null;  // Return null if text is null or empty
//       }
//       try {
//         return double.parse(text);
//       } catch (e) {
//         return null;  // Return null if parsing fails
//       }
//     }
//
//     try {
//       // if(_mySelectedSymptoms == [){}
//       examBox.add(
//           Exam(
//             examId: 18,
//             createdAt: DateTime.now(),
//             studentId: widget.student.id,
//             nurseId: '1',
//             symptoms: jsonDecode(_mySelectedSymptoms ?? ''),
//             symptomsComment: _symptomCommentsController.text,
//             temperature: tryParseDouble(_tempController.text),
//             weight: tryParseDouble(_weightController.text),
//             oximeter: jsonDecode(_myOximeterValues ?? " "),
//             bloodPressure: jsonDecode(_myBloodPressureValues ?? " "),
//             glucose: tryParseDouble(_glucoseController.text),
//             height: tryParseDouble(_heightController.text),
//             rapidTests: jsonDecode(_myRapidTests ?? " "),
//             examCategory: _selectedCategoryOption,
//             diseases: jsonDecode(_mySelectedDiseases ?? ''),
//             diseasesComment: _diseasesCommentController.text,
//             drugs: jsonDecode(_mySelectedDrugs ?? ''),
//             drugsComment: _drugsCommentController.text,
//           )
//       );
//
//       // Show success dialog
//       showDialog(
//         context: context,
//         builder: (BuildContext context) {
//           return AlertDialog(
//             title: const Text('Success'),
//             content: const Text('Medical exam saved.'),
//             actions: <Widget>[
//               TextButton(
//                 child: const Text('OK'),
//                 onPressed: () {
//                   Navigator.of(context).pop(); // Close the dialog
//                 },
//               ),
//             ],
//           );
//         },
//       );
//     } catch (e) {
//       // Show error dialog if saving fails
//       showDialog(
//         context: context,
//         builder: (BuildContext context) {
//           return AlertDialog(
//             title: const Text('Error'),
//             content: Text('Failed to save medical exam. Error: $e'),
//             actions: <Widget>[
//               TextButton(
//                 child: const Text('OK'),
//                 onPressed: () {
//                   Navigator.of(context).pop(); // Close the dialog
//                 },
//               ),
//             ],
//           );
//         },
//       );
//     }
//   }

  void saveExamLocally(BuildContext context) {
    double? tryParseDouble(String? text) {
      if (text == null || text.isEmpty) {
        return null;  // Return null if text is null or empty
      }
      try {
        return double.parse(text);
      } catch (e) {
        return null;  // Return null if parsing fails
      }
    }

    // //////////////////////////////////
    if (
    _mySelectedSymptoms == null || _mySelectedSymptoms!.isEmpty

    ) {
      // Show error dialog if symptoms are not selected
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Error'),
            content: const Text('Please select symptoms.'),
            actions: <Widget>[
              TextButton(
                child: const Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop(); // Close the dialog
                },
              ),
            ],
          );
        },
      );
      return; // Exit function
    }

    if (_symptomCommentsController.text.isEmpty) {
      // Show error dialog if symptom comment is empty
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Error'),
            content: const Text('Please add comment for the selected symptoms.'),
            actions: <Widget>[
              TextButton(
                child: const Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop(); // Close the dialog
                },
              ),
            ],
          );
        },
      );
      return; // Exit function
    }
    // ////////////////////////////////////

    // //////////////////////////////////
    if (
    _mySelectedDiseases == null || _mySelectedDiseases!.isEmpty

    ) {
      // Show error dialog if symptoms are not selected
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Error'),
            content: const Text('Please select suspected conditions.'),
            actions: <Widget>[
              TextButton(
                child: const Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop(); // Close the dialog
                },
              ),
            ],
          );
        },
      );
      return; // Exit function
    }

    if (_diseasesCommentController.text.isEmpty) {
      // Show error dialog if symptom comment is empty
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Error'),
            content: const Text('Please add comment for the selected suspected conditions.'),
            actions: <Widget>[
              TextButton(
                child: const Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop(); // Close the dialog
                },
              ),
            ],
          );
        },
      );
      return; // Exit function
    }
    // ////////////////////////////////////

    // //////////////////////////////////
    if (
    _mySelectedDrugs == null || _mySelectedDrugs!.isEmpty

    ) {
      // Show error dialog if symptoms are not selected
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Error'),
            content: const Text('Please select prescription.'),
            actions: <Widget>[
              TextButton(
                child: const Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop(); // Close the dialog
                },
              ),
            ],
          );
        },
      );
      return; // Exit function
    }

    if (_drugsCommentController.text.isEmpty) {
      // Show error dialog if symptom comment is empty
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Error'),
            content: const Text('Please add comment for the selected prescription.'),
            actions: <Widget>[
              TextButton(
                child: const Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop(); // Close the dialog
                },
              ),
            ],
          );
        },
      );
      return; // Exit function
    }
    // ////////////////////////////////////

    try {
      examBox.add(
          Exam(
            examId: 18,
            createdAt: DateTime.now(),
            studentId: widget.student.id.toString(),
            nurseId: '1',
            symptoms: jsonDecode(_mySelectedSymptoms ?? ''),
            symptomsComment: _symptomCommentsController.text,
            temperature: tryParseDouble(_tempController.text),
            weight: tryParseDouble(_weightController.text),
            oximeter: jsonDecode(_myOximeterValues ?? " "),
            bloodPressure: jsonDecode(_myBloodPressureValues ?? " "),
            glucose: tryParseDouble(_glucoseController.text),
            height: tryParseDouble(_heightController.text),
            rapidTests: jsonDecode(_myRapidTests ?? " "),
            examCategory: _selectedCategoryOption,
            diseases: jsonDecode(_mySelectedDiseases ?? ''),
            diseasesComment: _diseasesCommentController.text,
            drugs: jsonDecode(_mySelectedDrugs ?? ''),
            drugsComment: _drugsCommentController.text,
          )
      );

      // Show success dialog
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Success'),
            content: const Text('Medical exam saved.'),
            actions: <Widget>[
              TextButton(
                child: const Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop(); // Close the dialog
                },
              ),
            ],
          );
        },
      );
    } catch (e) {
      // Show error dialog if saving fails
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Error'),
            content: Text('Failed to save medical exam. Error: $e'),
            actions: <Widget>[
              TextButton(
                child: const Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop(); // Close the dialog
                },
              ),
            ],
          );
        },
      );
    }
  }



  // //// post method ends here

  // //// post method ends here

  // send data to bd end


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xff18588d),
        title: Text("Conduct Medical Exam for ${widget.student.first_name}", style: const TextStyle(color: Color(0xffffffff))),
          iconTheme: const IconThemeData(
          color: Color(0xffffffff),
    ),
      ),
      body: SingleChildScrollView(
        // scrollDirection: Axis.vertical,
        child:
        Column(
            children: [
              Wrap(
                alignment: WrapAlignment.center,
                spacing: 8.0,
                runSpacing: 4.0,
                  children: [
                    Stepper(
                    steps: getSteps(),
                    onStepCancel: () {
                      if(_currentStep > 0){
                        setState(() {
                          _currentStep--;
                        });
                      }

                    },
                    currentStep: _currentStep,
                    onStepContinue: () {
                      final isLastStep = _currentStep == getSteps().length -1;

                      if(isLastStep){

                      }
                      else {
                        setState(() {
                          _currentStep++;
                        });
                      }
                    },
                      controlsBuilder: (BuildContext context, ControlsDetails controlsDetails) {
                        return _buildStepperControls(controlsDetails, _currentStep == getSteps().length - 1);
                      },

                  ),
                    ElevatedButton(
                      onPressed: () {
                        saveExamLocally(context);
                      },
                      style: ElevatedButton.styleFrom(
                        foregroundColor: const Color(0xffffffff),
                        backgroundColor: const Color(0xff18588d),
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.zero, // No rounded corners
                        ),
                      ),
                      child: const Text('Save Medical Exam'),
                    ),

                  ]
              ),
            ],
        ),



      ),
    );
  }

  List<Step> getSteps(){
    return <Step> [
      Step(
        title: const Text('Symptoms & General comments'),
        content: Column(
          children: [
            MultiSelectDialogField(
              searchable: true,
              items: _symptoms.map((symptom) => MultiSelectItem<DiseaseSymptom>(symptom, symptom.symptomName)).toList(),
              title: const Text("Symptoms"),
              selectedColor: Theme.of(context).primaryColor,
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColor.withOpacity(0.1),
                // borderRadius: BorderRadius.all(Radius.circular(0)),
                border: Border.all(
                  color: Theme.of(context).primaryColor,
                  width: 2,
                ),
              ),
              buttonIcon: Icon(
                Icons.add,
                color: Theme.of(context).primaryColor,
              ),
              buttonText: const Text(
                "Select Symptoms",
                style: TextStyle(
                  color: Color(0xff000000),
                  fontSize: 16,
                ),
              ),
              onConfirm: (results) {
                setState(() {
                  // List<Symptoms> symptomsFromJson(String str) => List<Symptoms>.from(json.decode(str).map((x) => Symptoms.fromJson(x)));
                  _selectedSymptoms = List<DiseaseSymptom>.from(results);
                  String symptomsToJson(List<DiseaseSymptom> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));
                  final mySymptoms = symptomsToJson(_selectedSymptoms);
                  _mySelectedSymptoms = mySymptoms as String?;
                  print(_mySelectedSymptoms);
                });

              },
              chipDisplay: MultiSelectChipDisplay(
                chipColor: const Color(0xffc5c5c5),
                textStyle: const TextStyle(
                  color: Color(0xff000000),
                ),
                onTap: (value) {
                  setState(() {
                    _selectedSymptoms.remove(value);
                  });
                },
              ),
            ),
            const Padding(padding: EdgeInsets.all(10)),
            TextField(
              maxLines: 3,
              controller: _symptomCommentsController,
              decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Add briefs about symptoms'
              ),
            ),
            const SizedBox(height: 50,),


          ],
        ),),

      Step(
          title: const Text('Take Vitals'),
          content: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Wrap(
              spacing: 20.0, // space between cards horizontally
              runSpacing: 20.0, // space between cards vertically
              children: <Widget>[
              SizedBox(
                        width: 180,
                        height: 40,
                        child:ElevatedButton.icon(
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(Color(0xff18588d)), // Set button background color
                            foregroundColor: MaterialStateProperty.all<Color>(Color(0xffffffff)),
                            shape: MaterialStateProperty.all(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                            ),
                          ),
                        onPressed: () => _showTemperatureForm(context),
                        icon: const Icon(Icons.thermostat_sharp),
                        label: const Text('Temperature'),
                      )),
              SizedBox(
                        width: 180,
                        height: 40,
                        child:ElevatedButton.icon(
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(Color(0xff18588d)), // Set button background color
                            foregroundColor: MaterialStateProperty.all<Color>(Color(0xffffffff)),
                            shape: MaterialStateProperty.all(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                            ),
                          ),
                        onPressed: () => _showWeightForm(context),
                        icon: const Icon(Icons.monitor_weight_outlined),
                        label: const Text('Weight'),
                      )),
              SizedBox(
                        width: 180,
                        height: 40,
                        child:ElevatedButton.icon(
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(Color(0xff18588d)), // Set button background color
                            foregroundColor: MaterialStateProperty.all<Color>(Color(0xffffffff)),
                            shape: MaterialStateProperty.all(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                            ),
                          ),
                        onPressed: () => _showOximeterForm(context),
                        icon: const Icon(Icons.all_inclusive_rounded),
                        label: const Text('Oximeter'),
                      )),
              SizedBox(
                        width: 180,
                        height: 40,
                        child:ElevatedButton.icon(
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(Color(0xff18588d)), // Set button background color
                            foregroundColor: MaterialStateProperty.all<Color>(Color(0xffffffff)),
                            shape: MaterialStateProperty.all(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                            ),
                          ),
                        onPressed: () => _showBloodPressureForm(context),
                        icon: const Icon(Icons.bloodtype_outlined),
                        label: const Text('Blood pressure'),
                      )),
              SizedBox(
                        width: 180,
                        height: 40,
                        child:ElevatedButton.icon(
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(Color(0xff18588d)), // Set button background color
                            foregroundColor: MaterialStateProperty.all<Color>(Color(0xffffffff)),
                            shape: MaterialStateProperty.all(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                            ),
                          ),
                        onPressed: () => _showGlucoseForm(context),
                        icon: const Icon(Icons.electric_meter_outlined),
                        label: const Text('Glucose'),
                      ),
                      ),
              SizedBox(
                        width: 180,
                        height: 40,
                        child: ElevatedButton.icon(
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(Color(0xff18588d)), // Set button background color
                            foregroundColor: MaterialStateProperty.all<Color>(Color(0xffffffff)),
                            shape: MaterialStateProperty.all(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                            ),
                          ),
                          onPressed: () => _showHeightForm(context),
                          icon: const Icon(Icons.height),
                          label: const Text('Height'),
                        ),
                      ),
              const SizedBox(height: 50),
                //     // preview area starts here
                    Column(
                      key: const ValueKey('vitalsPreview'),
                      children: [
                        const Text('Vitals Preview Area.'),
                        const SizedBox(height: 10,),
                        if (_tempController.text.isNotEmpty) Text('Temperature: ${_tempController.text} °C'),

                        const SizedBox(height: 10,),
                        if (_weightController.text.isNotEmpty) Text('Weight: ${_weightController.text} Kg'),

                        const SizedBox(height: 10,),
                        if (_heightController.text.isNotEmpty) Text('Height: ${_heightController.text} cm'),

                        const SizedBox(height: 10,),
                        if (_glucoseController.text.isNotEmpty) Text('Glucose Level: ${_glucoseController.text} cm'),

                        const SizedBox(height: 10,),
                        if (_spOxiController.text.isNotEmpty) Text('Sp02: ${_spOxiController.text} and PRbpm: ${_bpmOxiController.text}'),

                        const SizedBox(height: 10,),
                        if (_sysBPController.text.isNotEmpty && _diaBPController.text.isNotEmpty && _pulBPController.text.isNotEmpty)
                          Text('SYS: ${_sysBPController.text} (mmHg) , DIA: ${_diaBPController.text} (mmHg)  and PUL : ${_pulBPController.text}(/min)'),


                        // Text('Temperature: ${_tempController.text} °C'),
                      ],
                    )
                //
                //     // preview area ends here here


              ],
            ),
          ),


          //     // preview area starts here
          //     Column(
          //       key: const ValueKey('vitalsPreview'),
          //       children: [
          //         const Text('Vitals Preview Area.'),
          //         const SizedBox(height: 10,),
          //         if (_tempController.text.isNotEmpty) Text('Temperature: ${_tempController.text} °C'),
          //
          //         const SizedBox(height: 10,),
          //         if (_weightController.text.isNotEmpty) Text('Weight: ${_weightController.text} Kg'),
          //
          //         const SizedBox(height: 10,),
          //         if (_heightController.text.isNotEmpty) Text('Height: ${_heightController.text} cm'),
          //
          //         const SizedBox(height: 10,),
          //         if (_glucoseController.text.isNotEmpty) Text('Glucose Level: ${_glucoseController.text} cm'),
          //
          //         const SizedBox(height: 10,),
          //         if (_spOxiController.text.isNotEmpty) Text('Sp02: ${_spOxiController.text} and PRbpm: ${_bpmOxiController.text}'),
          //
          //         const SizedBox(height: 10,),
          //         if (_sysBPController.text.isNotEmpty && _diaBPController.text.isNotEmpty && _pulBPController.text.isNotEmpty)
          //           Text('SYS: ${_sysBPController.text} (mmHg) , DIA: ${_diaBPController.text} (mmHg)  and PUL : ${_pulBPController.text}(/min)'),
          //
          //
          //         // Text('Temperature: ${_tempController.text} °C'),
          //       ],
          //     )
          //
          //     // preview area ends here here
          //
          //   ],
          // )
      ),
      Step(
        title: const Text('Rapid Tests'),
        content: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: [
              const ListTile(
                title: Text('Malaria', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
              ),
              Wrap(
                // mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    width: 200,
                    child: RadioListTile<String>(
                      title: const Text('Negative'),
                      value: 'Negative',
                      groupValue: _selectedMalariaOption,
                      onChanged: _setSelectedMalariaOption,
                    ),
                  ),
                  SizedBox(
                    width: 200,
                    child: RadioListTile<String>(
                      title: const Text('Positive'),
                      value: 'Positive',
                      groupValue: _selectedMalariaOption,
                      onChanged: _setSelectedMalariaOption,
                    ),
                  ),
                  SizedBox(
                    width: 200,
                    child: RadioListTile<String>(
                      title: const Text('Not tested'),
                      value: 'Not tested',
                      groupValue: _selectedMalariaOption,
                      onChanged: _setSelectedMalariaOption,
                    ),
                  ),
                ],
              ),

              const ListTile(
                title: Text('Pregnancy', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold) ),
              ),
              Wrap(
                // mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 200,
                    child:  RadioListTile<String>(
                      title: const Text('Negative'),
                      value: 'Negative',
                      groupValue: _selectedPregnancyOption,
                      onChanged: _setSelectedPregnancyOption,
                    ),
                  ),
                  SizedBox(
                    width: 200,
                    child: RadioListTile<String>(
                      title: const Text('Positive'),
                      value: 'Positive',
                      groupValue: _selectedPregnancyOption,
                      onChanged: _setSelectedPregnancyOption,
                    ),
                  ),
                  SizedBox(
                    width: 200,
                    child: RadioListTile<String>(
                      title: const Text('Not tested'),
                      value: 'Not tested',
                      groupValue: _selectedPregnancyOption,
                      onChanged: _setSelectedPregnancyOption,
                    ),
                  ),
                ],
              ),


              const ListTile(
                title: Text('Urinary Tract Infections', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold) ),
              ),

              Wrap(
                // mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 200,
                    child:  RadioListTile<String>(
                      title: const Text('Negative'),
                      value: 'Negative',
                      groupValue: _selectedVaginalInfectionOption,
                      onChanged: _setSelectedVaginalInfectionOption,
                    ),
                  ),
                  SizedBox(
                    width: 200,
                    child: RadioListTile<String>(
                      title: const Text('Positive'),
                      value: 'Positive',
                      groupValue: _selectedVaginalInfectionOption,
                      onChanged: _setSelectedVaginalInfectionOption,
                    ),
                  ),
                  SizedBox(
                    width: 200,
                    child: RadioListTile<String>(
                      title: const Text('Not tested'),
                      value: 'Not tested',
                      groupValue: _selectedVaginalInfectionOption,
                      onChanged: _setSelectedVaginalInfectionOption,
                    ),
                  ),
                ],
              ),


              const ListTile(
                title: Text('COVID 19', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
              ),

              Wrap(
                // mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 200,
                    child:  RadioListTile<String>(
                      title: const Text('Negative'),
                      value: 'Negative',
                      groupValue: _selectedCovidOption,
                      onChanged: _setSelectedCovidOption,
                    ),
                  ),
                  SizedBox(
                    width: 200,
                    child: RadioListTile<String>(
                      title: const Text('Positive'),
                      value: 'Positive',
                      groupValue: _selectedCovidOption,
                      onChanged: _setSelectedCovidOption,
                    ),
                  ),
                  SizedBox(
                    width: 200,
                    child: RadioListTile<String>(
                      title: const Text('Not tested'),
                      value: 'Not tested',
                      groupValue: _selectedCovidOption,
                      onChanged: _setSelectedCovidOption,
                    ),
                  ),
                ],
              ),
              ////////////////////
              const ListTile(
                title: Text('HIV', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
              ),

              Wrap(
                // mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 200,
                    child:  RadioListTile<String>(
                      title: const Text('Negative'),
                      value: 'Negative',
                      groupValue: _selectedHivOption,
                      onChanged: _setSelectedHivOption,
                    ),
                  ),
                  SizedBox(
                    width: 200,
                    child: RadioListTile<String>(
                      title: const Text('Positive'),
                      value: 'Positive',
                      groupValue: _selectedHivOption,
                      onChanged: _setSelectedHivOption,
                    ),
                  ),
                  SizedBox(
                    width: 200,
                    child: RadioListTile<String>(
                      title: const Text('Not tested'),
                      value: 'Not tested',
                      groupValue: _selectedHivOption,
                      onChanged: _setSelectedHivOption,
                    ),
                  ),
                ],
              ),

              const ListTile(
                title: Text('Typhoid Widal Test', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
              ),

              Wrap(
                // mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 200,
                    child:  RadioListTile<String>(
                      title: const Text('Negative'),
                      value: 'Negative',
                      groupValue: _selectedTyphoidOption,
                      onChanged: _setSelectedTyphoidOption,
                    ),
                  ),
                  SizedBox(
                    width: 200,
                    child: RadioListTile<String>(
                      title: const Text('Positive'),
                      value: 'Positive',
                      groupValue: _selectedTyphoidOption,
                      onChanged: _setSelectedTyphoidOption,
                    ),
                  ),
                  SizedBox(
                    width: 200,
                    child: RadioListTile<String>(
                      title: const Text('Not tested'),
                      value: 'Not tested',
                      groupValue: _selectedTyphoidOption,
                      onChanged: _setSelectedTyphoidOption,
                    ),
                  ),
                ],
              ),

            ],
          ),
        ),

      ),
      Step(
          title: const Text('Categorise the examination'),
          content: SingleChildScrollView(
            child: Column(
              children: <Widget>[

                ListTile(
                  title: const Text('Minor'),
                  leading: Radio<String>(
                    value: 'Minor',
                    groupValue: _selectedCategoryOption,
                    onChanged: _setSelectedCategoryOption,
                  ),
                ),
                ListTile(
                  title: const Text('Mild'),
                  leading: Radio<String>(
                    value: 'Mild',
                    groupValue: _selectedCategoryOption,
                    onChanged: _setSelectedCategoryOption,
                  ),
                ),
                ListTile(
                  title: const Text('Severe'),
                  leading: Radio<String>(
                    value: 'Severe',
                    groupValue: _selectedCategoryOption,
                    onChanged: _setSelectedCategoryOption,
                  ),
                ),
                const SizedBox(height: 50,),

              ],
            ),
          ),

      ),
      Step(
          title: const Text('Diseases'),
          content: Column(
            children: [
              MultiSelectDialogField(
                searchable: true,
                items: _diseases.map((diseases) => MultiSelectItem<Diseases>(diseases, diseases.diseaseName)).toList(),
                title: const Text("Diseases"),
                selectedColor: Theme.of(context).primaryColor,
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor.withOpacity(0.1),
                  // borderRadius: BorderRadius.all(Radius.circular(0)),
                  border: Border.all(
                    color: Theme.of(context).primaryColor,
                    width: 2,
                  ),
                ),
                buttonIcon: Icon(
                  Icons.add,
                  color: Theme.of(context).primaryColor,
                ),
                buttonText: const Text(
                  "Suspected condition",
                  style: TextStyle(
                    color: Color(0xff000000),
                    fontSize: 16,
                  ),
                ),
                onConfirm: (results) {
                  _selectedDiseases = List<Diseases>.from(results);
                  String diseasesToJson(List<Diseases> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));
                  final myDiseases = diseasesToJson(_selectedDiseases);
                  _mySelectedDiseases = myDiseases;

                //   ////// populate rapid tests start

                  List<Map<String, String>> theRapidTestValues = [ {
                    "malaria":_selectedMalariaOption.toString(),
                    "pregnancy":_selectedPregnancyOption.toString(),
                    "vaginalInfection":_selectedVaginalInfectionOption.toString(),
                    "covid19":_selectedCovidOption.toString(),
                    "hiv":_selectedHivOption.toString(),
                    "typhoid":_selectedTyphoidOption.toString()
                  }];
                  String rapidTestsToJson(List<Map<String, String>> theRapidTestValues) => json.encode(List<dynamic>.from(theRapidTestValues.map((x) => Map.from(x).map((k, v) => MapEntry<String, dynamic>(k, v)))));
                  final rapidTestsValues = rapidTestsToJson(theRapidTestValues);
                  _myRapidTests = rapidTestsValues;
                  print(rapidTestsValues);

                  //   ////// populate rapid tests end

                },
                chipDisplay: MultiSelectChipDisplay(
                  chipColor: const Color(0xffc5c5c5),
                  textStyle: const TextStyle(
                    color: Color(0xff000000),
                  ),
                  onTap: (value) {
                    setState(() {
                      _selectedDiseases.remove(value);
                    });
                  },
                ),
              ),
              const Padding(padding: EdgeInsets.all(10)),
              TextField(
                controller: _diseasesCommentController,
                // maxLines: 5,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Add briefs about suspected condition'
                ),
              ),
              const SizedBox(height: 50,),
            ],
          )
      ),

      Step(
          title: const Text('Prescriptions'),
          content: Column(
            children: [
              MultiSelectDialogField(
                searchable: true,
                items: _drugs.map((drugs) => MultiSelectItem<Prescriptions>(drugs, drugs.drugName)).toList(),
                title: const Text("Prescriptions"),
                selectedColor: Theme.of(context).primaryColor,
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor.withOpacity(0.1),
                  // borderRadius: BorderRadius.all(Radius.circular(0)),
                  border: Border.all(
                    color: Theme.of(context).primaryColor,
                    width: 2,
                  ),
                ),
                buttonIcon: Icon(
                  Icons.add,
                  color: Theme.of(context).primaryColor,
                ),
                buttonText: const Text(
                  "Select Prescriptions",
                  style: TextStyle(
                    color: Color(0xff000000),
                    fontSize: 16,
                  ),
                ),
                onConfirm: (results) {

                  _selectedDrugs = List<Prescriptions>.from(results);
                  String drugsToJson(List<Prescriptions> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));
                  final myDrugs = drugsToJson(_selectedDrugs);
                  _mySelectedDrugs = myDrugs;

                },
                chipDisplay: MultiSelectChipDisplay(
                  chipColor: const Color(0xffc5c5c5),
                  textStyle: const TextStyle(
                    color: Color(0xff000000),
                  ),
                  onTap: (value) {
                    setState(() {
                      _selectedDrugs.remove(value);
                    });
                  },
                ),
              ),
              const Padding(padding: EdgeInsets.all(10)),
              TextField(
                controller: _drugsCommentController,
                // maxLines: 5,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Add briefs about prescriptiion'
                ),
              ),
              const SizedBox(height: 50,),
            ],
          )
      ),
    ];
  }

//   ////////////////// vitals methods

  void _showTemperatureForm(BuildContext context) {
    // Now show the dialog.
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder( // Use StatefulBuilder to update the dialog's content
          builder: (context, setState) {
            return AlertDialog(
              title: const Row(
                mainAxisSize: MainAxisSize.min,
                children: [

                  Expanded(child: Text('Temperature')),
                  // CircularProgressIndicator(), // Reflect current state
                ],
              ),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[

                    const Text('Enter the temperature: (°C)'),
                    const SizedBox(height: 20,),
                    TextField(
                      keyboardType: TextInputType.numberWithOptions(decimal: true),
                      inputFormatters: [
                        FilteringTextInputFormatter.allow(RegExp(r'^\d*\.?\d{0,1}')),
                      ],
                      autofocus: true,
                      controller: _tempController, // Assuming this gets updated when scan finishes
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        // hintText: 'Temperature in °C',
                      ),
                    ),
                  ],
                ),
              ),
              actions: <Widget>[
                TextButton(
                  child: const Text('Cancel'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                TextButton(
                  child: const Text('Save'),
                  onPressed: () {
                    setState(() {
                      // This assumes _tempController.text contains the new temperature value.
                      // You might need to convert or validate the value as necessary.
                    });
                    // Logic to save the temperature here
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      },
    );
  }

  void _showOximeterForm(BuildContext context) {
    // Now show the dialog.
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder( // Use StatefulBuilder to update the dialog's content
          builder: (context, setState) {
            return AlertDialog(
              title: const Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  // /////////////////

                  // ////////////////
                  Expanded(child: Text('Oximetry')),
                  
                ],
              ),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[

                    const Text('Enter values:'),
                    const SizedBox(height: 20,),
                    TextField(
                      // readOnly: true,
                      autofocus: true,
                      controller: _spOxiController, // Assuming this gets updated when scan finishes
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'Sp02',
                      ),
                    ),
                    const SizedBox(height: 10),
                    TextField(
                      // readOnly: true,
                      autofocus: true,
                      controller: _bpmOxiController, // Assuming this gets updated when scan finishes
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'PRbpm',
                      ),
                    ),
                  ],
                ),
              ),
              actions: <Widget>[
                TextButton(
                  child: const Text('Cancel'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                TextButton(
                  child: const Text('Save'),
                  onPressed: () {
                    setState(() {
                      int? getIntValue(String str) {
                        try {
                          final intValue = int.parse(str);
                          return intValue;
                        } catch (e) {
                          return null; // Return null if parsing fails
                        }
                      }
                      List<Map<String, int?>> theOximeterValues = [ {"sp02":getIntValue(_spOxiController.text), "prbpm":getIntValue(_bpmOxiController.text)}];
                      String oximeterToJson(List<Map<String, int?>> theOximeterValues) => json.encode(List<dynamic>.from(theOximeterValues.map((x) => Map.from(x).map((k, v) => MapEntry<String, dynamic>(k, v)))));
                      final oximeterValues = oximeterToJson(theOximeterValues);
                      _myOximeterValues = oximeterValues;
                      print(_myOximeterValues);
                    });
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      },
    );
  }

  void _showWeightForm(BuildContext context) {
    // Now show the dialog.
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder( // Use StatefulBuilder to update the dialog's content
          builder: (context, setState) {
            return AlertDialog(
              title: const Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  // /////////////////

                  // ////////////////
                  Expanded(child: Text('Weight')),
                  // CircularProgressIndicator(), // Reflect current state
                ],
              ),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[

                    const Text('Enter weight:'),
                    const SizedBox(height: 20,),
                    TextField(
                      // readOnly: true,
                      autofocus: true,
                      controller: _weightController, // Assuming this gets updated when scan finishes
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        // hintText: 'Temperature in °C',
                      ),
                    ),
                  ],
                ),
              ),
              actions: <Widget>[
                TextButton(
                  child: const Text('Cancel'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                TextButton(
                  child: const Text('Save'),
                  onPressed: () {
                    // Logic to save the temperature here
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      },
    );
  }

  void _showBloodPressureForm(BuildContext context) {
    // Now show the dialog.
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder( // Use StatefulBuilder to update the dialog's content
          builder: (context, setState) {
            return AlertDialog(
              title: const Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  // /////////////////

                  // ////////////////
                  Expanded(child: Text('Blood Pressure')),
                  // CircularProgressIndicator(), // Reflect current state
                ],
              ),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[

                    const Text('Enter values:'),
                    const SizedBox(height: 5,),
                    TextField(
                      // readOnly: true,
                      autofocus: true,
                      controller: _sysBPController, // Assuming this gets updated when scan finishes
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'SYS (mmHg)',
                      ),
                    ),
                    const SizedBox(height: 5,),
                    TextField(
                      // readOnly: true,
                      autofocus: true,
                      controller: _diaBPController, // Assuming this gets updated when scan finishes
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'DIA (mmHg)',
                      ),
                    ),
                    const SizedBox(height: 5,),
                    TextField(
                      // readOnly: true,
                      autofocus: true,
                      controller: _pulBPController, // Assuming this gets updated when scan finishes
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'PUL (/min)',
                      ),
                    ),
                  ],
                ),
              ),
              actions: <Widget>[
                TextButton(
                  child: const Text('Cancel'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                TextButton(
                  child: const Text('Save'),
                  onPressed: () {
                    setState(() {
                      int? getIntValue(String str) {
                        try {
                          final intValue = int.parse(str);
                          return intValue;
                        } catch (e) {
                          return null; // Return null if parsing fails
                        }
                      }
                      List<Map<String, int?>> theBloodPressureValues = [ {"sys":getIntValue(_sysBPController.text), "dia":getIntValue(_diaBPController.text), "pul":getIntValue(_pulBPController.text)}];
                      String bloodPressureToJson(List<Map<String, int?>> theBloodPressureValues) => json.encode(List<dynamic>.from(theBloodPressureValues.map((x) => Map.from(x).map((k, v) => MapEntry<String, dynamic>(k, v)))));
                      final bloodPressureValues = bloodPressureToJson(theBloodPressureValues);
                      _myBloodPressureValues = bloodPressureValues;
                      // print(_myOximeterValues);
                    });
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      },
    );
  }

  void _showHeightForm(BuildContext context) {
    // Now show the dialog.
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder( // Use StatefulBuilder to update the dialog's content
          builder: (context, setState) {
            return AlertDialog(
              title: const Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  // /////////////////

                  // ////////////////
                  Expanded(child: Text('Height')),
                  // CircularProgressIndicator(), // Reflect current state
                ],
              ),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[

                    const Text('Enter height: (cm)'),
                    const SizedBox(height: 20,),
                    TextField(
                      // readOnly: true,
                      autofocus: true,
                      controller: _heightController, // Assuming this gets updated when scan finishes
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        // hintText: 'Temperature in °C',
                      ),
                    ),
                  ],
                ),
              ),
              actions: <Widget>[
                TextButton(
                  child: const Text('Cancel'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                TextButton(
                  child: const Text('Save'),
                  onPressed: () {
                    // Logic to save the temperature here
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      },
    );
  }

  void _showGlucoseForm(BuildContext context) {
    // Now show the dialog.
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder( // Use StatefulBuilder to update the dialog's content
          builder: (context, setState) {
            return AlertDialog(
              title: const Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  // /////////////////

                  // ////////////////
                  Expanded(child: Text('Glucose Levels')),
                  // CircularProgressIndicator(), // Reflect current state
                ],
              ),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[

                    const Text('Enter level'),
                    const SizedBox(height: 20,),
                    TextField(
                      // readOnly: true,
                      autofocus: true,
                      controller: _glucoseController, // Assuming this gets updated when scan finishes
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'mg/dL',
                      ),
                    ),
                  ],
                ),
              ),
              actions: <Widget>[
                TextButton(
                  child: const Text('Cancel'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                TextButton(
                  child: const Text('Save'),
                  onPressed: () {
                    // Logic to save the temperature here
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      },
    );
  }

//   stepper controlsBuilder start

  Widget _buildStepperControls(ControlsDetails controlsDetails, bool isLastStep) {
    return Row(
      children: [
        ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(
                isLastStep ? Colors.grey : Color(0xff18588d)
            ), // Set button background color
            foregroundColor: MaterialStateProperty.all<Color>(Color(0xffffffff)),
            shape: MaterialStateProperty.all(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(3.0),
              ),
            ),
          ),
          onPressed: isLastStep ? null : controlsDetails.onStepContinue,
          child: Text(isLastStep ? 'Finish' : 'Continue'),
        ),
        SizedBox(width: 16),
        ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(
                _currentStep == 0 ? Colors.grey : Color(0xff18588d)
            ), // Set button background color
            foregroundColor: MaterialStateProperty.all<Color>(Color(0xffffffff)),
            shape: MaterialStateProperty.all(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(3.0),
              ),
            ),
          ),
          onPressed: _currentStep == 0 ? null : controlsDetails.onStepCancel,
          child: Text('Back'),
        ),
      ],
    );
  }

// stepper controlsBuilder end


}
