import 'dart:core';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';
import 'package:uhaiapp/nosql_models/student.dart';
import 'package:uhaiapp/nosql_models/astudent.dart';
import 'package:uhaiapp/nurse/direction_message.dart';
import 'package:uhaiapp/nurse/edit_chronic_illlness.dart';
import 'dart:convert';


import '../nosql_models/exam.dart';



class DiseaseSymptom {
  int id;
  String symptomName;

  DiseaseSymptom({
    required this.id,
    required this.symptomName,
  });

  factory DiseaseSymptom.fromJson(Map<String, dynamic> json) {
    return DiseaseSymptom(
      id: json['id'] is int ? json['id'] : 0,
      symptomName: json['symptomName'] is String ? json['symptomName'] : 'Unknown',
    );
  }
  // ///// added
  Map<String, dynamic> toJson() => {
    "symptomId": id,
    "symptomName": symptomName,
  };


}

class Diseases {
  int id;
  String diseaseName;

  Diseases({
    required this.id,
    required this.diseaseName,
  });

  factory Diseases.fromJson(Map<String, dynamic> json) {
    return Diseases(
      id: json['id'] is int ? json['id'] : 0,
      diseaseName: json['diseaseName'] is String ? json['diseaseName'] : 'Unknown',
    );
  }

  // ///// added
  Map<String, dynamic> toJson() => {
    "id": id,
    "diseaseName": diseaseName,
  };
}

class Prescriptions {
  int id;
  String drugName;

  Prescriptions({
    required this.id,
    required this.drugName,
  });

  factory Prescriptions.fromJson(Map<String, dynamic> json) {
    return Prescriptions(
      id: json['id'] is int ? json['id'] : 0,
      drugName: json['drugName'] is String ? json['drugName'] : 'Unknown',
    );
  }

  // ///// added
  Map<String, dynamic> toJson() => {
    "id": id,
    "drugName": drugName,
  };
}


// //////////

class MedicalProfile extends StatefulWidget {
  final Astudent student;

  const MedicalProfile({Key? key, required this.student}) : super(key: key);

  @override
  _MedicalProfileState createState() => _MedicalProfileState();
}

class _MedicalProfileState extends State<MedicalProfile> {

  final _diseasesCommentController = TextEditingController();
  final _symptomCommentsController = TextEditingController();
  final _tempController = TextEditingController();
  final _weightController = TextEditingController();
  final _heightController = TextEditingController();
  final  _spOxiController = TextEditingController();
  final _bpmOxiController = TextEditingController();
  final _glucoseController = TextEditingController();
  final _sysBPController = TextEditingController();
  final _diaBPController = TextEditingController();
  final _pulBPController = TextEditingController();
  final _drugsCommentController = TextEditingController();

  String _selectedCategoryOption = 'Minor';

  String? _selectedMalariaOption;
  String? _selectedPregnancyOption;
  String? _selectedVaginalInfectionOption;
  String? _selectedCovidOption;

  void _setSelectedMalariaOption(String? value) {
    setState(() {
      _selectedMalariaOption = value;
    });
  }

  void _setSelectedPregnancyOption(String? value) {
    setState(() {
      _selectedPregnancyOption = value;
    });
  }

  void _setSelectedVaginalInfectionOption(String? value) {
    setState(() {
      _selectedVaginalInfectionOption = value;
    });
  }

  void _setSelectedCovidOption(String? value) {
    setState(() {
      _selectedCovidOption = value;
    });
  }

  void _setSelectedCategoryOption(String? value) {
    setState(() {
      _selectedCategoryOption = value!;
    });
  }

  // ///// hive

  // late Box studentBox;
  late Box <Exam>examBox;


  @override
  void initState() {
    super.initState();

    // studentBox = Hive.box('students');
    examBox = Hive.box('exams');

    // _selectedSymptomsId = _selectedSymptoms.map((results) => results.id).toList();
    _tempController.addListener(_updateTempPreview);
    _heightController.addListener(_updateHeightPreview);
    _weightController.addListener(_updateWeightPreview);
    _glucoseController.addListener(_updateGlucosePreview);
    _sysBPController.addListener(_updateSYSBPPreview);
    _diaBPController.addListener(_updateDIABPPreview);
    _pulBPController.addListener(_updatePULBPPreview);
    _spOxiController.addListener(_updateSpoOxiPreview);
    _bpmOxiController.addListener(_updateBPMOxiPreview);

    //   oximeter values change of state
    int? getIntValue(String str) {
      try {
        final intValue = int.parse(str);
        return intValue;
      } catch (e) {
        return null; // Return null if parsing fails
      }
    }
    List<Map<String, int?>> theOximeterValues = [ {"sp02":getIntValue(_spOxiController.text), "prbpm":getIntValue(_bpmOxiController.text)}];
    String oximeterToJson(List<Map<String, int?>> theOximeterValues) => json.encode(List<dynamic>.from(theOximeterValues.map((x) => Map.from(x).map((k, v) => MapEntry<String, dynamic>(k, v)))));
    final oximeterValues = oximeterToJson(theOximeterValues);
    _myOximeterValues = oximeterValues;
    // print(_myOximeterValues);

    //   /////blood pressure values change state

    List<Map<String, int?>> theBloodPressureValues = [ {"sys":getIntValue(_sysBPController.text), "dia":getIntValue(_diaBPController.text), "pul":getIntValue(_pulBPController.text)}];
    String bloodPressureToJson(List<Map<String, int?>> theBloodPressureValues) => json.encode(List<dynamic>.from(theBloodPressureValues.map((x) => Map.from(x).map((k, v) => MapEntry<String, dynamic>(k, v)))));
    final bloodPressureValues = bloodPressureToJson(theBloodPressureValues);
    _myBloodPressureValues = bloodPressureValues;
    // print(_myOximeterValues);






  }

  void _updateTempPreview() {
    setState(() {
    });
  }
  void _updateWeightPreview() {
    setState(() {
    });
  }
  void _updateHeightPreview() {
    setState(() {
    });
  }

  void _updateGlucosePreview() {
    setState(() {
    });
  }

  void _updateSpoOxiPreview() {
    setState(() {
    });
  }

  void _updateBPMOxiPreview() {
    setState(() {
    });
  }

  void _updateSYSBPPreview() {
    setState(() {
    });
  }

  void _updateDIABPPreview() {
    setState(() {
    });
  }

  void _updatePULBPPreview() {
    setState(() {
    });
  }


  @override
  void dispose() {

    _tempController.removeListener(_updateTempPreview);
    _heightController.removeListener(_updateTempPreview);
    _weightController.addListener(_updateWeightPreview);
    _glucoseController.removeListener(_updateTempPreview);
    _sysBPController.removeListener(_updateTempPreview);
    _diaBPController.removeListener(_updateTempPreview);
    _pulBPController.removeListener(_updateTempPreview);
    _spOxiController.removeListener(_updateTempPreview);
    _bpmOxiController.removeListener(_updateTempPreview);
    // Clean up the listener when the widget is disposed.

    _tempController.dispose();
    _heightController.dispose();
    _weightController.dispose();
    _glucoseController.dispose();
    _sysBPController.dispose();
    _diaBPController.dispose();
    _pulBPController.dispose();
    _spOxiController.dispose();
    _bpmOxiController.dispose();


    super.dispose();
  }

  int _currentStep = 0;

  final List<DiseaseSymptom> _symptoms = [
    DiseaseSymptom(id: 1, symptomName: "Fever"),
    DiseaseSymptom(id: 2, symptomName: "Cough"),
    DiseaseSymptom(id: 3, symptomName: "Fatigue"),
    DiseaseSymptom(id: 4, symptomName: "Nausea"),
    DiseaseSymptom(id: 5, symptomName: "Headache"),
    DiseaseSymptom(id: 6, symptomName: "Dizziness"),
    DiseaseSymptom(id: 7, symptomName: "Shortness of breath"),
    DiseaseSymptom(id: 8, symptomName: "Chest pain"),
    DiseaseSymptom(id: 9, symptomName: "Loss of taste or smell"),
    DiseaseSymptom(id: 10, symptomName: "Muscle or joint pain"),
    DiseaseSymptom(id: 11, symptomName: "Pale skin"),
    DiseaseSymptom(id: 12, symptomName: "Red eyes"),
    DiseaseSymptom(id: 13, symptomName: "Itching skin"),
    DiseaseSymptom(id: 14, symptomName: "Paralyzed"),
    DiseaseSymptom(id: 15, symptomName: "Stomachache"),
    DiseaseSymptom(id: 16, symptomName: "No appetite"),
    DiseaseSymptom(id: 17, symptomName: "Loss of hair"),
    DiseaseSymptom(id: 18, symptomName: "Blurred vision"),
    // Add your symptoms here...
  ];

  final List<Diseases> _diseases = [
    Diseases(id: 1, diseaseName: "Malaria"),
    Diseases(id: 2, diseaseName: "Typhoid"),
    Diseases(id: 3, diseaseName: "Sexually Transmitted Infection"),
    Diseases(id: 4, diseaseName: "Urinary Tract Infection"),
    Diseases(id: 5, diseaseName: "Common Cold"),
    Diseases(id: 6, diseaseName: "Pneumonia"),
    Diseases(id: 7, diseaseName: "Asthma"),
    Diseases(id: 8, diseaseName: "Diabetes"),
    Diseases(id: 9, diseaseName: "Hypertension"),
    Diseases(id: 10, diseaseName: "Migraine"),
    Diseases(id: 11, diseaseName: "Measles"),
    Diseases(id: 12, diseaseName: "Fever"),
    Diseases(id: 13, diseaseName: "Coronary Artery Disease"),
    Diseases(id: 14, diseaseName: "Gastroenteritis"),
    Diseases(id: 15, diseaseName: "Depression"),


    // Add your symptoms here...
  ];

  final List<Prescriptions> _drugs = [
    Prescriptions(id: 1, drugName: "Paracetamol"),
    Prescriptions(id: 2, drugName: "Amoxicillin"),
    Prescriptions(id: 3, drugName: "Vitamin C"),
    Prescriptions(id: 4, drugName: "Cough Syrup"),
    Prescriptions(id: 5, drugName: "Magnesium"),
    Prescriptions(id: 6, drugName: "Cetirizine"),
    Prescriptions(id: 7, drugName: "Metformin"),
    Prescriptions(id: 8, drugName: "Pantoprazole"),
    Prescriptions(id: 9, drugName: "Pravastatin"),
    Prescriptions(id: 10, drugName: "Sertraline"),
    Prescriptions(id: 11, drugName: "Simvastatin"),
    Prescriptions(id: 12, drugName: "Gabapentin"),
    Prescriptions(id: 13, drugName: "Losartan"),
    Prescriptions(id: 14, drugName: "Omeprazole"),
    Prescriptions(id: 15, drugName: "Albuterol"),
    Prescriptions(id: 16, drugName: "Metoprolol"),
    Prescriptions(id: 17, drugName: "Amlodipine"),
    Prescriptions(id: 18, drugName: "Mabendazo"),
    Prescriptions(id: 19, drugName: "Levothyroxine"),
    Prescriptions(id: 20, drugName: "Atorvastatin"),

  ];

  String? _myOximeterValues;
  String? _myBloodPressureValues;
  String? _myRapidTests;

  List<DiseaseSymptom> _selectedSymptoms = [];
  String? _mySelectedSymptoms;
  // late List<DiseaseSymptom> _mySelectedSymptoms;

  List<Diseases> _selectedDiseases = [];
  String? _mySelectedDiseases;

  List<Prescriptions> _selectedDrugs = [];
  String? _mySelectedDrugs;
  // send data to db start

  final storeExamUrl = "https://copint.org/device_test/postExam.php";
  // ///// post method starts here

  // ///// post method starts here
  
// Ensure this is part of your stateful widget class or has access to a BuildContext
  void saveExamLocally(BuildContext context) {
    double? tryParseDouble(String? text) {
      if (text == null || text.isEmpty) {
        return null;  // Return null if text is null or empty
      }
      try {
        return double.parse(text);
      } catch (e) {
        return null;  // Return null if parsing fails
      }
    }

    try {
      examBox.add(
          Exam(
            examId: 18,
            createdAt: DateTime.now(),
            studentId: widget.student.id,
            nurseId: '1',
            symptoms: jsonDecode(_mySelectedSymptoms ?? ''),
            symptomsComment: _symptomCommentsController.text,
            temperature: tryParseDouble(_tempController.text),
            weight: tryParseDouble(_weightController.text),
            oximeter: jsonDecode(_myOximeterValues ?? " "),
            bloodPressure: jsonDecode(_myBloodPressureValues ?? " "),
            glucose: tryParseDouble(_glucoseController.text),
            height: tryParseDouble(_heightController.text),
            rapidTests: jsonDecode(_myRapidTests ?? " "),
            examCategory: _selectedCategoryOption,
            diseases: jsonDecode(_mySelectedDiseases ?? ''),
            diseasesComment: _diseasesCommentController.text,
            drugs: jsonDecode(_mySelectedDrugs ?? ''),
            drugsComment: _drugsCommentController.text,
          )
      );

      // Show success dialog
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Success'),
            content: const Text('Medical exam saved.'),
            actions: <Widget>[
              TextButton(
                child: const Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop(); // Close the dialog
                },
              ),
            ],
          );
        },
      );
    } catch (e) {
      // Show error dialog if saving fails
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Error'),
            content: Text('Failed to save medical exam. Error: $e'),
            actions: <Widget>[
              TextButton(
                child: const Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop(); // Close the dialog
                },
              ),
            ],
          );
        },
      );
    }
  }


  // //// post method ends here

  // //// post method ends here

  // send data to bd end


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xff18588d),
        title: Text("Medical profile", style: const TextStyle(color: Color(0xffffffff))),
        iconTheme: const IconThemeData(
          color: Color(0xffffffff),
        ),
      ),
      body: SingleChildScrollView(
        // scrollDirection: Axis.vertical,
        child:
        Column(
          children: [

            Padding(
              padding: EdgeInsets.all(8.0),
              child: ExpansionTile(
                initiallyExpanded:true,
                collapsedBackgroundColor: const Color(0xff949494),
                collapsedTextColor: const Color(0xff000000),
                title: Text('Chronic Illnesses', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                trailing: Icon(Icons.expand_more),
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        GestureDetector(
                            onTap: () {
                              // Navigator.push(
                              //   context,
                              //   MaterialPageRoute(builder: (context) =>  EditChronicIllness(student: widget.student)),
                              // );
                            },
                            child: Icon(Icons.add_circle_outline)
                        ),
                      ],
                    ),),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Wrap(
                        alignment: WrapAlignment.start,
                        spacing: 8.0,
                        children: widget.student.chronicIllness.map((illness) {
                          return InputChip(
                            onSelected: (bool value) {},
                            backgroundColor: Color(0xffc8c8c8),
                            label: Text(
                              illness['condition_name'],
                              style: TextStyle(color: Color(0xff000000)),
                            ),
                            avatar: Icon(
                              Icons.check,
                              size: 20.0,
                              color: Color(0xff000000),
                            ),
                          );
                        }).toList(),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Align(
                        alignment: Alignment.topLeft,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Text("Comment", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                            SizedBox(height: 15,),
                            Text(widget.student.chronicIllnessComment!),
                          ],

                        )
                    ),

                  )

                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: ExpansionTile(
                initiallyExpanded:true,
                collapsedBackgroundColor: const Color(0xff949494),
                collapsedTextColor: const Color(0xff000000),
                title: Text('Family history', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                trailing: Icon(Icons.expand_more),
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        GestureDetector(
                            onTap: () {
                              // Navigator.push(
                              //   context,
                              //   MaterialPageRoute(builder: (context) =>  EditChronicIllness(student: widget.student)),
                              // );
                            },
                            child: Icon(Icons.add_circle_outline)
                        ),
                      ],
                    ),),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Wrap(
                        alignment: WrapAlignment.start,
                        spacing: 8.0,
                        children: widget.student.chronicIllness.map((illness) {
                          return InputChip(
                            onSelected: (bool value) {},
                            backgroundColor: Color(0xffc8c8c8),
                            label: Text(
                              illness['condition_name'],
                              style: TextStyle(color: Color(0xff000000)),
                            ),
                            avatar: Icon(
                              Icons.check,
                              size: 20.0,
                              color: Color(0xff000000),
                            ),
                          );
                        }).toList(),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Align(
                        alignment: Alignment.topLeft,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Text("Comment", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                            SizedBox(height: 15,),
                            Text(widget.student.chronicIllnessComment!),
                          ],

                        )
                    ),

                  )

                ],
              ),
            ),



          ],
        ),




      ),
    );
  }

//   /////////9999999

  // Helper method to build a TableRow with given data
  TableRow buildTableRow(List<String> data) {
    return TableRow(
      children: data.map((cellData) {
        return TableCell(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: Text(cellData),
            ),
          ),
        );
      }).toList(),
    );
  }

// /////////9999999



//   ////////////////// vitals methods



}
