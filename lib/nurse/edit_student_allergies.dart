import 'dart:convert';
import 'dart:core';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:multi_select_flutter/multi_select_flutter.dart';
import 'allStudents.dart';


class Allergies {
  int id;
  String condition_name;

  Allergies({
    required this.id,
    required this.condition_name,
  });

  factory Allergies.fromJson(Map<String, dynamic> json) {
    return Allergies(
      id: json['condition'],
      condition_name: json['condition_name'] is String ? json['condition_name'] : 'Unknown',
    );
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'condition_name': condition_name,
  };
}

class EditStudAllergies extends StatefulWidget {
  final Student student;

  const EditStudAllergies({Key? key, required this.student}) : super(key: key);

  @override
  _EditStudAllergiesState createState() => _EditStudAllergiesState();
}

class _EditStudAllergiesState extends State<EditStudAllergies> {
  final _allergiesCommentController = TextEditingController();

  List<Allergies> _allergies = [];
  List<Allergies> _selectedAllergies = [];
  List<Allergies>? _mySelectedAllergies;
  List<Map<String, String>> transformedList = [];

  @override
  void initState() {
    super.initState();
    _fetchAllergies();
  }

  Future<void> _fetchAllergies() async {
    try {
      final response = await http.get(Uri.parse('https://uhaihealthcare.com/api/v1/school/medical-conditions/condition/1/'));
      if (response.statusCode == 200) {
        List<dynamic> data = json.decode(response.body);
        setState(() {
          _allergies = data.map((json) => Allergies.fromJson(json)).toList();
        });
      } else {
        throw Exception('Failed to load allergies');
      }
    } catch (e) {
      print('Failed to load allergies: $e');
    }
  }

  @override
  void dispose() {
    _allergiesCommentController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xff18588d),
        title: Text("${widget.student.first_name}", style: const TextStyle(color: Color(0xffffffff))),
        iconTheme: const IconThemeData(
          color: Color(0xffffffff),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(16.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Edit / Add Allergies", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.all(16.0),
              child: _allergies.isEmpty
                  ? Center(child: CircularProgressIndicator())
                  : MultiSelectDialogField(
                searchable: true,
                items: _allergies.map((allergies) => MultiSelectItem<Allergies>(allergies, allergies.condition_name)).toList(),
                title: const Text("Allergies"),
                selectedColor: Theme.of(context).primaryColor,
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor.withOpacity(0.1),
                  border: Border.all(
                    color: Theme.of(context).primaryColor,
                    width: 2,
                  ),
                ),
                buttonIcon: Icon(
                  Icons.add,
                  color: Theme.of(context).primaryColor,
                ),
                buttonText: const Text(
                  "Select Allergies",
                  style: TextStyle(
                    color: Color(0xff000000),
                    fontSize: 16,
                  ),
                ),
                onConfirm: (results) {
                  setState(() {
                    _selectedAllergies = List<Allergies>.from(results);
                    _mySelectedAllergies = _selectedAllergies;
                    transformedList = transformAllergiesList(_mySelectedAllergies);

                  });
                },
                chipDisplay: MultiSelectChipDisplay(
                  chipColor: const Color(0xffc5c5c5),
                  textStyle: const TextStyle(
                    color: Color(0xff000000),
                  ),
                  onTap: (value) {
                    setState(() {
                      _selectedAllergies.remove(value);
                    });
                  },
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(16.0),
              child: TextField(
                controller: _allergiesCommentController,
                maxLines: 5,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Add comments about the allergies',
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(16.0),
              child: ElevatedButton(
                onPressed: () {
                  saveAllergies(context);
                },
                style: ElevatedButton.styleFrom(
                  foregroundColor: const Color(0xffffffff),
                  backgroundColor: const Color(0xff18588d),
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.zero,
                  ),
                ),
                child: const Text('Save allergies'),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> saveAllergies(BuildContext context) async {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                CircularProgressIndicator(),
                SizedBox(width: 20),
                Text("Saving data..."),
              ],
            ),
          ),
        );
      },
    );

    Map<String, dynamic> allergies_data = {
      "student_id": widget.student.id,
      'condition_type': 1,
      'selected_conditions': transformedList,
      'condition_comment': _allergiesCommentController.text,
    };

    String encodedData = jsonEncode(allergies_data);
    print(encodedData);
    // try catch starts here

    try {
      final response = await http.post(
        Uri.parse('https://uhaihealthcare.com/api/v1/school/add-student-medical-conditions/'),
        headers: {
          'Content-Type': 'application/json',
        },
        body: encodedData,
      );
      Navigator.pop(context);
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Response', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xff18588d))),
            content: Text(jsonDecode(response.body)['message'] ?? '', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xff008000))),
            actions: <Widget>[
              TextButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    } catch (e) {
      Navigator.pop(context);

      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Error', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xff000000))),
            content: Text('An error occurred: $e', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xffff0000))),
            actions: <Widget>[
              TextButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
    //   try catch ends here

  }
}

List<Map<String, String>> transformAllergiesList(List<Allergies>? allergiesList) {
  if (allergiesList == null) {
    return [];
  }
  return allergiesList.map((illness) {
    return {"condition_name": illness.id.toString()};

  }).toList();
}
