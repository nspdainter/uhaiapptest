import 'dart:core';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';
// import 'package:uhaiapp/nosql_models/student.dart';
import 'package:uhaiapp/nosql_models/astudent.dart';
import 'dart:convert';
import 'package:uhaiapp/nurse/studts.dart';
import 'allStudents.dart';

import '../nosql_models/exam.dart';
import 'direction_message.dart';

class FamilyHistory {
  String condition_name;

  FamilyHistory({
    required this.condition_name,
  });

  factory FamilyHistory.fromJson(Map<String, dynamic> json) {
    return FamilyHistory(
      condition_name: json['condition_name'] is String ? json['condition_name'] : 'Unknown',
    );
  }
  // ///// added
  Map<String, dynamic> toJson() => {
    "condition_name": condition_name,
  };


}
// //////////

class EditFamilyHistory extends StatefulWidget {
  
  final Student student;

  const EditFamilyHistory({Key? key, required this.student}) : super(key: key);

  @override
  _EditFamilyHistoryState createState() => _EditFamilyHistoryState();
}

class _EditFamilyHistoryState extends State<EditFamilyHistory> {

  final _familyHistoryCommentController = TextEditingController();

  // ///// hive

  // late Box studentBox;
  late Box <Exam>examBox;
  


  @override
  void initState() {
    super.initState();

    // studentBox = Hive.box('students');
    examBox = Hive.box('exams');
    // localstudentBox = Hive.box('localstudents');


    //   /////blood pressure values change state


  }



  @override
  void dispose() {
    super.dispose();
  }

  final List<FamilyHistory> _familyHistory = [
    FamilyHistory(condition_name: "Diabetes"),
    FamilyHistory(condition_name: "Ulcers (PUD)"),
    FamilyHistory(condition_name: "Sickle Cell Disease"),
    FamilyHistory(condition_name: "Sickle Cell Trait (Carrier)"),
    FamilyHistory(condition_name: "Asthma"),
    FamilyHistory(condition_name: "Hypertension"),
    FamilyHistory(condition_name: "Eye condition"),
    FamilyHistory(condition_name: "Epilepsy"),
    FamilyHistory(condition_name: "Psychiatric disorders"),
    FamilyHistory(condition_name: "Autism Spectrum Disorder (ASD) "),
    FamilyHistory(condition_name: "Sleep disorder"),
    FamilyHistory(condition_name: "COPD - Chronic Obstructive Pulmonary Disease "),
    FamilyHistory(condition_name: "HIV/ AIDS"),
    FamilyHistory(condition_name: "Hepatitis ("),
    FamilyHistory(condition_name: "Metabolic syndrome"),
    FamilyHistory(condition_name: "Thyroid hormone resistance"),
    FamilyHistory(condition_name: "Heart Disease"),
    FamilyHistory(condition_name: "Chronic Kidney Disease"),

  ];

  List<FamilyHistory> _selectedFamilyHistory = [];
  String? _mySelectedFamilyHistory;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xff18588d),
        title: Text("${widget.student.first_name}", style: const TextStyle(color: Color(0xffffffff))),
        iconTheme: const IconThemeData(
          color: Color(0xffffffff),
        ),
      ),
      body: SingleChildScrollView(
        // scrollDirection: Axis.vertical,
        child:
        Column(
          children: [
            Padding(
              padding: EdgeInsets.all(16.0),
              child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  // alignment: ,
                  children: [
                    Text("Edit / Add Family History", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                  ]

              )
            ),

            // Padding(
            //   padding: EdgeInsets.all(16.0),
            //   child: TextFormField(
            //     readOnly: true,
            //     initialValue: 'Family History', // Set your pre-filled value here
            //     decoration: InputDecoration(
            //       labelText: 'Condition type',
            //       border: OutlineInputBorder(),
            //     ),
            //   ),
            // ),

            Padding(
                padding: EdgeInsets.all(16.0),
                child: MultiSelectDialogField(
                  searchable: true,
                  items: _familyHistory.map((familyHistory) => MultiSelectItem<FamilyHistory>(familyHistory, familyHistory.condition_name)).toList(),
                  title: const Text("Family History"),
                  selectedColor: Theme.of(context).primaryColor,
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor.withOpacity(0.1),
                    // borderRadius: BorderRadius.all(Radius.circular(0)),
                    border: Border.all(
                      color: Theme.of(context).primaryColor,
                      width: 2,
                    ),
                  ),
                  buttonIcon: Icon(
                    Icons.add,
                    color: Theme.of(context).primaryColor,
                  ),
                  buttonText: const Text(
                    "Select Family History",
                    style: TextStyle(
                      color: Color(0xff000000),
                      fontSize: 16,
                    ),
                  ),
                  onConfirm: (results) {
                    setState(() {
                      // List<Symptoms> symptomsFromJson(String str) => List<Symptoms>.from(json.decode(str).map((x) => Symptoms.fromJson(x)));
                      _selectedFamilyHistory = List<FamilyHistory>.from(results);
                      String chronicToJson(List<FamilyHistory> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));
                      final myFamilyHistory = chronicToJson(_selectedFamilyHistory);
                      _mySelectedFamilyHistory = myFamilyHistory as String?;
                      print(_mySelectedFamilyHistory);
                    });

                  },
                  chipDisplay: MultiSelectChipDisplay(
                    chipColor: const Color(0xffc5c5c5),
                    textStyle: const TextStyle(
                      color: Color(0xff000000),
                    ),
                    onTap: (value) {
                      setState(() {
                        _selectedFamilyHistory.remove(value);
                      });
                    },
                  ),
                ),
            ),

            Padding(
              padding: EdgeInsets.all(16.0),
              child: TextField(
                controller: _familyHistoryCommentController,
                maxLines: 5,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Add comments about family history'
                ),
              ),
            ),

            Padding(
              padding: EdgeInsets.all(16.0),
              child: ElevatedButton(
                onPressed: () {
                  saveFamilyHistory(context);
                },
                style: ElevatedButton.styleFrom(
                  foregroundColor: const Color(0xffffffff),
                  backgroundColor: const Color(0xff18588d),
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.zero, // No rounded corners
                  ),
                ),
                child: const Text('Save Family History'),
              ),
            ),

          ],
        ),


      ),
    );
  }

  Future<void> saveFamilyHistory(BuildContext context) async {
    //
    showDialog(
      context: context,
      barrierDismissible: false, // Prevent dialog from closing on tap
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                CircularProgressIndicator(),
                SizedBox(width: 20),
                Text("Saving data..."),
              ],
            ),
          ),
        );
      },
    );

    // Prepare the data for saving
    Map<String, dynamic> family_history_data = {
      'student_id': widget.student.id,
      'condition_type': "Family History",
      'selected_conditions' : jsonDecode(_mySelectedFamilyHistory ?? ''),
      'condition_comment': _familyHistoryCommentController.text,

    };

    String encodedData = jsonEncode(family_history_data);

// ////////////////////

    try {
      // Send data to server
      final response = await http.post(
        Uri.parse('https://uhaihealthcare.com/api/v1/school/add-student-medical-conditions/'),
        headers: {
          'Content-Type': 'application/json',
        },
        body: encodedData,
      );

      // Close initial dialog
      Navigator.pop(context);

      // Show response dialog
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Response' , style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xff18588d)),),
            content: Text(jsonDecode(response.body)['message'] ?? '', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xff008000)),),

            actions: <Widget>[
              TextButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    } catch (e) {
      // Close initial dialog
      Navigator.pop(context);

      // Show error dialog
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Error', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xff000000)),),
            content: Text('An error occurred: $e', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xffff0000)),),

            actions: <Widget>[
              TextButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }

    // ///////////////////


  }




}
