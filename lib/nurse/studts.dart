import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:uhaiapp/nurse/studentDetails.dart';
import 'package:uhaiapp/nurse/studt_details.dart';


// /////// student class start

class Studt {
  int? id;
  List<dynamic>? familyHistory;
  List<dynamic>? schoolHistory;
  List<dynamic>? studentNok;
  DateTime? created;
  DateTime? updated;
  String? firstName;
  String? lastName;
  String? gender;
  DateTime? dateOfBirth;
  String? email;
  String? phone;
  String? aakey;
  int? activeStatus;
  String? nin;
  String? address;
  String? nationality;
  String? passportNumber;
  String? studentUniqueId;

  Studt({
    this.id,
    this.familyHistory,
    this.schoolHistory,
    this.studentNok,
    this.created,
    this.updated,
    this.firstName,
    this.lastName,
    this.gender,
    this.dateOfBirth,
    this.email,
    this.phone,
    this.aakey,
    this.activeStatus,
    this.nin,
    this.address,
    this.nationality,
    this.passportNumber,
    this.studentUniqueId,
  });

  Studt.fromJson(Map<String, dynamic> json) {
    id = json["id"] as int?;
    familyHistory = json["family_history"] != null ? List<dynamic>.from(json["family_history"]!) : null;
    schoolHistory = json["school_history"] != null ? List<dynamic>.from(json["school_history"]!) : null;
    studentNok = json["student_nok"] != null ? List<dynamic>.from(json["student_nok"]!) : null;
    created = json['created'] != null ? DateTime.parse(json['created']) : null;
    updated = json['updated'] != null ? DateTime.parse(json['updated']) : null;
    firstName = json["first_name"] as String?;
    lastName = json["last_name"] as String?;
    gender = json["gender"] as String?;
    dateOfBirth = json['dateOfBirth'] != null ? DateTime.parse(json['dateOfBirth']) : null;
    email = json["email"] as String?;
    phone = json["phone"] as String?;
    aakey = json["aakey"] as String?;
    activeStatus = json["active_status"] as int?;
    nin = json["nin"] as String?;
    address = json["address"] as String?;
    nationality = json["nationality"] as String?;
    passportNumber = json["passport_number"] as String?;
    studentUniqueId = json["student_unique_id"] as String?;
  }


}

// ////// student class end

// /////////

Future<List<Studt>> getStudents() async {

  var url = Uri.parse("https://uhaihealthcare.com/api/v1/school/students/school/1");
  final response = await http.get(url, headers: {"Content-Type": "application/json"});
  final List body = json.decode(response.body);
  return body.map((e) => Studt.fromJson(e)).toList();
}

// /////////


class Studts extends StatefulWidget {
  @override
  _StudtsState createState() => _StudtsState();
}

class _StudtsState extends State<Studts> {
  // List<dynamic> jsonData = [];

  late Future<List<Studt>> studentsFuture;
  final TextEditingController searchController = TextEditingController();
  String searchQuery = "";


  @override
  void initState() {
    super.initState();
    studentsFuture = getStudents();
  }

  @override
  Widget build(BuildContext context) {

    return  Scaffold(
        appBar: AppBar(
          backgroundColor:  const Color(0xff18588d),
          title: const Text('All students',
            style: TextStyle(color: Color(0xffffffff)),
          ),
          iconTheme: const IconThemeData(
            color: Color(0xffffffff),
          ),

        ),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: searchController,
                decoration: const InputDecoration(
                  labelText: 'Search student',
                  suffixIcon: Icon(Icons.search),
                ),
                onChanged: (value) {
                  setState(() {
                    searchQuery = value.toLowerCase();
                  });
                },
              ),
            ),
            Expanded(
              child: FutureBuilder<List<Studt>>(
                future: studentsFuture,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return
                      // //////// pre-loader start
                      const Center(
                        child: SizedBox(
                            width: 50,
                            height: 50,
                            child: CircularProgressIndicator()
                        ),);
                    ///////// pre-loader end
                  } else if (snapshot.hasData) {
                    final students = snapshot.data!.where((student) {
                      // Search filter
                      final fullName = '${student.firstName} ${student.lastName}'.toLowerCase();
                      return fullName.contains(searchQuery);
                    }).toList();
                    return buildStudents(students);
                  } else if (snapshot.hasError) {
                    return Text('${snapshot.error}');
                  } else {
                    return const Text("No students data available");
                  }
                },
              ),

            ),
          ],
        ),
      );

  }

  //   build student widget
  Widget buildStudents(List<Studt> students) {
    return ListView.builder(
      itemCount: students.length,
      itemBuilder: (context, index) {
        final student = students[index];
        String? firstname = student.firstName;
        String? lastname = student.lastName;
        // String? initials = "${firstname?[0]}${lastname?[0]}";
        String? initials = (firstname != null && lastname != null) ? "${firstname[0]}${lastname[0]}" : null;
        return Container(
          color: Colors.grey.shade100,
          // margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
          padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 5),
          height: 100,
          width: double.maxFinite,
          margin: const EdgeInsets.only(bottom: 20),
          child: ListTile(
            leading: CircleAvatar(
              radius: 30.0,
              backgroundColor: const Color(0xffe0e0e0),
              child: Text(
                  initials?.toUpperCase() ?? "", style: const TextStyle(fontSize: 20)
              ),
            ),
            title:
            Row(
              children: <Widget>[
                Flexible(
                  child: Text(
                    student.firstName ?? "",
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                const SizedBox(width: 20),
                Flexible(
                  child: Text(
                    student.lastName ?? "",
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ],
            ),
            subtitle: Text(student.gender ?? ""),
            trailing: const Icon(Icons.chevron_right),
            onTap: () => {
              Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => StudtDetails(student: student,)),
              ),
            },
          ),

          // child: Row(
          //   children: [
          //     Text(student.firstName!),
          //   ],
          // ),
        );
      },
    );
  }
// build student widget


}
