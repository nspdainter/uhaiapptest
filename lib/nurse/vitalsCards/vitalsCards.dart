import 'package:flutter/material.dart';


// BMICard start

class BMICard extends StatefulWidget {
  final double bmi;

  const BMICard({super.key, required this.bmi});

  @override
  _BMICardState createState() => _BMICardState();
}

class _BMICardState extends State<BMICard> {
  Color getBorderColor() {
    if (widget.bmi < 18.5) {
      return Colors.orange;
    } else if (widget.bmi >= 18.5 && widget.bmi <= 24.9) {
      return Colors.green;
    } else if (widget.bmi >= 25.0 && widget.bmi <= 29.9) {
      return Colors.orange;
    } else {
      return Colors.red;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        side: BorderSide(
          color: getBorderColor(), // Dynamic border color based on BMI
          width: 4.0,
        ),
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: Container(
        width: 100,
        height: 60,
        color: const Color(0xffc8c8c8),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text('BMI:'),
            Text(
              widget.bmi.toStringAsFixed(1), // Display the passed BMI value
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}

// BMICard end


// TempCard start

class TempCard extends StatefulWidget {
  final double? temp;

  const TempCard({Key? key, required this.temp}) : super(key: key);

  @override
  _TempCardState createState() => _TempCardState();
}

class _TempCardState extends State<TempCard> {
  Color getBorderColor() {
    if (widget.temp != null) {
      if (widget.temp! < 35) {
        return Colors.blue;
      } else if (widget.temp! >= 35.5 && widget.temp! <= 37.5) {
        return Colors.green;
      } else if (widget.temp! >= 38.3 && widget.temp! <= 38.3) {
        return Colors.orange;
      }
    }
    return Colors.red;
  }

  String getTempText() {
    if (widget.temp != null) {
      return widget.temp!.toStringAsFixed(1); // Display the passed temp value
    } else {
      return 'null'; // Display 'null' if temp is null
    }
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        side: BorderSide(
          color: getBorderColor(), // Dynamic border color based on temp
          width: 4.0,
        ),
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: Container(
        width: 100,
        height: 60,
        color: const Color(0xffc8c8c8),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text('Temp:'),
            Text(
              getTempText(),
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}

// TempCard end

// WeightCard start

class WeightCard extends StatefulWidget {
  final double? weight;

  const WeightCard({Key? key, required this.weight}) : super(key: key);

  @override
  _WeightCardState createState() => _WeightCardState();
}

class _WeightCardState extends State<WeightCard> {
  Color getBorderColor() {
    if (widget.weight != null) {
      if (widget.weight! < 20.0) {
        return Colors.blue;
      } else if (widget.weight! >= 21.0 && widget.weight! <= 70.0) {
        return Colors.green;
      } else if (widget.weight! >= 71.0 && widget.weight! <= 120.0) {
        return Colors.orange;
      }
    }
    return Colors.red;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        side: BorderSide(
          color: getBorderColor(), // Dynamic border color based on weight
          width: 4.0,
        ),
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: Container(
        width: 100,
        height: 60,
        color: const Color(0xffc8c8c8),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text('Weight:'),
            Text(
              widget.weight != null ? widget.weight!.toStringAsFixed(1) : 'null',
              // Display the passed weight value or 'null' if it's null
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}

// WeightCard end

// glucoseCard start

class GlucoseCard extends StatefulWidget {
  final double? glucose;

  const GlucoseCard({Key? key, required this.glucose}) : super(key: key);

  @override
  _GlucoseCardState createState() => _GlucoseCardState();
}

class _GlucoseCardState extends State<GlucoseCard> {
  Color getBorderColor() {
    if (widget.glucose != null) {
      if (widget.glucose! < 70.0) {
        return Colors.blue;
      } else if (widget.glucose! >= 71.0 && widget.glucose! <= 125.0) {
        return Colors.green;
      } else if (widget.glucose! >= 126.0 && widget.glucose! <= 140.0) {
        return Colors.orange;
      }
    }
    return Colors.red;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        side: BorderSide(
          color: getBorderColor(), // Dynamic border color based on glucose level
          width: 4.0,
        ),
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: Container(
        width: 100,
        height: 60,
        color: const Color(0xffc8c8c8),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text('Glucose:'),
            Text(
              widget.glucose != null ? widget.glucose!.toStringAsFixed(1) : 'null',
              // Display the passed glucose value or 'null' if it's null
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}


// glucoseCard end

// sp02Card start

class Sp02Card extends StatefulWidget {
  final int? sp02;

  const Sp02Card({Key? key, required this.sp02}) : super(key: key);

  @override
  _Sp02CardState createState() => _Sp02CardState();
}

class _Sp02CardState extends State<Sp02Card> {
  Color getBorderColor() {
    if (widget.sp02 != null) {
      if (widget.sp02! < 75) {
        return Colors.red;
      } else if (widget.sp02! >= 75 && widget.sp02! <= 94) {
        return Colors.orange;
      } else if (widget.sp02! >= 95 && widget.sp02! <= 100) {
        return Colors.green;
      }
    }
    return Colors.black;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        side: BorderSide(
          color: getBorderColor(), // Dynamic border color based on sp02 level
          width: 4.0,
        ),
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: Container(
        width: 100,
        height: 60,
        color: const Color(0xffc8c8c8),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text('Sp02:'),
            Text(
              widget.sp02 != null ? widget.sp02!.toString() : 'null',
              // Display the passed sp02 value or 'null' if it's null
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}

// sp02Card end


// PbrmCard start

class PbrmCard extends StatefulWidget {
  final int? pbrm;

  const PbrmCard({Key? key, required this.pbrm}) : super(key: key);

  @override
  _PbrmCardState createState() => _PbrmCardState();
}

class _PbrmCardState extends State<PbrmCard> {
  Color getBorderColor() {
    if (widget.pbrm != null) {
      if (widget.pbrm! < 50) {
        return Colors.red;
      } else if (widget.pbrm! >= 50 && widget.pbrm! <= 60) {
        return Colors.orange;
      } else if (widget.pbrm! >= 60 && widget.pbrm! <= 100) {
        return Colors.green;
      }
    }
    return Colors.black;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        side: BorderSide(
          color: getBorderColor(), // Dynamic border color based on pbrm level
          width: 4.0,
        ),
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: Container(
        width: 100,
        height: 60,
        color: const Color(0xffc8c8c8),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text('PRbpm:'),
            Text(
              widget.pbrm != null ? widget.pbrm!.toStringAsFixed(1) : 'null', // Display the pbrm value or 'null' if it's null
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}


// PbrmCard end


// SysCard start

class SysCard extends StatefulWidget {
  final int? sys;

  const SysCard({Key? key, required this.sys}) : super(key: key);

  @override
  _SysCardState createState() => _SysCardState();
}

class _SysCardState extends State<SysCard> {
  Color getBorderColor() {
    if (widget.sys != null) {
      if (widget.sys! < 90) {
        return Colors.red;
      } else if (widget.sys! >= 90 && widget.sys! <= 120) {
        return Colors.green;
      } else if (widget.sys! >= 120 && widget.sys! <= 139) {
        return Colors.orange;
      }
    }
    return Colors.black; // Default color if sys is null
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        side: BorderSide(
          color: getBorderColor(), // Dynamic border color based on sys level
          width: 4.0,
        ),
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: Container(
        width: 100,
        height: 60,
        color: const Color(0xffc8c8c8),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text('SYS:'),
            Text(
              widget.sys != null ? widget.sys!.toStringAsFixed(1) : 'null', // Display the sys value or 'null' if it's null
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}


// SysCard end

// DiaCard start

class DiaCard extends StatefulWidget {
  final int? dia;

  const DiaCard({Key? key, required this.dia}) : super(key: key);

  @override
  _DiaCardState createState() => _DiaCardState();
}

class _DiaCardState extends State<DiaCard> {
  Color getBorderColor() {
    if (widget.dia != null) {
      if (widget.dia! < 40) {
        return Colors.red;
      } else if (widget.dia! >= 50 && widget.dia! <= 80) {
        return Colors.green;
      } else if (widget.dia! >= 80 && widget.dia! <= 89) {
        return Colors.orange;
      } else if (widget.dia! >= 90) {
        return Colors.red;
      }
    }
    return Colors.black; // Default color if dia is null
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        side: BorderSide(
          color: getBorderColor(), // Dynamic border color based on dia level
          width: 4.0,
        ),
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: Container(
        width: 100,
        height: 60,
        color: const Color(0xffc8c8c8),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text('DIA:'),
            Text(
              widget.dia != null ? widget.dia!.toStringAsFixed(1) : 'null', // Display the dia value or 'null' if it's null
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}


// DiaCard end

// DiaCard start

class PulCard extends StatefulWidget {
  final int? pul;

  const PulCard({Key? key, required this.pul}) : super(key: key);

  @override
  _PulCardState createState() => _PulCardState();
}

class _PulCardState extends State<PulCard> {
  Color getBorderColor() {
    if (widget.pul != null) {
      if (widget.pul! < 12) {
        return Colors.red;
      } else if (widget.pul! >= 12 && widget.pul! <= 18) {
        return Colors.green;
      } else if (widget.pul! >= 19 && widget.pul! <= 20) {
        return Colors.orange;
      } else if (widget.pul! >= 20) {
        return Colors.red;
      }
    }

    return Colors.black; // Default color if pul is null
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        side: BorderSide(
          color: getBorderColor(), // Dynamic border color based on pul level
          width: 4.0,
        ),
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: Container(
        width: 100,
        height: 60,
        color: const Color(0xffc8c8c8),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text('PUL:'),
            Text(
              widget.pul != null ? widget.pul!.toStringAsFixed(1) : 'null', // Display the pul value or 'null' if it's null
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}


// DiaCard end