import 'dart:convert';
import 'dart:core';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:multi_select_flutter/multi_select_flutter.dart';
import 'allStudents.dart';


class ChronicIllness {
  int id;
  String condition_name;

  ChronicIllness({
    required this.id,
    required this.condition_name,
  });

  factory ChronicIllness.fromJson(Map<String, dynamic> json) {
    return ChronicIllness(
      id: json['condition'],
      condition_name: json['condition_name'] is String ? json['condition_name'] : 'Unknown',
    );
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'condition_name': condition_name,
  };
}

class EditStudChronic extends StatefulWidget {
  final Student student;

  const EditStudChronic({Key? key, required this.student}) : super(key: key);

  @override
  _EditStudChronicState createState() => _EditStudChronicState();
}

class _EditStudChronicState extends State<EditStudChronic> {
  final _chronicIllnessCommentController = TextEditingController();

  List<ChronicIllness> _chronicIllness = [];
  List<ChronicIllness> _selectedChronicIllness = [];
  List<ChronicIllness>? _mySelectedChronicIllness;
  List<Map<String, String>> transformedList = [];

  @override
  void initState() {
    super.initState();
    _fetchChronicIllnesses();
  }

  Future<void> _fetchChronicIllnesses() async {
    try {
      final response = await http.get(Uri.parse('https://uhaihealthcare.com/api/v1/school/medical-conditions/condition/3/'));
      if (response.statusCode == 200) {
        List<dynamic> data = json.decode(response.body);
        setState(() {
          _chronicIllness = data.map((json) => ChronicIllness.fromJson(json)).toList();
        });
      } else {
        throw Exception('Failed to load chronic illnesses');
      }
    } catch (e) {
      print('Failed to load chronic illnesses: $e');
    }
  }

  @override
  void dispose() {
    _chronicIllnessCommentController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xff18588d),
        title: Text("${widget.student.first_name}", style: const TextStyle(color: Color(0xffffffff))),
        iconTheme: const IconThemeData(
          color: Color(0xffffffff),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(16.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Edit / Add Chronic Illness", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.all(16.0),
              child: _chronicIllness.isEmpty
                  ? Center(child: CircularProgressIndicator())
                  : MultiSelectDialogField(
                searchable: true,
                items: _chronicIllness.map((chronicIllness) => MultiSelectItem<ChronicIllness>(chronicIllness, chronicIllness.condition_name)).toList(),
                title: const Text("Chronic Illness"),
                selectedColor: Theme.of(context).primaryColor,
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor.withOpacity(0.1),
                  border: Border.all(
                    color: Theme.of(context).primaryColor,
                    width: 2,
                  ),
                ),
                buttonIcon: Icon(
                  Icons.add,
                  color: Theme.of(context).primaryColor,
                ),
                buttonText: const Text(
                  "Select Chronic Illness",
                  style: TextStyle(
                    color: Color(0xff000000),
                    fontSize: 16,
                  ),
                ),
                onConfirm: (results) {
                  setState(() {
                    _selectedChronicIllness = List<ChronicIllness>.from(results);
                    _mySelectedChronicIllness = _selectedChronicIllness;
                    transformedList = transformChronicIllnessList(_mySelectedChronicIllness);

                  });
                },
                chipDisplay: MultiSelectChipDisplay(
                  chipColor: const Color(0xffc5c5c5),
                  textStyle: const TextStyle(
                    color: Color(0xff000000),
                  ),
                  onTap: (value) {
                    setState(() {
                      _selectedChronicIllness.remove(value);
                    });
                  },
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(16.0),
              child: TextField(
                controller: _chronicIllnessCommentController,
                maxLines: 5,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Add comments about the chronic illness',
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(16.0),
              child: ElevatedButton(
                onPressed: () {
                  saveChronicIllness(context);
                },
                style: ElevatedButton.styleFrom(
                  foregroundColor: const Color(0xffffffff),
                  backgroundColor: const Color(0xff18588d),
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.zero,
                  ),
                ),
                child: const Text('Save chronic illness'),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> saveChronicIllness(BuildContext context) async {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                CircularProgressIndicator(),
                SizedBox(width: 20),
                Text("Saving data..."),
              ],
            ),
          ),
        );
      },
    );

    Map<String, dynamic> chronic_illness_data = {
      "student_id": widget.student.id,
      'condition_type': 3,
      'selected_conditions': transformedList,
      'condition_comment': _chronicIllnessCommentController.text,
    };

    String encodedData = jsonEncode(chronic_illness_data);

    // try catch starts here

    try {
      final response = await http.post(
        Uri.parse('https://uhaihealthcare.com/api/v1/school/add-student-medical-conditions/'),
        headers: {
          'Content-Type': 'application/json',
        },
        body: encodedData,
      );
      Navigator.pop(context);
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Response', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xff18588d))),
            content: Text(jsonDecode(response.body)['message'] ?? '', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xff008000))),
            actions: <Widget>[
              TextButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    } catch (e) {
      Navigator.pop(context);

      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Error', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xff000000))),
            content: Text('An error occurred: $e', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xffff0000))),
            actions: <Widget>[
              TextButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
  //   try catch ends here

  }
}

List<Map<String, String>> transformChronicIllnessList(List<ChronicIllness>? chronicIllnessList) {
  if (chronicIllnessList == null) {
    return [];
  }
  return chronicIllnessList.map((illness) {
    return {"condition_name": illness.id.toString()};

  }).toList();
}
