import 'dart:core';
import 'dart:convert';
import 'dart:ffi';
import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';
import 'package:uhaiapp/nurse/studts.dart';
import 'dart:convert';
import 'allStudents.dart';
import '../nosql_models/exam.dart';
import 'direction_message.dart';
import 'package:dropdown_search/dropdown_search.dart';

// class Vaccination {
//   int conditionId;
//   String conditionName;
//
//   Vaccination({
//     required this.conditionId,
//     required this.conditionName,
//   });
//
//   factory Vaccination.fromJson(Map<String, dynamic> json) {
//     return Vaccination(
//       conditionId: json['conditionId'] is int ? json['conditionId'] : 0,
//       conditionName: json['conditionName'] is String ? json['conditionName'] : 'Unknown',
//     );
//   }
//   // ///// added
//   Map<String, dynamic> toJson() => {
//     "conditionId": conditionId,
//     "conditionName": conditionName,
//   };
//
//
// }

// ////////////

class Vaccine {
  final int id;
  final String vaccine;

  Vaccine({required this.id, required this.vaccine});

  factory Vaccine.fromJson(Map<String, dynamic> json) {
    return Vaccine(
      id: json['id'],
      vaccine: json['vaccine'],
    );
  }
}



// ///////////

// //////////

class EditVaccination extends StatefulWidget {
  final Student student;

  const EditVaccination({Key? key, required this.student}) : super(key: key);

  @override
  _EditVaccinationState createState() => _EditVaccinationState();
}

class _EditVaccinationState extends State<EditVaccination> {

  final _vaccinationCommentController = TextEditingController();

  Vaccine? _selectedVaccineName;
  String? _selectedVaccine = 'Select vaccine';
  int? _selectedVaccineId;

  TextEditingController _dateController = TextEditingController();
  // ///// hive

  // List<Vaccination> _selectedVaccination = [];
  // String? _mySelectedVaccination;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xff18588d),
        title: Text("${widget.student.first_name}", style: const TextStyle(color: Color(0xffffffff))),
        iconTheme: const IconThemeData(
          color: Color(0xffffffff),
        ),
      ),
      body: SingleChildScrollView(
        // scrollDirection: Axis.vertical,
        child:
        Column(
          children: [
            Padding(
              padding: EdgeInsets.all(16.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                // alignment: ,
                children: [
                  Text("Edit / Add Vaccination", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                ]

              )

            ),

            // Center(
            //   child: Padding(
            //     padding: EdgeInsets.symmetric(horizontal: 20.0),
            //     child: DropdownSearch<String>(
            //       key: ValueKey(_selectedVaccine),
            //
            //       items: [
            //         'Measles, Mumps, Rubella (MMR)',
            //         'Polio (IPV)',
            //         'Chickenpox (Varicella)',
            //         'Diphtheria, tetanus, (DTaP)',
            //         'Haemophilus influenzae type B',
            //         'Hepatitis A',
            //         'Hepatitis B',
            //         'Pneumococcal',
            //         'Yellow fever',
            //         'HPV vaccine',
            //         'Meningitis',
            //         'Cholera',
            //         'Rotavirus',
            //         'Tuberculosis',
            //         'Whooping cough'
            //
            //       ],
            //
            //       onChanged: (String? newValue) {
            //         setState(() {
            //           _selectedVaccine = newValue!;
            //         });
            //       },
            //       selectedItem: _selectedVaccine,
            //     ),
            //   ),
            // ),

            // Padding(
            //   padding: EdgeInsets.all(16.0),
            //   child: TextField(
            //     controller: _vaccinationCommentController,
            //     maxLines: 5,
            //     decoration: const InputDecoration(
            //         border: OutlineInputBorder(),
            //         labelText: 'Add comments about vaccination'
            //     ),
            //   ),
            // ),

            // //////////

            Padding(
              padding: const EdgeInsets.all(16.0),
              child: FutureBuilder<List<Vaccine>>(
                future: fetchItems(),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Center(child: CircularProgressIndicator());
                  } else if (snapshot.hasError) {
                    return Center(child: Text('Error: ${snapshot.error}'));
                  } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
                    return Center(child: Text('No items found'));
                  } else {
                    return DropdownSearch<Vaccine>(
                      key: ValueKey(_selectedVaccine),
                      items: snapshot.data!,
                      itemAsString: (Vaccine v) => v.vaccine,
                      selectedItem: _selectedVaccineName,
                      onChanged: (Vaccine? data) {
                        setState(() {
                          _selectedVaccineId = data?.id;
                          _selectedVaccine = data?.vaccine;
                          _selectedVaccineName = data;
                        });
                        print(_selectedVaccine);
                        print(_selectedVaccineId);
                      },
                      dropdownDecoratorProps: DropDownDecoratorProps(
                       dropdownSearchDecoration: InputDecoration(
                          labelText: "Select vaccine",
                          contentPadding: EdgeInsets.fromLTRB(12, 12, 0, 0),
                          border: OutlineInputBorder(),
                        ),

                      ),

                      // dropdownSearchDecoration: InputDecoration(
                      //   labelText: "Select an item",
                      //   contentPadding: EdgeInsets.fromLTRB(12, 12, 0, 0),
                      //   border: OutlineInputBorder(),
                      // ),

                    );
                  }
                },
              ),
            ),

            // //////////

            Padding(
              padding: const EdgeInsets.all(20),
              child: TextField(
                controller: _dateController,
                decoration: const InputDecoration(
                  labelText: 'DATE',
                  filled: true,
                  prefixIcon: Icon(Icons.calendar_today),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xff18588d))
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xff18588d))
                  ),
                ),
                readOnly: true,
                onTap: (){
                  _selectDate();
                },
              ),
            ),

            Padding(
              padding: EdgeInsets.all(16.0),
              child: ElevatedButton(
                onPressed: () {
                  saveVaccination(context);
                },
                style: ElevatedButton.styleFrom(
                  foregroundColor: const Color(0xffffffff),
                  backgroundColor: const Color(0xff18588d),
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.zero, // No rounded corners
                  ),
                ),
                child: const Text('Save vaccination'),
              ),
            ),

          ],
        ),


      ),
    );
  }

  // void saveVaccination(BuildContext context) {
    Future<void> saveVaccination(BuildContext context) async {

      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Dialog(
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  CircularProgressIndicator(),
                  SizedBox(width: 20),
                  Text("Saving data..."),
                ],
              ),
            ),
          );
        },
      );

      // Prepare the data for saving
      // Map<String, dynamic> vaccination_data = {
      //   'student': widget.student.id,
      //   'vaccine': _selectedVaccineId,
      //   'vaccine_issue_date': _dateController.text
      //
      // };

      Map<String, dynamic> vaccination_data = {
        'student': widget.student.id,
        'vaccine': _selectedVaccineId,
        'vaccine_issue_date': _dateController.text.isNotEmpty ? _dateController.text : null,
      };

      String encodedData = jsonEncode(vaccination_data);
      print(encodedData);

      // showDialog(
      //   context: context,
      //   builder: (BuildContext context) {
      //     return AlertDialog(
      //       title: Text('Data' , style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xff18588d)),),
      //       content: Text(encodedData),
      //
      //       actions: <Widget>[
      //         TextButton(
      //           child: Text('OK'),
      //           onPressed: () {
      //             Navigator.of(context).pop();
      //           },
      //         ),
      //       ],
      //     );
      //   },
      // );

        //   // //////////////
      // try catch starts here

      try {
        final response = await http.post(
          Uri.parse('https://uhaihealthcare.com/api/v1/school/add-student-vaccination/'),
          headers: {
            'Content-Type': 'application/json',
          },
          body: encodedData,
        );
        Navigator.pop(context);
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Response', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xff18588d))),
              content: Text(jsonDecode(response.body)['message'] ?? '', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xff008000))),
              actions: <Widget>[
                TextButton(
                  child: Text('OK'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      } catch (e) {
        Navigator.pop(context);

        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Error', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xff000000))),
              content: Text('An error occurred: $e', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Color(0xffff0000))),
              actions: <Widget>[
                TextButton(
                  child: Text('OK'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      }
      //   try catch ends here

  }

  // Get vaccines from end-point start
  Future<List<Vaccine>> fetchItems() async {
    final response = await http.get(Uri.parse('https://uhaihealthcare.com/api/v1/school/vaccines/'));

    if (response.statusCode == 200) {
      List jsonResponse = json.decode(response.body);
      return jsonResponse.map((vaccine) => Vaccine.fromJson(vaccine)).toList();
    } else {
      throw Exception('Failed to load items');
    }
  }

  // Get vaccines from end-point end

  Future<void> _selectDate() async {
    DateTime? _picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2000),
        lastDate: DateTime(2100)
    );

    if (_picked != null){
      setState(() {
        _dateController.text = _picked.toString().split(" ")[0];
      });
    }
  }





}




