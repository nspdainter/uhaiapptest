import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_expandable_fab/flutter_expandable_fab.dart';
import 'package:hive/hive.dart';
import 'package:uhaiapp/nosql_models/exam.dart';
import 'package:uhaiapp/nurse/edit_allergies.dart';
import 'package:uhaiapp/nurse/edit_chronic_illlness.dart';
import 'package:uhaiapp/nurse/edit_medical_history.dart';
import 'package:uhaiapp/nurse/edit_student_chronic.dart';
import 'package:uhaiapp/nurse/edit_vitals.dart';
import 'package:uhaiapp/nurse/medical_profile.dart';
import 'package:uhaiapp/nurse/mystepper.dart';
// import '../nosql_models/student.dart';
// import '../nosql_models/astudent.dart';
import 'conductExam.dart'; //
import 'package:fl_chart/fl_chart.dart';
import 'package:intl/intl.dart';
import 'edit_family_history.dart';
import 'edit_student_allergies.dart';
import 'edit_student_family_history.dart';
import 'edit_student_medical_history.dart';
import 'edit_vaccination.dart';
import 'examDetails.dart';
import 'localStudents.dart';
import 'allStudents.dart';
import 'vitalsCards/vitalsCards.dart';


class AccountDetailsPage extends StatefulWidget {
  // final Astudent student;
  // final Student student;


  const AccountDetailsPage({
    Key? key,
    required int school_id,
  }) : super(key: key);

  @override
  _AccountDetailsPageState createState() => _AccountDetailsPageState();
}

class _AccountDetailsPageState extends State<AccountDetailsPage> with SingleTickerProviderStateMixin {

  bool isExpanded = false; // ExpansionTile state

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xff18588d),
        title: Text(
            "Account Details",
            style: TextStyle(color: const Color(0xffffffff))
        ),
        iconTheme: IconThemeData(
          color: const Color(0xffffffff),
        ),
      ),
      body: SingleChildScrollView(
            child: Column (
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Card(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [

                        SizedBox(height: 10,),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Padding(
                                padding: EdgeInsets.all(0.0),
                                child: ExpansionTile(
                                  initiallyExpanded:true,
                                  collapsedBackgroundColor: const Color(0xff949494),
                                  collapsedTextColor: const Color(0xff000000),
                                  title: Text('School Details', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                                  trailing: Icon(Icons.expand_more),
                                  children: <Widget>[
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Padding(
                                        padding: EdgeInsets.all(8.0),
                                        child: Wrap(
                                            alignment: WrapAlignment.start,
                                            spacing: 8.0,
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.all(8.0),
                                                child: Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    Table(
                                                      columnWidths: {
                                                        0: FlexColumnWidth(1), // Adjusts the width of the label column
                                                      },
                                                      children: [
                                                        _buildTableRow('Name:', ''),
                                                        _buildTableRow('Address:', ''),
                                                        _buildTableRow('EMIS No.:', ''),
                                                        _buildTableRow('uhai ID', ''),
                                                        // _buildTableRow('ID:', '${widget.student.id}'),
                                                      ],
                                                    ),

                                                  ],
                                                ),
                                              ),
                                            ]

                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),

                                  ],
                                ),
                              ),
                            ),

                          ],
                        ),
                        SizedBox(height: 40,),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Padding(
                                padding: EdgeInsets.all(0.0),
                                child: ExpansionTile(
                                  initiallyExpanded:true,
                                  collapsedBackgroundColor: const Color(0xff949494),
                                  collapsedTextColor: const Color(0xff000000),
                                  title: Text('Nurse Details', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                                  trailing: Icon(Icons.expand_more),
                                  children: <Widget>[
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Padding(
                                        padding: EdgeInsets.all(8.0),
                                        child: Wrap(
                                            alignment: WrapAlignment.start,
                                            spacing: 8.0,
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.all(8.0),
                                                child: Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    Table(
                                                      columnWidths: {
                                                        0: FlexColumnWidth(1), // Adjusts the width of the label column
                                                      },
                                                      children: [
                                                        _buildTableRow('Names:', ''),
                                                        // _buildTableRow('ID:', '${widget.student.id}'),
                                                        _buildTableRow('uhai ID:', ''),
                                                        _buildTableRow('Service No:', ''),
                                                        // _buildTableRow('Home Address:', '${widget.student.address}'),
                                                      ],
                                                    ),

                                                  ],
                                                ),
                                              ),
                                            ]

                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    // Padding(
                                    //   padding: EdgeInsets.all(8.0),
                                    //   child: Align(
                                    //       alignment: Alignment.topLeft,
                                    //       child: Column(
                                    //         crossAxisAlignment: CrossAxisAlignment.start,
                                    //         mainAxisAlignment: MainAxisAlignment.start,
                                    //         children: <Widget>[
                                    //           Text("Comment", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                                    //           SizedBox(height: 15,),
                                    //           // Expanded(
                                    //           //     child: CommentList(studentId: widget.student.id!)
                                    //           // )
                                    //
                                    //           // Text(widget.student.chronicIllnessComment!),
                                    //         ],
                                    //
                                    //       )
                                    //   ),
                                    //
                                    // )

                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),

                      ],
                    ),
                  ),
                ),

                SizedBox(height: 20,),

                const SizedBox(height: 20,),

              ],),
          ),

    );
  }

  String formatDateTime(DateTime dateTime) {
    // Create a DateFormat with the desired format
    final DateFormat formatter = DateFormat('yyyy-MM-dd HH:mm');
    // Return the formatted date and time string
    return formatter.format(dateTime);
  }

}

TableRow _buildTableRow(String label, String value) {
  return TableRow(
    children: [
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 5.0),
        child: Text(
          label,
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 5.0),
        child: Text(
          value,
          style: TextStyle(
            fontSize: 16.0,
          ),
        ),
      ),
    ],
  );
}
