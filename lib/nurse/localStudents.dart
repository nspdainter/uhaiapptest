import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hive/hive.dart';
import 'package:uhaiapp/nosql_models/student.dart';
import 'package:uhaiapp/nosql_models/astudent.dart';
import 'dart:async';
import 'dart:convert';
import 'studentDetails.dart';

class LocalStudentsPage extends StatefulWidget {
  const LocalStudentsPage({super.key});

  @override
  State<LocalStudentsPage> createState() => _LocalStudentsPageState();
}

class _LocalStudentsPageState extends State<LocalStudentsPage> {
  late Box<Astudent> localstudentsBox;

  late Future<void> _loadDataFuture;

  // late Future<List<Astudent>> studentsFuture;
  final TextEditingController searchController = TextEditingController();


  @override
  void initState() {
    super.initState();

    localstudentsBox = Hive.box<Astudent>('localstudents');

    _loadDataFuture = insertStudentsDataFromAPI();
  }
  /////// localdata from file

  @override
  Widget build(BuildContext context) {

    // final studentData = _studentsBox.values.toList();

    return Scaffold(
      appBar: AppBar(
        backgroundColor:  const Color(0xff18588d),
        title: const Text('Students List',
          style: TextStyle(color: Color(0xffffffff)),
        ),
        iconTheme: const IconThemeData(
          color: Color(0xffffffff),
        ),

      ),
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(8.0),
            child: TextField(
              decoration: InputDecoration(
                hintText: 'Search student ...',
                prefixIcon: Icon(Icons.search),
                border: OutlineInputBorder(),
              ),
              onChanged: (value) {
                // Perform search logic here
              },
            ),
          ),
          Expanded(
              child: FutureBuilder<void>(
                future: _loadDataFuture,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const Center(child: CircularProgressIndicator());
                  } else if (snapshot.error != null) {
                    // Handle errors, if any
                    return  Center(child: Text(snapshot.error.toString()));
                  } else {
                    // Data is loaded, build the list view
                    return ListView.builder(
                        itemCount: localstudentsBox.length,
                        itemBuilder: ((context, index) {
                          // final studentBox = Hive.box<Student>('students');
                          final Astudent student = localstudentsBox.getAt(index) as Astudent;
                          String firstname = student.firstName;
                          String lastname = student.lastName;
                          String initials = "${firstname[0]}${lastname[0]}";
                          return Container(
                            color: Colors.grey.shade100,
                            // margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                            padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                            height: 100,
                            width: double.maxFinite,
                            margin: const EdgeInsets.only(bottom: 20),
                            child:  ListTile(
                              leading:  CircleAvatar(
                                radius: 40.0,
                                backgroundColor: const Color(0xffe0e0e0),
                                child: Text(
                                    initials.toUpperCase(), style: const TextStyle(fontSize: 20)
                                ),
                              ),
                              title:
                              Row(
                                children: <Widget>[
                                  Text(student.firstName),
                                  const SizedBox(width: 20),
                                  Text(student.lastName),
                                ],
                              ),
                              subtitle:Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Text(student.gender == "M" ? "Male" : "Female"),
                                  Text("${calculateAge(student.dateOfBirth)}  yrs"),
                                  const Text("Class:    Senior 2"),

                                ],
                              ),


                              trailing: const Icon(Icons.chevron_right),
                              onTap: () => {
                                // if (student != null) {
                                // Navigator.of(context).push(
                                //   MaterialPageRoute(builder: (context) => StudentDetailsPage(student: student)),
                                // )
                              },
                            ),

                            // child: Row(
                            //   children: [
                            //     Text(student.first_name!),
                            //   ],
                            // ),
                          );
                        }));
                  }
                },
              )
          )
        ],
      )


      // FutureBuilder<void>(
      //   future: _loadDataFuture,
      //   builder: (context, snapshot) {
      //     if (snapshot.connectionState == ConnectionState.waiting) {
      //       return const Center(child: CircularProgressIndicator());
      //     } else if (snapshot.error != null) {
      //       // Handle errors, if any
      //       return  Center(child: Text(snapshot.error.toString()));
      //     } else {
      //       // Data is loaded, build the list view
      //       return ListView.builder(
      //           itemCount: localstudentsBox.length,
      //           itemBuilder: ((context, index) {
      //             // final studentBox = Hive.box<Student>('students');
      //             final Astudent student = localstudentsBox.getAt(index) as Astudent;
      //             String firstname = student.firstName;
      //             String lastname = student.lastName;
      //             String initials = "${firstname[0]}${lastname[0]}";
      //             return Container(
      //               color: Colors.grey.shade100,
      //               // margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
      //               padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 5),
      //               height: 100,
      //               width: double.maxFinite,
      //               margin: const EdgeInsets.only(bottom: 20),
      //               child:  ListTile(
      //                 leading:  CircleAvatar(
      //                   radius: 40.0,
      //                   backgroundColor: const Color(0xffe0e0e0),
      //                   child: Text(
      //                       initials.toUpperCase(), style: const TextStyle(fontSize: 20)
      //                   ),
      //                 ),
      //                 title:
      //                 Row(
      //                   children: <Widget>[
      //                     Text(student.firstName),
      //                     const SizedBox(width: 20),
      //                     Text(student.lastName),
      //                   ],
      //                 ),
      //                 subtitle:Column(
      //                   crossAxisAlignment: CrossAxisAlignment.start,
      //                   mainAxisSize: MainAxisSize.min,
      //                   children: [
      //                     Text(student.gender == "M" ? "Male" : "Female"),
      //                     Text("${calculateAge(student.dateOfBirth)}  yrs"),
      //                     const Text("Class:    Senior 2"),
      //
      //                   ],
      //                 ),
      //
      //
      //                 trailing: const Icon(Icons.chevron_right),
      //                 onTap: () => {
      //                 // if (student != null) {
      //                   Navigator.of(context).push(
      //                     MaterialPageRoute(builder: (context) => StudentDetailsPage(student: student)),
      //                   )
      //                 },
      //               ),
      //
      //               // child: Row(
      //               //   children: [
      //               //     Text(student.first_name!),
      //               //   ],
      //               // ),
      //             );
      //           }));
      //     }
      //   },
      // )

    );

  }


  Future<void> insertStudentsDataFromAPI() async {
    const String localJsonPath = 'assets/data/astudentsJsonData.json';
    String jsonDataString = await rootBundle.loadString(localJsonPath);

    // Example: Fetch data from an API
    // var apiUrl = 'https://copint.org/device_test/get_data.php';
    // var response = await http.get(Uri.parse(apiUrl));
    // if (response.statusCode == 200) {
      // Assuming the API returns a JSON list of students
      List<dynamic> studentData = json.decode(jsonDataString);

      // Insert data into Hive
      await insertStudentsData(studentData.map((e) => e as Map<String, dynamic>).toList());
    // } else {
    //   throw Exception('Failed to load student data from API');
    // }
  }

//   internet connectivity

  // Future<bool> checkInternetConnection() async {
  //   var connectivityResult = await (Connectivity().checkConnectivity());
  //   if (connectivityResult == ConnectivityResult.mobile || connectivityResult == ConnectivityResult.wifi) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }
//   internet connectivity

//   insert data in local storage
  Future<void> insertStudentsData(List<Map<String, dynamic>> apiData) async {
    var box = await Hive.openBox<Astudent>('localstudents');

    for (var data in apiData) {
      final String studentId = data['id'];
      // Check if the student already exists in the box
      if (!box.containsKey(studentId)) {
        // Convert API data to Student object
        final student = Astudent(
          id: studentId,
          created: DateTime.parse(data['created'] ?? DateTime.now().toString()),
          updated: DateTime.parse(data['updated'] ?? DateTime.now().toString()),
          firstName: data['first_name'],
          lastName: data['last_name'],
          gender: data['gender'],
          dateOfBirth : DateTime.parse(data['date_of_birth']),
          tribe : data['tribe'],
          bloodGroup : data['blood_group'],
          email : data['email'],
          phone : data['phone'],
          aakey : data['aakey'],
          activeStatus : data['active_status'],
          NIN :data['NIN'],
          address : data['address'] ?? '',
          nationality : data['nationality'] ?? 'Unknown', // Provide a default value if null
          passportNumber : data['passport_number'],
          studentUniqueId : data['student_unique_id'],
          chronicIllness: data["chronic_illness"],
          chronicIllnessComment: data["chronic_illness_comment"],
          familyHistory: data["family_history"],
          familyHistoryComment: data["family_history_comment"],
          medicalHistory: data["medical_history"],
          medicalHistoryComment: data["medical_history_comment"],
          allergies: data["allergies"],
          allergiesComment: data["allergies_comment"],
          vaccination: data["vaccination"],
          vaccinationComment: data["vaccination_comment"]


    );
        // Insert the student into the box
        await box.put(studentId, student);
      }
    }
  }

  int calculateAge(DateTime birthdate) {
    DateTime currentDate = DateTime.now();
    int age = currentDate.year - birthdate.year;
    if (birthdate.month > currentDate.month ||
        (birthdate.month == currentDate.month && birthdate.day > currentDate.day)) {
      age--;
    }
    return age;
  }
//  insert data in local storage
}