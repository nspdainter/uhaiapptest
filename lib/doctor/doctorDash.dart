import 'package:flutter/material.dart';
// import 'package:flutter_expandable_fab/flutter_expandable_fab.dart';
// // import 'package:persistent_bottom_nav_bar/persistent_tab_view.dart';
import '../allStudents.dart';
import '../students.dart';

class DoctorDash extends StatelessWidget {
  final List<Map<String, dynamic>> gridItems = [
    // {'title': 'Students', 'subtitle': '546', 'icon': Icons.people_rounded, 'image': 'assets/images/students.png',},
    {'title': 'Exams', 'subtitle': '76', 'icon': Icons.medication_liquid, 'image': 'assets/images/exams.png',},
    // {'title': 'Questionnaires', 'subtitle': '987', 'icon': Icons.note_add_sharp, 'image': 'assets/images/questionnaire.png',},
    {'title': 'Notifications', 'subtitle': '16', 'icon': Icons.notifications_active, 'image': 'assets/images/notification.png',},
    // {'title': 'Statistics', 'subtitle': '657', 'icon': Icons.stacked_bar_chart_outlined, 'image': 'assets/images/stats.png',},
    // {'title': 'Referrals', 'subtitle': '5', 'icon': Icons.account_balance, 'image': 'assets/images/referrals.png',},
    {'title': 'Settings', 'subtitle': '5', 'icon': Icons.settings, 'image': 'assets/images/settings.png',},
    {'title': 'Profile', 'subtitle': '', 'icon': Icons.person, 'image': 'assets/images/profile.png',},
    // Add more items as needed
  ];

  DoctorDash({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: const Color(0xfff1f8ed),
      appBar: AppBar(
        backgroundColor:  const Color(0xff18588d),
        title: const Text('Doctor Dashboard',
          style: TextStyle(color: Color(0xffffffff)),
        ),
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.menu),
              color:  const Color(0xffffffff),
              onPressed: () {
                Scaffold.of(context).openDrawer();
              },
            );
          },
        ),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            const DrawerHeader(
              decoration: BoxDecoration(
                  color: Color(0xff18588d)
              ),
              child: Text(
                'Menu',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 24,
                ),
              ),
            ),
            ListTile(
              title: const Text('Home'),
              onTap: () {
                // Handle drawer item click
                Navigator.pop(context); // Close the drawer
              },
            ),
            ListTile(
              title: const Text('Patients'),
              onTap: () {
                // Handle drawer item click
                Navigator.pop(context); // Close the drawer
              },
              // Add more drawer items as needed
            ),
            ListTile(
              title: const Text('Exams'),
              onTap: () {
                // Handle drawer item click
                Navigator.pop(context); // Close the drawer
              },
              // Add more drawer items as needed
            ),
            ListTile(
              title: const Text('Answers'),
              onTap: () {
                // Handle drawer item click
                Navigator.pop(context); // Close the drawer
              },
              // Add more drawer items as needed
            ),
            ListTile(
              title: const Text('Notifications'),
              onTap: () {
                // Handle drawer item click
                Navigator.pop(context); // Close the drawer
              },
              // Add more drawer items as needed
            ),
            ListTile(
              title: const Text('Settings'),
              onTap: () {
                // Handle drawer item click
                Navigator.pop(context); // Close the drawer
              },
              // Add more drawer items as needed
            ),
            ListTile(
              title: const Text('Logout'),
              onTap: () {
                // Handle drawer item click
                Navigator.pop(context); // Close the drawer
              },
              // Add more drawer items as needed
            ),
          ],
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(40.0),
        child: GridView.builder(
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            crossAxisSpacing: 15.0,
            mainAxisSpacing: 15.0,
          ),
          itemCount: gridItems.length,
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () {
                // Handle item click here

                // click on students
                if (index == 0) {
                  // Navigator.push(
                  //   context,
                  //   MaterialPageRoute(builder: (context) => const StudentsPage()),
                  // );
                }
                else if (index == 1) {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const StudentPage()),
                  );
                }
                else {
                  print('Item ${index + 1} clicked');
                }
              },
              child: Card(
                // shape: RoundedRectangleBorder(
                //   borderRadius: BorderRadius.zero, // Set to zero for no round corners
                // ),
                color: const Color(0xfff1f8ed),
                elevation: 0.5,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      gridItems[index]['image'],
                      // Additional properties can be specified here
                      width: 50.0,
                      height: 50.0,
                      fit: BoxFit.contain, // or BoxFit.cover, BoxFit.fill, etc.
                    ),
                    // Icon(
                    //   gridItems[index]['icon'],
                    //   size: 30.0,
                    //   color: const Color(0xff18588d),
                    // ),
                    const SizedBox(height: 8.0),
                    Text(
                      gridItems[index]['title'],
                      style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 14.0
                      ),
                    ),
                    const SizedBox(height: 8.0),
                    Text(
                      gridItems[index]['subtitle'],
                      style: const TextStyle(fontSize: 14.0),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
      // bottomNavigationBar: BottomNavigationBar(
      //   unselectedLabelStyle: TextStyle(color: const Color(0xffffffff)),
      //   selectedLabelStyle: TextStyle(color: const Color(0xffffffff)),
      //   backgroundColor: const Color(0xff18588d),
      //   // fixedColor: const Color(0xff18588d),
      //   items: [
      //     BottomNavigationBarItem(
      //       // backgroundColor: const Color(0xff18588d),
      //       icon: Icon(Icons.home, color: const Color(0xffffffff), size: 30,),
      //       label: 'Home',
      //       // color: const Color(0xff18588d),
      //     ),
      //     BottomNavigationBarItem(
      //       icon: Icon(Icons.account_circle),
      //       label: 'Account',
      //     ),
      //   ],
      // ),
      // floatingActionButtonLocation: ExpandableFab.location,
      // floatingActionButton: ExpandableFab(
      //
      //   // key: _key,
      //   // pos: ExpandableFabPos.center,
      //   distance: 112.0,
      //   children: [
      //     FloatingActionButton.extended(
      //       onPressed: () {
      //         // Handle Add Student action
      //         print('Add Student');
      //       },
      //       label: Text("Add Student"),
      //       icon: Icon(Icons.person_add),
      //       // child: const Icon(Icons.person_add),
      //       // icon: Icon(Icons.person_add),
      //     ),
      //     FloatingActionButton.extended(
      //
      //       label: Text("Add Questionnaire"),
      //       onPressed: () {
      //         // Handle Add Questionnaire action
      //         print('Add Questionnaire');
      //       },
      //       // child: const Icon(Icons.assignment),
      //       icon: Icon(Icons.assignment),
      //     ),
      //     FloatingActionButton.extended(
      //       label: Text("Add Exam"),
      //       onPressed: () {
      //         // Handle Add Exam action
      //         print('Add Exam');
      //       },
      //       // child: const Icon(Icons.note_add),
      //       icon: Icon(Icons.note_add),
      //     ),
      //   ],
      // ),

    );
  }
}
