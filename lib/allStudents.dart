// import 'dart:js';

import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
// import 'models/students.dart';
// import 'studentDetails.dart';



// //////////

class Student {
  int? id;
  DateTime? created;
  DateTime? updated;
  String? first_name;
  String? last_name;
  String? gender;
  DateTime? date_of_birth;
  String? email;
  String? phone;
  String? aakey;
  int? active_status;
  String? NIN;
  String? address;
  String? nationality;
  String? passport_number;
  String? student_unique_id;

  Student({
    this.id,
    this.created,
    this.updated,
    this.first_name,
    this.last_name,
    this.gender,
    this.date_of_birth,
    this.email,
    this.phone,
    this.aakey,
    this.active_status,
    this.NIN,
    this.address,
    this.nationality,
    this.passport_number,
    this.student_unique_id,
  });

  Student.fromJson(Map<String, dynamic> json) {
    id = json['id'] != null ? int.tryParse(json['id']) : null;
    created = json['created'] != null ? DateTime.parse(json['created']) : null;
    updated = json['updated'] != null ? DateTime.parse(json['updated']) : null;
    first_name = json['first_name'];
    last_name = json['last_name'];
    gender = json['gender'];
    date_of_birth = json['date_of_birth'] != null ? DateTime.parse(json['date_of_birth']) : null;
    email = json['email'];
    phone = json['phone'];
    aakey = json['aakey'] ?? ''; // Provide a fallback value
    active_status = json['active_status'] != null ? int.tryParse(json['active_status']) : null;
    NIN = json['NIN'];
    address = json['address'] ?? ''; // Provide a fallback value
    nationality = json['nationality'];
    passport_number = json['passport_number'];
    student_unique_id = json['student_unique_id'];
  }


}

// /////////

Future<List<Student>> getStudents() async {

var url = Uri.parse("https://copint.org/device_test/get_data.php");
final response = await http.get(url, headers: {"Content-Type": "application/json"});
final List body = json.decode(response.body);
return body.map((e) => Student.fromJson(e)).toList();
}

// /////////

class StudentsPage extends StatefulWidget {
  const StudentsPage({super.key, required int school_id});


  @override
  State<StudentsPage> createState() => _StudentsPageState();

}

class _StudentsPageState extends State<StudentsPage> {

  late Future<List<Student>> studentsFuture;

 @override
 void initState() {
   super.initState();
   studentsFuture = getStudents();
 }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor:  const Color(0xff18588d),
        title: const Text('All students',
          style: TextStyle(color: Color(0xffffffff)),
        ),

      ),
      body: Center(
        child: FutureBuilder<List<Student>>(
          future: studentsFuture,
          builder: (context, snapshot){
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const CircularProgressIndicator();
            }
            else if (snapshot.hasData) {
              final students = snapshot.data!;
              return buildStudents(students);
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            else {
              return const Text("No students data available");
            }
          },

        ),
      )

      );


  }

//   build student widget
  Widget buildStudents(List<Student> students) {
    return ListView.builder(
      itemCount: students.length,
      itemBuilder: (context, index) {
        final student = students[index];
        return Container(
          color: Colors.grey.shade200,
          margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
          padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 5),
          height: 100,
          width: double.maxFinite,
          child: ListTile(
            leading: CircleAvatar(
              backgroundColor: Colors.brown.shade800,
              child: const Text('AH'),
            ),
            title:  Text(student.first_name!),
            onTap: () => {
            // Navigator.push(
            // context,
            // MaterialPageRoute(builder: (context) => StudentDetailsPage(student: student,)),
            // ),
            },
          ),
          // child: Row(
          //   children: [
          //     Text(student.first_name!),
          //   ],
          // ),
        );
      },
    );
  }
// build student widget
}