import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  static const _databaseName = "alocalstore.db";
  static const _databaseVersion = 1;

  // Make this a singleton class.
  DatabaseHelper._privateConstructor();

  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  // Only have a single app-wide reference to the database.
  static Database? _database;

  Future<Database> get database async =>
      _database ??= await _initializeDatabase();

  // Open the database and create it if it doesn't exist.
  Future<Database> _initializeDatabase() async {
    String path = join(await getDatabasesPath(), _databaseName);
    return await openDatabase(path,
        version: _databaseVersion,
        onCreate: _onCreate);
  }

  // SQL code to create the database tables.
  Future _onCreate(Database db, int version) async {
    // Create the 'examinations' table
    await db.execute('''
          CREATE TABLE examinations (
            id INTEGER PRIMARY KEY,
            created TEXT,
            updated TEXT,
            symptom_comment TEXT,
            disease_comment TEXT,
            prescription_comment TEXT,
            nurse_id INTEGER,
            student_id INTEGER,
            examinationStatus TEXT,
            examinationCategory_id INTEGER,
            link_to_previous_exam INTEGER
          )
          ''');

    // Create the 'symptoms' table
    await db.execute('''
          CREATE TABLE symptoms (
            id INTEGER PRIMARY KEY,
            exam_id INTEGER,
            symptom_id INTEGER,
            symptom_name TEXT,
            FOREIGN KEY (exam_id) REFERENCES examinations(id)
          )
          ''');

    // Similarly, create the other dependent tables ('diseases', 'prescriptions', etc.) with the structure you provided
    // Example for 'diseases' table
    await db.execute('''
          CREATE TABLE diseases (
            id INTEGER PRIMARY KEY,
            exam_id INTEGER,
            disease_id INTEGER,
            disease_name TEXT,
            FOREIGN KEY (exam_id) REFERENCES examinations(id)
          )
          ''');

    await db.execute('''
          CREATE TABLE prescriptions (
            id INTEGER PRIMARY KEY,
            exam_id INTEGER,
            drug_id INTEGER,
            drug_name TEXT,
            created TEXT, -- Stores both date and time
            FOREIGN KEY (exam_id) REFERENCES examinations(id)
          )
          ''');

    await db.execute('''
          CREATE TABLE temperature (
            id INTEGER PRIMARY KEY,
            exam_id INTEGER,
            temp REAL, -- Temperature value, using REAL for fractional numbers
            mode TEXT, -- 'manual' or 'auto'
            created TEXT, -- Stores both date and time
            FOREIGN KEY (exam_id) REFERENCES examinations(id)
          )
          ''');

    await db.execute('''
          CREATE TABLE weight (
            id INTEGER PRIMARY KEY,
            exam_id INTEGER,
            weight REAL, -- Weight value, using REAL for fractional numbers
            mode TEXT, -- 'manual' or 'auto'
            created TEXT, -- Stores both date and time
            FOREIGN KEY (exam_id) REFERENCES examinations(id)
            )
          ''');

    await db.execute('''
          CREATE TABLE glucose (
            id INTEGER PRIMARY KEY,
            exam_id INTEGER,
            glucose_level REAL, -- Glucose level, using REAL for fractional numbers
            mode TEXT, -- 'manual' or 'auto'
            created TEXT, -- Stores both date and time
            FOREIGN KEY (exam_id) REFERENCES examinations(id)
          )
        ''');
    await db.execute('''
          CREATE TABLE height (
            id INTEGER PRIMARY KEY,
            exam_id INTEGER,
            height REAL, -- Height value, using REAL for fractional numbers
            mode TEXT, -- 'manual' or 'auto'
            created TEXT, -- Stores both date and time
            FOREIGN KEY (exam_id) REFERENCES examinations(id)
          )
        ''');

    await db.execute('''
          CREATE TABLE oximeter (
            id INTEGER PRIMARY KEY,
            exam_id INTEGER,
            spo2 REAL, -- Oxygen saturation level
            pr_bpm REAL, -- Pulse rate in beats per minute
            mode TEXT, -- 'manual' or 'auto'
            created TEXT, -- Stores both date and time
            FOREIGN KEY (exam_id) REFERENCES examinations(id)
          )
        ''');

    await db.execute('''
          CREATE TABLE blood_pressure (
            id INTEGER PRIMARY KEY,
            exam_id INTEGER,
            sys INTEGER, -- Systolic blood pressure
            dia INTEGER, -- Diastolic blood pressure
            pul INTEGER, -- Pulse rate
            mode TEXT, -- 'manual' or 'auto'
            created TEXT, -- Stores both date and time
            FOREIGN KEY (exam_id) REFERENCES examinations(id)
          )
        ''');

    await db.execute('''
          CREATE TABLE quick_test (
            id INTEGER PRIMARY KEY,
            exam_id INTEGER,
            disease_name TEXT, -- Name of the disease tested for
            neg_pos TEXT, -- Test result: 'negative' or 'positive'
            mode TEXT, -- 'manual' or 'auto'
            created TEXT, -- Stores both date and time
            FOREIGN KEY (exam_id) REFERENCES examinations(id)
          )
        ''');

    // Continue with 'prescriptions', 'temperature', 'height', 'weight', 'glucose', 'oximeter', 'blood_pressure', 'quick_test'
    // Make sure to replace the below line with actual table creation SQL commands for each of the tables mentioned
    // ...
  }

//   save starts here

  Future<void> saveExam({
    required Map<String, dynamic> examinationData,
    required List<Map<String, dynamic>> symptomsData,
    required List<Map<String, dynamic>> diseasesData,
    required Map<String, dynamic> quickTestData,
    required Map<String, dynamic> temperatureData,
  }) async {
    final db = await database;
    await db.transaction((txn) async {
      try {
        // Step 1: Insert the examination record
        final int examId = await txn.insert('examinations', examinationData);

        // Step 2: Insert related symptoms
        for (var symptom in symptomsData) {
          await txn.insert('symptoms', {
            ...symptom,
            'exam_id': examId,
          });
        }

        // Step 3: Insert related diseases
        for (var disease in diseasesData) {
          await txn.insert('diseases', {
            ...disease,
            'exam_id': examId,
          });
        }

        // Step 4: Insert a quick_test record
        await txn.insert('quick_test', {
          ...quickTestData,
          'exam_id': examId,
        });

        // Step 5: Insert a temperature record
        await txn.insert('temperature', {
          ...temperatureData,
          'exam_id': examId,
        });

        // Transaction automatically commits if no exceptions were thrown
      } catch (e) {
        print("Error during saveExam transaction: $e");
        throw Exception("Failed to save examination and related details.");
      }
    });
  }


//  save ends here
}
