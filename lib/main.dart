import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart' as path;
import 'package:hive_flutter/hive_flutter.dart';
import 'package:uhaiapp/login_prod.dart';
import 'package:uhaiapp/nosql_models/astudent.dart';
import '../nosql_models/student.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';

import 'loginStart.dart';
import 'nosql_models/exam.dart';



void main() async{

  WidgetsBinding widgetsBinding= WidgetsFlutterBinding.ensureInitialized();
  FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
  await Future.delayed(
    Duration(seconds: 5),
  );
  FlutterNativeSplash.remove();

  final dir = await path.getApplicationDocumentsDirectory();
  Hive.init(dir.path);
  Hive.initFlutter('uhai_db');

  Hive.registerAdapter<Student>(StudentAdapter());
  Hive.registerAdapter<Astudent>(AstudentAdapter());
  Hive.registerAdapter<Exam>(ExamAdapter());

  await Hive.openBox<Student>('students');
  // await Hive.openBox<Astudent>('astudents'); old box has some data before model was updated
  await Hive.openBox<Astudent>('localstudents'); // has new data after updating model
  await Hive.openBox<Exam>('exams');

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: '',
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
          useMaterial3: true,
          // primarySwatch: Colors.orange,
        ),
        // home: const HomePage());
        home: const LoginProd());
  }
}

