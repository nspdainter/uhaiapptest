import 'package:flutter/material.dart';
import 'package:uhaiapp/doctor/doctorDash.dart';
import 'package:uhaiapp/nurse/nurseDash.dart';
import 'package:uhaiapp/parent/parentDash.dart';
// import 'package:flutter_native_splash/flutter_native_splash.dart';

class LoginStart extends StatefulWidget {
  const LoginStart({super.key});

  @override
  _LoginStartState createState() => _LoginStartState();
}

class _LoginStartState extends State<LoginStart> {
  final _phoneController = TextEditingController();

  // /////// splash screen start

  // /////// splash screen end

  // phone number checker



  // phone number checker ends here

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        // title: Text("Login Page"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 60.0),
              child: Center(
                child: SizedBox(
                    width: 200,
                    height: 150,
                    /*decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(50.0)),*/
                    child:

                    Image.asset('assets/images/uhai-logo-sample.png')
                ),
              ),
            ),
            const SizedBox(
              height: 80,
            ),
            Padding(
              //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: TextField(
                controller: _phoneController,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Phone number',
                    
                ),
              ),
            ),
            const SizedBox(
              height: 50,
            ),


            // Text('New User? Create Account')
          ],
        ),
      ),
    );
  }
}

