// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'student.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class StudentAdapter extends TypeAdapter<Student> {
  @override
  final int typeId = 0;

  @override
  Student read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Student(
      id: fields[0] as String,
      created: fields[1] as DateTime,
      updated: fields[2] as DateTime,
      firstName: fields[3] as String,
      lastName: fields[4] as String,
      gender: fields[5] as String,
      dateOfBirth: fields[6] as DateTime,
      email: fields[7] as String,
      phone: fields[8] as String,
      aakey: fields[9] as String,
      activeStatus: fields[10] as String,
      NIN: fields[11] as String?,
      address: fields[12] as String,
      nationality: fields[13] as String,
      passportNumber: fields[14] as String?,
      studentUniqueId: fields[15] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, Student obj) {
    writer
      ..writeByte(16)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.created)
      ..writeByte(2)
      ..write(obj.updated)
      ..writeByte(3)
      ..write(obj.firstName)
      ..writeByte(4)
      ..write(obj.lastName)
      ..writeByte(5)
      ..write(obj.gender)
      ..writeByte(6)
      ..write(obj.dateOfBirth)
      ..writeByte(7)
      ..write(obj.email)
      ..writeByte(8)
      ..write(obj.phone)
      ..writeByte(9)
      ..write(obj.aakey)
      ..writeByte(10)
      ..write(obj.activeStatus)
      ..writeByte(11)
      ..write(obj.NIN)
      ..writeByte(12)
      ..write(obj.address)
      ..writeByte(13)
      ..write(obj.nationality)
      ..writeByte(14)
      ..write(obj.passportNumber)
      ..writeByte(15)
      ..write(obj.studentUniqueId);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is StudentAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
