// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'exam.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ExamAdapter extends TypeAdapter<Exam> {
  @override
  final int typeId = 2;

  @override
  Exam read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Exam(
      examId: fields[0] as int,
      createdAt: fields[1] as DateTime,
      studentId: fields[2] as String,
      nurseId: fields[3] as String,
      symptoms: (fields[4] as List).cast<dynamic>(),
      symptomsComment: fields[5] as String,
      temperature: fields[6] as double?,
      weight: fields[7] as double?,
      oximeter: (fields[8] as List?)?.cast<dynamic>(),
      bloodPressure: (fields[9] as List?)?.cast<dynamic>(),
      glucose: fields[10] as double?,
      height: fields[11] as double?,
      rapidTests: (fields[12] as List?)?.cast<dynamic>(),
      examCategory: fields[13] as String,
      diseases: (fields[14] as List).cast<dynamic>(),
      diseasesComment: fields[15] as String,
      drugs: (fields[16] as List).cast<dynamic>(),
      drugsComment: fields[17] as String,
    );
  }

  @override
  void write(BinaryWriter writer, Exam obj) {
    writer
      ..writeByte(18)
      ..writeByte(0)
      ..write(obj.examId)
      ..writeByte(1)
      ..write(obj.createdAt)
      ..writeByte(2)
      ..write(obj.studentId)
      ..writeByte(3)
      ..write(obj.nurseId)
      ..writeByte(4)
      ..write(obj.symptoms)
      ..writeByte(5)
      ..write(obj.symptomsComment)
      ..writeByte(6)
      ..write(obj.temperature)
      ..writeByte(7)
      ..write(obj.weight)
      ..writeByte(8)
      ..write(obj.oximeter)
      ..writeByte(9)
      ..write(obj.bloodPressure)
      ..writeByte(10)
      ..write(obj.glucose)
      ..writeByte(11)
      ..write(obj.height)
      ..writeByte(12)
      ..write(obj.rapidTests)
      ..writeByte(13)
      ..write(obj.examCategory)
      ..writeByte(14)
      ..write(obj.diseases)
      ..writeByte(15)
      ..write(obj.diseasesComment)
      ..writeByte(16)
      ..write(obj.drugs)
      ..writeByte(17)
      ..write(obj.drugsComment);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ExamAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
