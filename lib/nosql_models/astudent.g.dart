// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'astudent.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class AstudentAdapter extends TypeAdapter<Astudent> {
  @override
  final int typeId = 3;

  @override
  Astudent read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Astudent(
      id: fields[0] as String,
      created: fields[1] as DateTime,
      updated: fields[2] as DateTime,
      firstName: fields[3] as String,
      lastName: fields[4] as String,
      gender: fields[5] as String,
      dateOfBirth: fields[6] as DateTime,
      tribe: fields[7] as String,
      bloodGroup: fields[8] as String,
      email: fields[9] as String,
      phone: fields[10] as String,
      aakey: fields[11] as String,
      activeStatus: fields[12] as String,
      NIN: fields[13] as String?,
      address: fields[14] as String,
      nationality: fields[15] as String,
      passportNumber: fields[16] as String?,
      studentUniqueId: fields[17] as String?,
      chronicIllness: (fields[18] as List).cast<dynamic>(),
      chronicIllnessComment: fields[19] as String?,
      familyHistory: (fields[20] as List).cast<dynamic>(),
      familyHistoryComment: fields[21] as String?,
      medicalHistory: (fields[22] as List).cast<dynamic>(),
      medicalHistoryComment: fields[23] as String?,
      allergies: (fields[24] as List).cast<dynamic>(),
      allergiesComment: fields[25] as String?,
      vaccination: (fields[26] as List).cast<dynamic>(),
      vaccinationComment: fields[27] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, Astudent obj) {
    writer
      ..writeByte(28)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.created)
      ..writeByte(2)
      ..write(obj.updated)
      ..writeByte(3)
      ..write(obj.firstName)
      ..writeByte(4)
      ..write(obj.lastName)
      ..writeByte(5)
      ..write(obj.gender)
      ..writeByte(6)
      ..write(obj.dateOfBirth)
      ..writeByte(7)
      ..write(obj.tribe)
      ..writeByte(8)
      ..write(obj.bloodGroup)
      ..writeByte(9)
      ..write(obj.email)
      ..writeByte(10)
      ..write(obj.phone)
      ..writeByte(11)
      ..write(obj.aakey)
      ..writeByte(12)
      ..write(obj.activeStatus)
      ..writeByte(13)
      ..write(obj.NIN)
      ..writeByte(14)
      ..write(obj.address)
      ..writeByte(15)
      ..write(obj.nationality)
      ..writeByte(16)
      ..write(obj.passportNumber)
      ..writeByte(17)
      ..write(obj.studentUniqueId)
      ..writeByte(18)
      ..write(obj.chronicIllness)
      ..writeByte(19)
      ..write(obj.chronicIllnessComment)
      ..writeByte(20)
      ..write(obj.familyHistory)
      ..writeByte(21)
      ..write(obj.familyHistoryComment)
      ..writeByte(22)
      ..write(obj.medicalHistory)
      ..writeByte(23)
      ..write(obj.medicalHistoryComment)
      ..writeByte(24)
      ..write(obj.allergies)
      ..writeByte(25)
      ..write(obj.allergiesComment)
      ..writeByte(26)
      ..write(obj.vaccination)
      ..writeByte(27)
      ..write(obj.vaccinationComment);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AstudentAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
