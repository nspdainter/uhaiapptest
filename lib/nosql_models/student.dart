import 'package:hive/hive.dart';

part 'student.g.dart'; // This file is generated by running the build_runner

@HiveType(typeId: 0)
class Student extends HiveObject {
  @HiveField(0)
  late String id;

  @HiveField(1)
  late DateTime created;

  @HiveField(2)
  late DateTime updated;

  @HiveField(3)
  late String firstName;

  @HiveField(4)
  late String lastName;

  @HiveField(5)
  late String gender;

  @HiveField(6)
  late DateTime dateOfBirth;

  @HiveField(7)
  late String email;

  @HiveField(8)
  late String phone;

  @HiveField(9)
  late String aakey;

  @HiveField(10)
  late String activeStatus;

  @HiveField(11)
  late String? NIN;

  @HiveField(12)
  late String address;

  @HiveField(13)
  late String nationality;

  @HiveField(14)
  late String? passportNumber;

  @HiveField(15)
  late String? studentUniqueId;

  Student({
    required this.id,
    required this.created,
    required this.updated,
    required this.firstName,
    required this.lastName,
    required this.gender,
    required this.dateOfBirth,
    required this.email,
    required this.phone,
    required this.aakey,
    required this.activeStatus,
    required this.NIN,
    required this.address,
    required this.nationality ,
    required this.passportNumber,
    required this.studentUniqueId
  });


}
