import 'package:flutter/material.dart';

class AddStudent extends StatefulWidget {
  const AddStudent({super.key});

  @override
  _AddStudentState createState() => _AddStudentState();
}

class _AddStudentState extends State<AddStudent> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  final TextEditingController _genderController = TextEditingController();
  final TextEditingController _dobController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _aakeyController = TextEditingController();
  final TextEditingController _ninController = TextEditingController();
  final TextEditingController _addressController = TextEditingController();
  final TextEditingController _nationalityController = TextEditingController();
  final TextEditingController _passportNumberController = TextEditingController();
  final TextEditingController _studentUniqueIdController = TextEditingController();
  bool _isActive = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor:  const Color(0xff18588d),
        title: const Text('Add Student',
          style: TextStyle(color: Color(0xffffffff)),
        ),

      ),
      body: Form(
    key: _formKey,
    child: ListView(
    padding: const EdgeInsets.all(8.0),
    children: <Widget>[
    TextFormField(controller: _firstNameController, decoration: const InputDecoration(labelText: 'First Name')),
    TextFormField(controller: _lastNameController, decoration: const InputDecoration(labelText: 'Last Name')),
    TextFormField(controller: _genderController, decoration: const InputDecoration(labelText: 'Gender')),
    TextFormField(controller: _dobController, decoration: const InputDecoration(labelText: 'Date of Birth')),
    TextFormField(controller: _emailController, decoration: const InputDecoration(labelText: 'Email')),
    TextFormField(controller: _phoneController, decoration: const InputDecoration(labelText: 'Phone')),
    TextFormField(controller: _aakeyController, decoration: const InputDecoration(labelText: 'AA Key')),
    SwitchListTile(
    title: const Text('Active Status'),
    value: _isActive,
    onChanged: (bool value) { setState(() { _isActive = value; }); },
    ),
    TextFormField(controller: _ninController, decoration: const InputDecoration(labelText: 'NIN')),
    TextFormField(controller: _addressController, decoration: const InputDecoration(labelText: 'Address')),
    TextFormField(controller: _nationalityController, decoration: const InputDecoration(labelText: 'Nationality')),
    TextFormField(controller: _passportNumberController, decoration: const InputDecoration(labelText: 'Passport Number')),
    TextFormField(controller: _studentUniqueIdController, decoration: const InputDecoration(labelText: 'Student Unique ID')),
    ElevatedButton(
    onPressed: () {
    if (_formKey.currentState!.validate()) {
    // Process data
    }
    },
    child: const Text('Register'),
    ),
    ],
    ),

    ),
    );

    //   Form(
    //   key: _formKey,
    //   child: ListView(
    //     padding: EdgeInsets.all(8.0),
    //     children: <Widget>[
    //       TextFormField(controller: _firstNameController, decoration: InputDecoration(labelText: 'First Name')),
    //       TextFormField(controller: _lastNameController, decoration: InputDecoration(labelText: 'Last Name')),
    //       TextFormField(controller: _genderController, decoration: InputDecoration(labelText: 'Gender')),
    //       TextFormField(controller: _dobController, decoration: InputDecoration(labelText: 'Date of Birth')),
    //       TextFormField(controller: _emailController, decoration: InputDecoration(labelText: 'Email')),
    //       TextFormField(controller: _phoneController, decoration: InputDecoration(labelText: 'Phone')),
    //       TextFormField(controller: _aakeyController, decoration: InputDecoration(labelText: 'AA Key')),
    //       SwitchListTile(
    //         title: Text('Active Status'),
    //         value: _isActive,
    //         onChanged: (bool value) { setState(() { _isActive = value; }); },
    //       ),
    //       TextFormField(controller: _ninController, decoration: InputDecoration(labelText: 'NIN')),
    //       TextFormField(controller: _addressController, decoration: InputDecoration(labelText: 'Address')),
    //       TextFormField(controller: _nationalityController, decoration: InputDecoration(labelText: 'Nationality')),
    //       TextFormField(controller: _passportNumberController, decoration: InputDecoration(labelText: 'Passport Number')),
    //       TextFormField(controller: _studentUniqueIdController, decoration: InputDecoration(labelText: 'Student Unique ID')),
    //       ElevatedButton(
    //         onPressed: () {
    //           if (_formKey.currentState!.validate()) {
    //             // Process data
    //           }
    //         },
    //         child: Text('Register'),
    //       ),
    //     ],
    //   ),
    // );
  }

  @override
  void dispose() {
    _firstNameController.dispose();
    _lastNameController.dispose();
    _genderController.dispose();
    _dobController.dispose();
    _emailController.dispose();
    _phoneController.dispose();
    _aakeyController.dispose();
    _ninController.dispose();
    _addressController.dispose();
    _nationalityController.dispose();
    _passportNumberController.dispose();
    _studentUniqueIdController.dispose();
    super.dispose();
  }
}
